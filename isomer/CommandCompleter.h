/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifdef QCOMPLETER

#ifndef _COMMANDCOMPLETER_H_
#define _COMMANDCOMPLETER_H_

#include <QDialog> //base class of dialog windows
#include <QLineEdit> //one line text editor
#include <QCompleter> //completions based on item modal class
#include <QVBoxLayout> //lines widgets vertically
#include <QStringList> //list of strings
#include <QAbstractItemModel> // The QAbstractItemModel class provides the abstract interface for item model classes aka Qt's own MVC architecture.
#include <QFile> //read/write files
#include <QStringListModel> //strings to views
#include <QApplication>
#include <QLabel>
#include <QStyle> //encap look and feel of gui
#include <QPalette> //color groups fr widget state
#include <QShortcut> //keyboard shortcut/hotkeys
#include <QStandardItemModel> //storage of cutom data
#include <QAction>

/*
*  CommandCompleter.h
*  CommandCompleter
*
*  Popup commandline interface
*
* CommandCompleter provides a popup command line interface for accessing
* all the functionality of Isomer with keyboard input instead of mouse clicks.
*
*/

class CommandCompleter : public QDialog {
    Q_OBJECT

public:

    CommandCompleter ( QWidget *m, QWidget * parent = 0, Qt::WindowFlags f = 0 ); //constructor, that accepts a QWidget as its own display, a parent tht is main window, and a flag, which is used to specify various window-system properties for the widget

private:

    QLineEdit *mLineEdit;								// single line input widget
    QStringList mWordList;							// array of strings to feed to the AutoCompleter class
    QCompleter *mCompleter;							// the autoCompletion widget
    QLabel *mQuickCommandLabel;					// short instructions displayed in the popup window
    QStringListModel *mModel;						// the completion model for the AutoCompleter widget

public slots:

    /*
    * causes the CommandCompleter dialog to popup and sets the focus to the text input box
    *
    * this dialog is modal, meaning no other windows will accept focus while this window is displayed.
    * the user can hit escape to hide the window if they do not want to input anything, or simply hit enter.
    *
    */
    int exec(){
        mLineEdit->clear(); //clear the lineEdit
        if ( QDialog::exec() == QDialog::Accepted ) //if lineEdit gets executed (return 1) and accepted (return 1)
            return mWordList.indexOf(mLineEdit->text()); //return the match

        return -2; //exit
    }

};

#endif
#endif //QCOMPLETER
