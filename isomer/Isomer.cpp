/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
* Isomer File which pulls in teh MainWindow
*
* Isomer.h
*
*/

#include "Isomer.h"

Isomer::~Isomer(){

}

Isomer::Isomer(int & argc, char ** argv, bool GUIenabled )
    : QApplication(argc,argv,GUIenabled){

    setApplicationName("Isomer");

    mainWindow = new MainWindow();

    QString locale = QLocale::system().name();
    QTranslator translator;
    translator.load(QString(":/Isomer_") + locale);
    installTranslator(&translator);

    processEvents( );

    mainWindow->resize( 1000, 800 );
    mainWindow->show();
}

MainWindow *Isomer::getMainWindow(){
    return mainWindow;
}

bool Isomer::event(QEvent *event){

    switch (event->type()) {
    case 116:
        mainWindow->loadFile(static_cast<QFileOpenEvent *>(event)->file());
        return true;
    default:
        return QApplication::event(event);
    };
}


