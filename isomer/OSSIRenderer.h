/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSI_RENDERER_HH_
#define _OSSI_RENDERER_HH_

// Abstrtact base class for various renderers for OSSI
// Defines the minimum interface that renderer classes must implement
// Also contains an integer which can be used to store render flags
// The way the integer is used to specify the render flags is left
// to the derived class. An enumeration for render flags is also defined
// with the various values defined as powers of 2 to allow ORing together
// The class also provides static functions for rendering the object as a wireframe,
// silhouette and vertices, which can be used by derived classes

#include <QColor>
#include <QMessageBox>


#include <GL/gl.h>
#include <GL/glu.h>


#include <OSSIObject.h>
#include "GeometryRenderer.h"
#include "ISOPatchObject.h"

#include "CgCompute.h"

#ifdef GPU_OK
using namespace Cg;
#endif // GPU_OK


using namespace OSSI;

class OSSIRenderer;
typedef OSSIRenderer * OSSIRendererPtr;

class OSSIRenderer {

private :

  static QColor mWireframeColor;									// wireframe RGB color
  static QColor mSilhouetteColor;								// silhouette RGB color
  static QColor mVertexColor;										// vertex RGB color
  static QColor mFaceCentroidColor;							// face centroid RGB color
    static QColor mNormalColor;										// normals RGB color
    static double mFaceCentroidThickness;					// face centroid point size
    static double mNormalThickness;								// normal line thickness
    static double mNormalLength;										// normal length
    static double mWireframeThickness;							// wireframe line thickness
    static double mSilhouetteThickness;						// silhouette line thickness
  static double mVertexThickness;								// vertex point size

protected :

  GeometryRenderer *gr;										// pointer to the singleton class GeometryRenderer

  /* Integer which can be used to represent a combination of render flags */
  // static int render_flags;

  /* Flag indicating if we should front or back side of object */
  int render_flags;
  static bool reverse_object;
    #ifdef GPU_OK
    static bool useGPU;
    #endif
    static bool antialiasing;

public :



protected :

  /* Default constructor */
  OSSIRenderer() {

        #ifdef GPU_OK
        OSSIRenderer::useGPU = false;
        #endif
        OSSIRenderer::antialiasing = false;
    OSSIRenderer::mWireframeColor.setRgbF(0.0,0.0,0.0,0.9);
    OSSIRenderer::mSilhouetteColor.setRgbF(0.0,0.0,0.0,0.8);
    OSSIRenderer::mVertexColor.setRgbF(0.5,1.0,0.0,0.9);
        OSSIRenderer::mFaceCentroidColor.setRgbF(0.5,0.0,0.0,0.9);
        OSSIRenderer::mNormalColor.setRgbF(0.5,0.0,0.0,0.9);
    render_flags = 0;
    // reverse_object = 0;
    gr = GeometryRenderer::instance( );
        gr->drawWireframe = true;
  }

  OSSIRenderer(QColor wc, double wt, QColor sc, double st, QColor vc, double vt, QColor fc, double ft, QColor nc, double nt, bool gpu = false, bool aa = false) {

        #ifdef GPU_OK
        OSSIRenderer::useGPU = gpu;
        #endif
        OSSIRenderer::antialiasing = aa;
    render_flags = 0;
    gr = GeometryRenderer::instance( );
        gr->drawWireframe = true;
  };

  /* Copy constructor */
  OSSIRenderer(const OSSIRenderer&) { }

public :

  /* Assignment operator */
  OSSIRenderer& operator = (const OSSIRenderer&) {
    return (*this);
  }

  /* Destructor */
  virtual ~OSSIRenderer() {}

  /* Set the render flags */
  void setRenderFlags(int rflags) {
    // render_flags = rflags;
  }

  void toggleWireframe(void) {
    // render_flags ^= ShowWireframe;
        gr->drawWireframe = !(gr->drawWireframe);
  }

  void toggleSilhouette(void) {
    // render_flags ^= ShowSilhouette;
        gr->drawSilhouette = !(gr->drawSilhouette);
  }

  void toggleVertices(void) {
    // render_flags ^= ShowVertices;
        gr->drawVertices = !(gr->drawVertices);
  }

  void toggleFaceNormals(void) {
    // render_flags ^= ShowFaceNormals;
    gr->drawFaceNormals = !(gr->drawFaceNormals);
  }

  void toggleFaceCentroids(void) {
    // render_flags ^= ShowFaceCentroids;
    gr->drawFaceCentroids = !(gr->drawFaceCentroids);
  }

    #ifdef GPU_OK
    void toggleGPU() {
        OSSIRenderer::useGPU = !OSSIRenderer::useGPU;
        gr->useGPU = !gr->useGPU;
    }
    #endif

    void toggleAntialiasing(){
        OSSIRenderer::antialiasing = !OSSIRenderer::antialiasing;
        gr->antialiasing = !(gr->antialiasing);
    }

  void toggleObjectOrientation(void) {
    OSSIRenderer::reverse_object != OSSIRenderer::reverse_object;
    if(gr) gr->isReversed = OSSIRenderer::reverse_object;
    if ( OSSIRenderer::reverse_object ) {
      OSSIRenderer::reverse_object = false;
    } else {
      OSSIRenderer::reverse_object = true;
    }
  }

  static bool isReversed(void) {
    return OSSIRenderer::reverse_object;
  }

  /* Get the render flags */
  int getRenderFlags(void) const {
    return render_flags;
  }


  /*
    Render the OSSIObject specified by the given pointer.
    This is a virtual function which has to be implemented by derived classes.
    Return value can be used for error reporting..
  */
  virtual int render(OSSIObjectPtr object) = 0;
  virtual int render(ISOPatchObjectPtr patchObject) { /* most renderers leave this blank*/ }
  /*
    Perform any initializations required
  */
  virtual void initialize(void) {
    // Derived classes should override this if necessary
    // to do any initializations (like texture setup)
  };

  virtual void setState( ) { }

public :

  /*
    Set the culling based on which side of object we want to display.
    Has effect only if culling is enabled
  */
  void setCulling(void) {
    if ( OSSIRenderer::reverse_object )
      glCullFace(GL_FRONT);
    else
      glCullFace(GL_BACK);
  }

  /* Methods for various types of rendering */
  void drawWireframe(OSSIObjectPtr object) {
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glColor4f( OSSIRenderer::mWireframeColor.redF(), OSSIRenderer::mWireframeColor.greenF(), OSSIRenderer::mWireframeColor.blueF(), OSSIRenderer::mWireframeColor.alphaF());
    glDepthRange(0.0,1.0-0.0005);

    gr->renderEdges( object, OSSIRenderer::mWireframeThickness );
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  }

  void drawSilhouette(OSSIObjectPtr object) {
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glColor4f(OSSIRenderer::mSilhouetteColor.redF(),OSSIRenderer::mSilhouetteColor.greenF(),OSSIRenderer::mSilhouetteColor.blueF(),OSSIRenderer::mSilhouetteColor.alphaF());
    glDepthRange(0.1,1.0);
    if ( OSSIRenderer::reverse_object )
      gr->renderEdges( object, OSSIRenderer::mWireframeThickness );
    else
      gr->renderEdges( object, OSSIRenderer::mSilhouetteThickness );
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  };

  void drawVertices(OSSIObjectPtr object) {
    glColor4f(OSSIRenderer::mVertexColor.redF(),OSSIRenderer::mVertexColor.greenF(),OSSIRenderer::mVertexColor.blueF(),OSSIRenderer::mVertexColor.alphaF());
    glDepthRange(0.0,1.0-0.00075);

    gr->renderVertices( object, mVertexThickness );
  }

  void drawFaceCentroids(OSSIObjectPtr object) {
        #ifdef GPU_OK
      if(OSSIRenderer::useGPU) {
      cgSetParameter3f(CgData::instance()->basecolor, 0.0, 0.0, 0.0);
      // cgSetParameter3f(CgData::instance()->basecolor, mVertexColor.redF(), mVertexColor.greenF(), mVertexColor.blueF());
      cgSetParameter3f(CgData::instance()->Ka, 0.0, 0.0, 0.0);
      cgSetParameter3f(CgData::instance()->Kd, 0.0, 0.0, 0.0);
      cgSetParameter3f(CgData::instance()->Ks, 0.0, 0.0, 0.0);
      cgSetParameter1f(CgData::instance()->shininess, 0.0);
    }
        #endif
    glColor4f(OSSIRenderer::mFaceCentroidColor.redF(),OSSIRenderer::mFaceCentroidColor.greenF(),OSSIRenderer::mFaceCentroidColor.blueF(),OSSIRenderer::mFaceCentroidColor.alphaF());
    glDepthRange(0.0,1.0-0.00075);
    //object->renderVertices(mVertexThickness);
    gr->renderFaceCentroids( object, OSSIRenderer::mFaceCentroidThickness );
  }

  void drawFaceNormals(OSSIObjectPtr object) {
    glColor4f(OSSIRenderer::mNormalColor.redF(),OSSIRenderer::mNormalColor.greenF(),OSSIRenderer::mNormalColor.blueF(),OSSIRenderer::mNormalColor.alphaF());
    glDepthRange(0.0,1.0-0.00075);
    //object->renderVertices(mVertexThickness);
    gr->renderFaceNormals( object, OSSIRenderer::mNormalThickness, OSSIRenderer::mNormalLength );
  }

  /*
    This checks the render flags and calls the above functions.
    This is not defined as static since it needs access to the render flags
  */
  void drawOverlays(OSSIObjectPtr object) {

    if ( gr->drawWireframe ) drawWireframe(object);
    if ( gr->drawSilhouette ) drawSilhouette(object);
    if ( gr->drawVertices ) drawVertices(object);
    if ( gr->drawFaceNormals ) drawFaceNormals(object);
    if ( gr->drawFaceCentroids ) drawFaceCentroids(object);
  }

    //getters and setters for color and thickness
    void setSilhouetteColor(QColor c){ OSSIRenderer::mSilhouetteColor = c; }
    void setSilhouetteThickness(double t){ OSSIRenderer::mSilhouetteThickness = t; }
    void setWireframeColor(QColor c){	OSSIRenderer::mWireframeColor = c; }
    void setWireframeThickness(double t){ OSSIRenderer::mWireframeThickness = t; }
    void setVertexColor(QColor c){ OSSIRenderer::mVertexColor = c; }
    void setVertexThickness(double t){ OSSIRenderer::mVertexThickness = t; }
    void setFaceCentroidColor(QColor c){ OSSIRenderer::mFaceCentroidColor = c; }
    void setFaceCentroidThickness(double t){ OSSIRenderer::mFaceCentroidThickness = t; }
    void setNormalColor(QColor c){ OSSIRenderer::mNormalColor = c; }
    void setNormalThickness(double t){ OSSIRenderer::mNormalThickness = t; }
    void setNormalLength(double l){ OSSIRenderer::mNormalLength = l; }


};

#endif
