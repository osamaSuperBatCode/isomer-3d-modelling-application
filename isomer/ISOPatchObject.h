/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _ISO_PATCH_OBJECT_H_
#define _ISO_PATCH_OBJECT_H_

#include "ISOPatchFace.h"

class ISOPatchObject;
typedef ISOPatchObject* ISOPatchObjectPtr;

class ISOPatchObject {
protected :

  uint uid;
  ISOPatchFacePtrList patch_list;		// List of patch faces
  ISOPatchFacePtrList::iterator it;
  int patchsize;				 // Size of each patch

public :

  // Default constructor
  ISOPatchObject( uint id ) : uid(id), patch_list(), patchsize(4) { }

  uint id( ) { return uid; }
  int size( ) { return patchsize; }
  const ISOPatchFacePtrList& list( ) { return patch_list; }
  void for_each( void (ISOPatchFace::*func)(void));

    /* - bezier export */
    void objPatchWrite( ostream& o );

private :

  // Copy constructor - private to prevent access
  ISOPatchObject(const ISOPatchObject& tmpo)
    : patch_list(), patchsize(tmpo.patchsize) { }

  // Assignment operator - private to prevent access
  void operator=(const ISOPatchObject& tmpo) { }

public :
  // Destructor
  ~ISOPatchObject() { destroyPatches(); destroyPatchMap(patchMap); }

protected :
  OSSIObjectPtr mObj; // the last obj created from

    ISOPatchMap patchMap;

  // Free the memory allocated for the patches
  void destroyPatches();
  // Build the list of patch faces
  void createPatches( OSSIObjectPtr obj );

public :

  // Set the patch size
  void setPatchSize(int size, OSSIObjectPtr obj = NULL );

  void updateForPatches( OSSIObjectPtr obj );

  // Update the patches
  void updatePatches( OSSIObjectPtr obj = NULL ) {
    if( !obj ) { obj = mObj; }
    if( !obj ) { return; } // never set an obj to update
    updateForPatches(obj);
    createPatches(obj);
  }

  // Render the patches
  void renderPatches(void) {
    glPushMatrix();
    transform();
    for_each(&ISOPatchFace::render);
    glPopMatrix();
  }

  // Render the object using wireframe patches
  void renderWireframePatches(void) {
    glPushMatrix();
    transform();
    for_each(&ISOPatchFace::outline);
    glPopMatrix();
  }

  // Render the object using point patches
  void renderPointPatches(void) {
    glPushMatrix();
    transform();
    for_each(&ISOPatchFace::controlpoints);
    glPopMatrix();
  }

  void renderPatchBoundaries(void) {
    glPushMatrix();
    transform();
    for_each(&ISOPatchFace::patch_boundary);
    glPopMatrix();
  }

  void renderPatchFaceBoundaries(void) {
    glPushMatrix();
    transform();
    for_each(&ISOPatchFace::face_boundary);
    glPopMatrix();
  }

  void renderPatchNormals(void) {
    glPushMatrix();
    transform();
    for_each(&ISOPatchFace::renderNormals);
    glPopMatrix();
  }

  // Compute lighting for patches and the base object
  void computeLighting(OSSIObjectPtr obj, LightPtr lightptr) {
    cout << "ISOPatchObject::computeLighting" << endl;
    computeLighting(obj,lightptr);
    ISOPatchFacePtrList::iterator first = patch_list.begin(), last = patch_list.end();
    ISOPatchFacePtr pfp = NULL;
    while ( first != last ) {
      pfp = (*first); ++first;
      pfp->computeLighting(lightptr);
    }
  }

  /*  bezier export */
  void printPatchCVList( ) const {
    cout << "Patch CVs" << endl;
    ISOPatchFacePtrList::const_iterator first = patch_list.begin(), last = patch_list.end();
    while( first != last ) {
      (*first)->print(cout);
      ++first;
    }
  }

    void transform( ) {
        double mat[16];
        mObj->tr.fillArrayColumnMajor(mat);
        glMultMatrixd(mat);
    }

};


#endif /* #ifndef _ISO_PATCH_OBJECT_H_ */

