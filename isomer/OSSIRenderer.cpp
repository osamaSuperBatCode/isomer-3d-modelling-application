/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software  Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* The Original Code is Copyright (C) 2005 by xxxxxxxxxxxxxx
* All rights reserved.
*
* The Original Code is: all of this file.
*
* Contributor(s): none yet.
*
* ***** END GPL LICENSE BLOCK *****
*
* Short description of this file
*
* name of .hh file containing function prototypes
*
*/

/* $Id: OSSIRenderer.cc,v 4.1 2004/04/18 18:09:06 vinod Exp $ */

// Non-inline function and static variable definitions for OSSIRenderer class

#include "OSSIRenderer.h"

// int OSSIRenderer::render_flags = 0;
bool OSSIRenderer::reverse_object = false;
bool OSSIRenderer::antialiasing = false;
#ifdef GPU_OK
bool OSSIRenderer::useGPU = false;
#endif

QColor OSSIRenderer::mWireframeColor = QColor(0,0,0);									//!< wireframe RGB color
QColor OSSIRenderer::mSilhouetteColor = QColor(0,0,0);								//!< silhouette RGB color
QColor OSSIRenderer::mVertexColor = QColor(0,0,0);										//!< vertex RGB color
QColor OSSIRenderer::mFaceCentroidColor = QColor(0,0,0);							//!< face centroid RGB color
QColor OSSIRenderer::mNormalColor = QColor(0,0,0);										//!< normals RGB color
double OSSIRenderer::mFaceCentroidThickness = 5.0;					//!< face centroid point size
double OSSIRenderer::mNormalThickness = 5.0;								//!< normal line thickness
double OSSIRenderer::mNormalLength = 1.0;										//!< normal length
double OSSIRenderer::mWireframeThickness = 1.0;							//!< wireframe line thickness
double OSSIRenderer::mSilhouetteThickness = 8.0;						//!< silhouette line thickness
double OSSIRenderer::mVertexThickness = 1.5;								//!< vertex point size
