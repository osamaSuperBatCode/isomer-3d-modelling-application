/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*/

#ifdef QCOMPLETER

#include "CommandCompleter.h"
#include <QPixmap> //offscreen image representation
#include <QBitmap> //provides monochrome bitmaps

    /*
    *  Constructor
    *
    * @param m the actionList passed from the MainWindow class after each Mode class is instantiated
    * @param parent the MainWindow parent widget
    * @param f window flags for the QDialog constructor
    *
    */
    CommandCompleter::CommandCompleter ( QWidget *m, QWidget * parent, Qt::WindowFlags f) : QDialog(parent, f) {
        setSizeGripEnabled(false); //disables sizegrip. a sizegrip is that pull thingy which expands text area boxes like in html.

        #ifdef LINUX
        setWindowFlags(Qt::Dialog);
        #else
        setWindowFlags(Qt::SplashScreen);
        #endif
        setModal(true); //no other things can be activated

        this->resize(341,134);

        mQuickCommandLabel = new QLabel(tr("<font color=\"white\">Type a command:</font>"));

        QPalette p = this->palette(); //The palette is used by the widget's style when rendering standard components, and is available as a means to ensure that custom widgets can maintain consistency with the native platform's look and feel.

        p.setBrush(QPalette::Window, QPixmap(QString(":images/commandcompleter.png")));

        this->setPalette(p);

        QPixmap pixmap(":/images/commandcompleter.png");

        this->setMask(pixmap.mask());

        mLineEdit = new QLineEdit(this);
        mCompleter = new QCompleter(this);

        mCompleter->setCaseSensitivity(Qt::CaseInsensitive);
        mCompleter->setCompletionMode(QCompleter::PopupCompletion);
        //loop through the actions added to the widget *m, add the text to a word list
        for (int i = 0; i < m->actions().count(); ++i)
            mWordList << ((QAction*)(m->actions().at(i)))->toolTip();

        mModel = new QStringListModel(mWordList,mCompleter);
        mCompleter->setModel(mModel);
        mLineEdit->setCompleter(mCompleter);

        mLineEdit->setFocusPolicy(Qt::StrongFocus);//tabbing and clicking

        QVBoxLayout *vbox = new QVBoxLayout(this);
        vbox->setContentsMargins(60,40,60,50);
        vbox->addWidget(mQuickCommandLabel);
        vbox->addWidget(mLineEdit);


        connect(mLineEdit, SIGNAL(editingFinished()), this, SLOT(accept()));
    }

#endif
