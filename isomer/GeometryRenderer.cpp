/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
* For Renderation Of Geometric Data in the GL Windows.
*
* GeometryRenderer.h
*
*/

#include <GeometryRenderer.h>
#include <OSSIRenderer.h>


/*

     GeometryRenderer
     functions for rendering edges, faces, vertices of OSSI Objects
     GeometryRenderer
*/

static void transform( OSSIObjectPtr obj ) {
    double mat[16];
    obj->tr.fillArrayColumnMajor(mat);
    glMultMatrixd(mat); //multiply current matrix with arbitary matrix, openGL function.
}

void GeometryRenderer::render( OSSIObjectPtr obj ) const {

    OSSIMaterialPtrList::iterator mp_it; //define iterator variable for materal pointer
    OSSIFacePtrList::iterator fp_it; //define pointer iterator for faces pointer
    glPushMatrix( ); { //glPushMatrix pushes the current matrix stack down by one, duplicating the current matrix
        transform( obj );
        for( mp_it = obj->beginMaterial(); mp_it != obj->endMaterial(); mp_it++ ) {
            OSSIMaterialPtr mat = *mp_it; //terate and set the material in the matrix or
            for( fp_it = mat->faces.begin(); fp_it != mat->faces.end(); fp_it++ ) {
                renderFace( *fp_it ); //iterate and set the faces in matrix
            }
        }
    } glPopMatrix( ); //pops items from the matrix stack
}

void GeometryRenderer::renderFace( OSSIFacePtr of, bool useAttrs ) const {
    #ifdef GPU_OK //gpu related functions, courtesy The Blender Foundation for CgCompute.
    if (useGPU){
        double Ka = of->material()->Ka; // ambience for a set of materials
        double Kd = of->material()->Kd; //diffuse for a set of materials
        double Ks = of->material()->Ks; //specular for a set of materials
        RGBColor basecolor = of->material()->color; //set RGBColor from Color.h to the pointer to the material, which stores the amibence specular and diffuse information into the color function.

        cgSetParameter3f(CgCompute::instance()->basecolor, basecolor.r, basecolor.g, basecolor.b); //fill in the ambience specular and diffuse, allong with rgb values
        cgSetParameter3f(CgCompute::instance()->Ka, Ka, Ka, Ka); //set cgparameter to ka, and feed ka values (check openGL NVIDIA docs for more details.
      cgSetParameter3f(CgCompute::instance()->Kd, Kd, Kd, Kd); // set cgparameter to kd, and feed the values
      cgSetParameter3f(CgCompute::instance()->Ks, Ks, Ks, Ks); //same as above
      cgSetParameter1f(CgCompute::instance()->shininess, 50); //set parameter shininess to the value specefied.
    }
    #endif //if not using GPU and instead using CPU,
    if( useAttrs ) {
        glBeginFace( of->size(), useOutline ); //check below for function definition
    } else {
        glBeginFace( of->size(), false ); //check below for function definition.
    }
    OSSIFaceVertexPtr head, curr; //create obejcts of faceVertices as head and current.
    curr = head = of->front(); //set the current to head, viz, inshort an STL list type access to the head via the front() function defined in OSSIFace.h
    renderFaceVertex( curr, useAttrs ); //render the face vertex using the current selected vertex and the boolean which sets the state to true or false.
    curr = curr->next(); //set current to next face vertex pointer.
    while( curr != head ) { //while the current vertex is not the starting vertex
        renderFaceVertex( curr, useAttrs ); //render the faceVertices
        curr = curr->next(); //select next vertex
    }
    glEnd( ); //glEnd defines the limits of the vertices that define a primitive or a group of like primitives.
}

void GeometryRenderer::renderFaceVertex( OSSIFaceVertexPtr ofvp, bool useAttrs ) const {
    if( ofvp->vertex ) {
        if( useAttrs ) {
            if( useNormal ) { glNormal3dv( (ofvp->normal).getCArray() ); } //function to get normal, with the ofvp normal, and an elem return that gives us 3 elements in Vector3d class.
            if( useLighting && useMaterial ) {
                // Lighting and solid color material
                RGBColor rgb(renderColor[0], renderColor[1], renderColor[2] );
                // RGBColor rgb( 0.1, 0.1, 0.1 );
                #ifdef GPU_OK
                if (!useGPU){
                    rgb = product(rgb, ofvp->color); // this multiplies corresponding elements together (e.g. RGBColor(r1*r2, g1*g2, b1*b2)  )
                }
                #else
                rgb = product(rgb, ofvp->color); // this multiplies corresponding elements together (e.g. RGBColor(r1*r2, g1*g2, b1*b2)  )
                #endif
                glColor4f( rgb.r, rgb.g, rgb.b, renderColor[3]); //4th is alpha.
            } else {
                if( useLighting ) { glColor( ofvp->color ); } // Actually means use lighting
                if( useMaterial ) { glColor4dv(renderColor); } //sets color from already existing color array.
                if ( useColorable ) {
                    RGBColor rgb(ofvp->getFacePtr()->material()->color );
                    glColor4f( rgb.r, rgb.g, rgb.b, renderColor[3]);
                }
            }
            if( useTexture ) {


                if( isReversed )
                    glTexCoord2d(1.0-ofvp->texcoord[0],1.0-ofvp->texcoord[1]); //sets texture co-ords.
                else
                    glTexCoord2d(1.0-ofvp->texcoord[0],1.0-ofvp->texcoord[1]);
            }
        }
        OSSIVertexPtr vp = ofvp->vertex;
        glVertex3dv( (vp->coords).getCArray() ); //specefies a vertex.
    }
}

void GeometryRenderer::renderEdge( OSSIEdgePtr oep ) const {
    OSSIFaceVertexPtr fvptr1, fvptr2;
    oep->getFaceVertexPointers( fvptr1, fvptr2 );
    glVertex3dv((fvptr1->vertex->coords).getCArray());
    glVertex3dv((fvptr2->vertex->coords).getCArray());
}

void GeometryRenderer::renderVertex( OSSIVertexPtr ovp ) const {
    glBegin(GL_POINTS); { //GL_POINTS is OpenGL's Primitive for creating an OpenGl Point.
        glVertex3dv( ovp->coords.getCArray() );
        } glEnd();
}

void GeometryRenderer::renderVertices( OSSIObjectPtr obj, double size ) const {
    OSSIVertexPtrList::iterator it;
    // Just render all the vertices with specified point size
    glPushMatrix(); {
        transform( obj );
        glPointSize( size ); //specefies diameter of rasterized points.
        for( it = obj->beginVertex(); it != obj->endVertex(); it++ ) {
            renderVertex( *it );
        }
        glPointSize(1.0);
    } glPopMatrix();
}

void GeometryRenderer::renderEdges( OSSIObjectPtr obj, double width ) const {
    OSSIEdgePtrList::iterator it;
// Just render all the edges with specified line width
    glPushMatrix(); {
        transform( obj );
        glLineWidth( width );
        glBegin(GL_LINES);
        for( it = obj->beginEdge(); it != obj->endEdge(); it++ ) {
            renderEdge( *it );
        }
        glEnd();
        glLineWidth(1.0);
        glPopMatrix();
    }
}


void GeometryRenderer::renderFaceCentroids( OSSIObjectPtr obj, double size ) const {
    OSSIFacePtrList::iterator it;
    glPushMatrix(); {
        transform( obj );
        glPointSize( size );
        glBegin(GL_POINTS);
        for( it = obj->beginFace(); it != obj->endFace(); it++ ) {
            (*it)->updateCentroid( );
            glVertex3dv( (*it)->centroid.getCArray() );
        }
        glEnd();
        glPointSize(1.0);
    }	glPopMatrix();
}

void GeometryRenderer::renderFaceNormals( OSSIObjectPtr obj, double width, double length ) const {
    OSSIFacePtrList::iterator it;
    glPushMatrix(); {
        transform( obj );
        glLineWidth( width );
        glBegin(GL_LINES);
        for( it = obj->beginFace(); it != obj->endFace(); it++ ) {
            (*it)->updateCentroid( );
            Vector3d norm = (*it)->centroid + ((*it)->getNormal() * length);
            glVertex3dv( (*it)->centroid.getCArray() );
            glVertex3dv( norm.getCArray() );
        }
        glEnd();
        glLineWidth(1.0);
    }	glPopMatrix();
}

// Call glBegin with the appropriate macro depending on no of vertices
// This is for filled polygons.
void GeometryRenderer::glBeginFace( int num, bool outline ) {
    if( outline ) {
        switch ( num ) {
            case 0 :
            return;
            case 1 :
            glBegin(GL_POINTS); break;
            case 2 :
            glBegin(GL_LINES); break;
            default :
            glBegin(GL_LINE_LOOP);
        }
    } else {
        switch ( num ) {
            case 0 :
            return;
            case 1 :
            glBegin(GL_POINTS); break;
            case 2 :
            glBegin(GL_LINES); break;
            case 3 :
            glBegin(GL_TRIANGLES); break;
            case 4 :
            glBegin(GL_QUADS); break;
            default :
            glBegin(GL_POLYGON);
        }
    }
}

// Singleton Stuff - so that only one instance exists at a single time, throughout the lifecycle of the application.
GeometryRenderer* GeometryRenderer::mInstance = 0;

GeometryRenderer* GeometryRenderer::instance( ) {
    // ensures only 1 instance (singleton)
    if ( !mInstance )
        mInstance = new GeometryRenderer;
    return mInstance;
}

