/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _ISO_PATCH_FACE_H_
#define _ISO_PATCH_FACE_H_

#include "ISOPatch.h"

/* Class for a Isomer Bezier Face containing several Patches */

class ISOPatchFace;
typedef ISOPatchFace *ISOPatchFacePtr;

typedef list<ISOPatchFacePtr> ISOPatchFacePtrList;
typedef vector<ISOPatchFacePtr> ISOPatchFacePtrArray;

typedef list<ISOPatchFace> ISOPatchFaceList;
typedef vector<ISOPatchFace> ISOPatchFaceArray;

class ISOPatchFace {
protected :

  int patchsize;		// Grid size of each patch in the face
  ISOPatchArray patcharray;	// Array of Bezier patches for this face
  OSSIFacePtr OSSIface;	// OSSIFace associated with this Bezier face

  // Resize the patch array depending on number of corners in the OSSIFace
  void resizePatchArray(void);

public :

  // Default & 1-arg constructor
  ISOPatchFace(int psize=4)
    : patchsize(psize), patcharray(), OSSIface(NULL) {}

  // Destructor
  ~ISOPatchFace() {}

  // Copy constructor
  ISOPatchFace(const ISOPatchFace& ISOpf)
    : patchsize(ISOpf.patchsize), patcharray(ISOpf.patcharray), OSSIface(ISOpf.OSSIface) {}

  // Assignment operator
  ISOPatchFace&  operator = (const ISOPatchFace& ISOpf) {
    patchsize = ISOpf.patchsize; patcharray = ISOpf.patcharray; OSSIface = ISOpf.OSSIface;
    return (*this);
  }

  // Resize all patches
  void resizePatches(int psize);

  // Set the OSSIFace pointer
  void setOSSIFace(OSSIFacePtr fp) {
    OSSIface = fp;
    resizePatchArray();
  }

  // Create the patches using face information
  void createPatches(ISOPatchMap &patchMap);

  // Adjust the edge points for each patch in the face
  void adjustEdgePoints(ISOPatchMap &patchMap);

  // Compute lighting for the patches in this face
  void computeLighting(LightPtr lightptr) {
    OSSIMaterialPtr matl;
    for (uint i=0; i < patcharray.size(); ++i) {
      matl = OSSIface->material();
      patcharray[i].computeLighting(matl->color,matl->Ka,matl->Kd,matl->Ks,lightptr);
    }
  }

  // Render the patches using filled polygons
  void render(void) {for (uint i=0; i < patcharray.size(); ++i) {
      patcharray[i].render();
    }
  }

  // Render the patches using outlined polygons
  void outline(void) {
    for (uint i=0; i < patcharray.size(); ++i) patcharray[i].outline();
  }

  // Render the boundaries of the patches
  void patch_boundary(void) {
    for (uint i=0; i < patcharray.size(); ++i) patcharray[i].patch_boundary();
  }

  // Render the boundaries of the patches
  void face_boundary(void) {
    for (uint i=0; i < patcharray.size(); ++i) patcharray[i].face_boundary();
  }

  // Render the control grids of the patches
  void controlgrid(void) {
    for (uint i=0; i < patcharray.size(); ++i) patcharray[i].controlgrid();
  }

  // Show the conrtol points
  void controlpoints(void) {
    for (uint i=0; i < patcharray.size(); ++i) patcharray[i].controlpoints();
  }

  // Show the normals
  void renderNormals(void) {
    for (uint i=0; i < patcharray.size(); ++i) patcharray[i].renderNormals();
  }

  /* stuart - bezier export */
  int print( ostream &stream ) {
    for( int i = 0; i < patcharray.size(); i++ ) {
      patcharray[i].printControlPoints( stream );
      stream << std::endl;
    }
    return patcharray.size( );
  }
};

#endif /* #ifndef _ISO_PATCH_FACE_H_ */

