/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
* Implements ConicalMode
*
* ConicalMode.h
*
*/

/****************************************************************************
 **Implements the Conical Mode.
 **
 ****************************************************************************/
#include <QtGui>

#include "ConicalMode.h"

/*

     ConicalMode
     Conical Operations GUI elements.

*/

ConicalMode::ConicalMode(QWidget *parent, QShortcutManager *sm)
        : QWidget(parent)
{
    setParent(0);
    mParent = parent;

    //create the stacked widget and all child widget pages
    mCutbyEdgeWidget = new QWidget;
  mCutbyVertexWidget = new QWidget;
  mCutbyFaceWidget = new QWidget;

    setupCutbyEdge();
    setupCutbyVertex();
    setupCutbyFace();


    mCutbyEdgeAction = new QAction(QIcon(":images/sculpting-cutedges.png"),tr("Cut by Edge"),this);
    mCutbyEdgeAction->setCheckable(true);
    sm->registerAction(mCutbyEdgeAction, "Conical Operations", "");
    mCutbyEdgeAction->setStatusTip(tr("Enter Cut by Edge Mode"));
    mCutbyEdgeAction->setToolTip(tr("Cut by Edge Mode"));
    connect(mCutbyEdgeAction, SIGNAL(triggered()), this, SLOT(triggerCutbyEdge()));

    mCutbyVertexAction = new QAction(QIcon(":images/sculpting-cutvertices.png"),tr("Cut by Vertex"),this);
    mCutbyVertexAction->setCheckable(true);
    sm->registerAction(mCutbyVertexAction, "Conical Operations", "");
    mCutbyVertexAction->setStatusTip(tr("Enter Cut by Vertex Mode"));
    mCutbyVertexAction->setToolTip(tr("Cut by Vertex Mode"));
    connect(mCutbyVertexAction, SIGNAL(triggered()), this, SLOT(triggerCutbyVertex()));

    mCutbyFaceAction = new QAction(QIcon(":images/sculpting-cutfaces.png"),tr("Cut by Face"),this);
    mCutbyFaceAction->setCheckable(true);
    sm->registerAction(mCutbyFaceAction, "Conical Operations", "");
    mCutbyFaceAction->setStatusTip(tr("Enter Cut by Face Mode"));
    mCutbyFaceAction->setToolTip(tr("Cut by Face Mode"));
    connect(mCutbyFaceAction, SIGNAL(triggered()), this, SLOT(triggerCutbyFace()));


}

QMenu *ConicalMode::getMenu(){

    mConicalMenu = new QMenu(tr("Conical"));

    mConicalMenu->addAction(mCutbyEdgeAction);
    mConicalMenu->addAction(mCutbyVertexAction);
    mConicalMenu->addAction(mCutbyFaceAction);


    return mConicalMenu;
}

QDoubleSpinBox *ConicalMode::createDoubleSpinBox(QGridLayout *layout, QLabel *label, QString s, double low, double high, double step, double value, double decimals, int row, int col){
    label->setText(s);
    QDoubleSpinBox *spinbox = new QDoubleSpinBox(this);
    spinbox->setAccelerated(true);
    spinbox->setRange(low, high);
    spinbox->setSingleStep(step);
    spinbox->setValue(value);
    spinbox->setDecimals(decimals);
    spinbox->setMaximumSize(75,25);
    layout->addWidget(label,row,col);
  layout->addWidget(spinbox,row,col+1);

    return spinbox;
}

void ConicalMode::triggerCutbyEdge(){

    ((MainWindow*)mParent)->setToolOptions(mCutbyEdgeWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::CutEdge);
}

void ConicalMode::triggerCutbyVertex(){

    ((MainWindow*)mParent)->setToolOptions(mCutbyVertexWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::CutVertex);
}

void ConicalMode::triggerCutbyFace(){

    ((MainWindow*)mParent)->setToolOptions(mCutbyFaceWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::CutFace);
}


void ConicalMode::addActions(QActionGroup *actionGroup, QToolBar *toolBar, QStackedWidget *stackedWidget){

    actionGroup->addAction(mCutbyEdgeAction);
    actionGroup->addAction(mCutbyVertexAction);
    actionGroup->addAction(mCutbyFaceAction);

    toolBar->addAction(mCutbyEdgeAction);
    toolBar->addAction(mCutbyVertexAction);
    toolBar->addAction(mCutbyFaceAction);

    stackedWidget->addWidget(mCutbyEdgeWidget);
    stackedWidget->addWidget(mCutbyVertexWidget);
    stackedWidget->addWidget(mCutbyFaceWidget);

}

//void ConicalMode::toggleGlobalCut(int state){

  //  cutbyEdgeGlobalCheckBox->setChecked(state);
   // cutbyVertexGlobalCheckBox->setChecked(state);
   // cutbyFaceGlobalCheckBox->setChecked(state);

 // ((MainWindow*)mParent)->toggleGlobalCut(state);
//}

void ConicalMode::setupCutbyEdge(){

    mCutbyEdgeLayout = new QGridLayout;
    mCutbyEdgeLayout->setVerticalSpacing(1);
    mCutbyEdgeLayout->setHorizontalSpacing(1);

    //offset
    cutbyEdgeOffsetLabel = new QLabel(this);
    cutbyEdgeOffsetSpinBox = createDoubleSpinBox(mCutbyEdgeLayout, cutbyEdgeOffsetLabel, tr("Offset:"), 0.0, 2.0, 0.01, 0.25, 3, 0,0);


    /*cutbyEdgeGlobalCheckBox = new QCheckBox(tr("Global Cut"),this);
    cutbyEdgeGlobalCheckBox->setChecked(Qt::Unchecked);
    connect(cutbyEdgeGlobalCheckBox, SIGNAL(stateChanged(int)), this, SLOT(toggleGlobalCut(int)));
    mCutbyEdgeLayout->addWidget(cutbyEdgeGlobalCheckBox, 1,0,1,2);*/

    performCuttingEdgeButton = new QPushButton(tr("Perform Cutting"), this);
    connect(performCuttingEdgeButton, SIGNAL(clicked()), ((MainWindow*)mParent),SLOT(performCutting()) );
    mCutbyEdgeLayout->addWidget(performCuttingEdgeButton,2,0,1,2);


    mCutbyEdgeLayout->setRowStretch(3,1);
    mCutbyEdgeLayout->setColumnStretch(2,1);
    mCutbyEdgeWidget->setWindowTitle(tr("Cut By Edge"));
    mCutbyEdgeWidget->setLayout(mCutbyEdgeLayout);


}

void ConicalMode::setupCutbyVertex(){

    mCutbyVertexLayout = new QGridLayout;
    mCutbyVertexLayout->setVerticalSpacing(1);
    mCutbyVertexLayout->setHorizontalSpacing(1);

    //offset
    cutbyVertexOffsetLabel = new QLabel(this);
    cutbyVertexOffsetSpinBox = createDoubleSpinBox(mCutbyVertexLayout, cutbyVertexOffsetLabel, tr("Offset:"), 0.0, 2.0, 0.01, 0.25, 3, 0,0);


    /*cutbyVertexGlobalCheckBox = new QCheckBox(tr("Global Cut"),this);
    cutbyVertexGlobalCheckBox->setChecked(Qt::Unchecked);
    connect(cutbyVertexGlobalCheckBox, SIGNAL(stateChanged(int)), this, SLOT(toggleGlobalCut(int)));
    mCutbyVertexLayout->addWidget(cutbyVertexGlobalCheckBox, 1,0,1,2);*/

    performCuttingVertexButton = new QPushButton(tr("Perform Cutting"), this);
    connect(performCuttingVertexButton, SIGNAL(clicked()), ((MainWindow*)mParent),SLOT(performCutting()) );
    mCutbyVertexLayout->addWidget(performCuttingVertexButton,2,0,1,2);

    mCutbyVertexLayout->setRowStretch(3,1);
    mCutbyVertexLayout->setColumnStretch(2,1);
    mCutbyVertexWidget->setWindowTitle(tr("Cut By Vertex"));
    mCutbyVertexWidget->setLayout(mCutbyVertexLayout);



}



void ConicalMode::setupCutbyFace(){

    mCutbyFaceLayout = new QGridLayout;
    mCutbyFaceLayout->setVerticalSpacing(1);
    mCutbyFaceLayout->setHorizontalSpacing(1);

    //offset
    cutbyFaceOffsetLabel = new QLabel(this);
    cutbyFaceOffsetSpinBox = createDoubleSpinBox(mCutbyFaceLayout, cutbyFaceOffsetLabel, tr("Offset:"), 0.0, 2.0, 0.01, 0.25, 3, 0,0);


   /* cutbyFaceGlobalCheckBox = new QCheckBox(tr("Global Cut"),this);
    cutbyFaceGlobalCheckBox->setChecked(Qt::Unchecked);
    connect(cutbyFaceGlobalCheckBox, SIGNAL(stateChanged(int)), this, SLOT(toggleGlobalCut(int)));
    mCutbyFaceLayout->addWidget(cutbyFaceGlobalCheckBox, 1,0,1,2);*/

    performCuttingFaceButton = new QPushButton(tr("Perform Cutting"), this);
    connect(performCuttingFaceButton, SIGNAL(clicked()), ((MainWindow*)mParent),SLOT(performCutting()) );
    mCutbyFaceLayout->addWidget(performCuttingFaceButton,2,0,1,2);

    mCutbyFaceLayout->setRowStretch(3,1);
    mCutbyFaceLayout->setColumnStretch(2,1);
    mCutbyFaceWidget->setWindowTitle(tr("Cut By Face"));
    mCutbyFaceWidget->setLayout(mCutbyFaceLayout);



}



void ConicalMode::retranslateUi(){

    cutbyFaceOffsetLabel->setText(tr("Offset:"));
    performCuttingFaceButton->setText(tr("Perform Cutting"));
    mCutbyFaceWidget->setWindowTitle(tr("Cut By Face"));

    cutbyVertexOffsetLabel->setText(tr("Offset:"));
    performCuttingVertexButton->setText(tr("Perform Cutting"));
    mCutbyVertexWidget->setWindowTitle(tr("Cut By Vertex"));

    cutbyEdgeOffsetLabel->setText(tr("Offset:"));
    performCuttingEdgeButton->setText(tr("Perform Cutting"));
    mCutbyEdgeWidget->setWindowTitle(tr("Cut By Edge"));

    mConicalMenu->setTitle(tr("Conical"));

    mCutbyEdgeAction->setText(tr("Cut by Edge"));
    mCutbyEdgeAction->setStatusTip(tr("Enter Cut by Edge Mode"));
    mCutbyEdgeAction->setToolTip(tr("Cut by Edge Mode"));

    mCutbyVertexAction->setText(tr("Cut by Vertex"));
    mCutbyVertexAction->setStatusTip(tr("Enter Cut by Vertex Mode"));
    mCutbyVertexAction->setToolTip(tr("Cut by Vertex Mode"));

    mCutbyFaceAction->setText(tr("Cut by Face"));
    mCutbyFaceAction->setStatusTip(tr("Enter Cut by Face Mode"));
    mCutbyFaceAction->setToolTip(tr("Cut by Face Mode"));



}

