/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
* Short description of this file
*
* name of .hh file containing function prototypes
*
*/




#include <QApplication> //The QApplication class manages the GUI application's control flow and main settings.
#include <QSplashScreen> //provides splash screen
#include <QMenu> //provides menus for menubar
#include <QAction> //provdes actions that can be inserted into widgets (f1)
#include <QMenuBar> //provides menubar
#include <QMainWindow> //main container window is provided by this.
#include <QMessageBox> //popup alert messages provided by this.
#include <QFileOpenEvent> //when a request to open a file is fired.
// #include <QWaitCondition>

#include "MainWindow.h"
#include "Isomer.h"

#define max(a, b)   (((a) < (b)) ? (b) : (a))
#define min(x1,x2) ((x1)<(x2)?(x1):(x2))

// #ifdef ISOMER_VERSION
// ISOMER_VERSION 1.0
// #endif





#include <iostream>
int main( int argc, char **argv ) {
    Isomer app( argc, argv, true );
    QApplication::setWindowIcon(QIcon(":/images/Isomer.png"));

    //this is a preliminary version of a splash screen functionality.
    //the app opens so quickly it is hardly shown, so i will consider adding a delay
    QPixmap pixmap(":/images/splash.png");// import splashscreen
    QSplashScreen *splash = new QSplashScreen(pixmap, Qt::WindowStaysOnTopHint); //call splashscreen, always stay on top splashscreen.
    splash->setMask(pixmap.mask()); //extract a bitmap mask from the alpha channel, and set it to splashscreen mask
    splash->setGeometry(300,200,splash->width(),splash->height()); //set x=300, y= 200 and then set that to splashscreen width and height
    splash->show(); //show splashscreen
    // splash->showMessage("doin stuff...");
    app.processEvents(); //load app, process events for main app
    //artificial delay for now to debug, and to give mad props to the developers
    // splash->finish(app.getMainWindow());
    QTimer::singleShot(1000, splash, SLOT(hide())); //This static function calls a slot after a given time interval.


    return app.exec( );
    //Enters the main event loop and waits until exit() is called, then returns the value that was set to exit() (which is 0 if exit() is called via quit()).
}
