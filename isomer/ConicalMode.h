/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

/****************************************************************************
 ** Conical Mode Header File.
 **
 ****************************************************************************/

#ifndef CONICALMODE_H
#define CONICALMODE_H

#include <QWidget>

#include <QGridLayout>
#include <QStackedWidget>
#include <QComboBox>

#include "MainWindow.h"

class MainWindow;

class ConicalMode : public QWidget {
    Q_OBJECT

public:
    ConicalMode(QWidget *parent, QShortcutManager *sm);
    void addActions(QActionGroup *actionGroup, QToolBar *toolBar, QStackedWidget *stackedWidget);
    QMenu *getMenu();
    void retranslateUi();

    QDoubleSpinBox *createDoubleSpinBox(QGridLayout *layout, QLabel *label, QString s, double low, double high, double step, double value, double decimals, int row, int col);

    QWidget *mCutbyEdgeWidget;
    QWidget *mCutbyVertexWidget;
    QWidget *mCutbyFaceWidget;

    QAction *mCutbyEdgeAction;
    QAction *mCutbyVertexAction;
    QAction *mCutbyFaceAction;


    QGridLayout *mCutbyEdgeLayout;
    QGridLayout *mCutbyVertexLayout;
    QGridLayout *mCutbyFaceLayout;


protected:
    void setupCutbyEdge();
    void setupCutbyVertex();
    void setupCutbyFace();


public slots:


    void triggerCutbyEdge();
    void triggerCutbyVertex();
    void triggerCutbyFace();

private:

    QWidget *mParent;
    QMenu *mConicalMenu;

    QDoubleSpinBox *cutbyEdgeOffsetSpinBox;
    QDoubleSpinBox *cutbyVertexOffsetSpinBox;
    QDoubleSpinBox *cutbyFaceOffsetSpinBox;

    QCheckBox *cutbyEdgeGlobalCheckBox;
    QCheckBox *cutbyEdgeCutSelectedCheckBox;
    QCheckBox *cutbyVertexGlobalCheckBox;
    QCheckBox *cutbyVertexCutSelectedCheckBox;

    QCheckBox *cutbyFaceGlobalCheckBox;
    QCheckBox *cutbyFaceCutSelectedCheckBox;

    QLabel *cutbyEdgeOffsetLabel;
    QLabel *cutbyVertexOffsetLabel;
    QLabel *cutbyFaceOffsetLabel;

    QPushButton *performCuttingEdgeButton;
    QPushButton *performCuttingFaceButton;
    QPushButton *performCuttingVertexButton;

};

#endif
