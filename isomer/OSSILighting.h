/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSI_LIGHTING_H_
#define _OSSI_LIGHTING_H_

// Subroutines for lighting computations in OSSI

#include "ISOPatchObject.h"
#include <OSSIObject.h>

#include <QApplication>
#include <QProgressDialog>

#include "CgCompute.h"

#ifdef GPU_OK
using namespace Cg;
#endif // GPU_OK

void computeLighting( OSSIFacePtr fp, LightPtr lightptr, bool usegpu = false);
void computeLighting( OSSIObjectPtr obj, ISOPatchObjectPtr po, LightPtr lightptr, bool usegpu = false );

#endif /* #ifndef _OSSI_LIGHTING_H_ */

