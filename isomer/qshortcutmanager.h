/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/



#ifndef _QSHORTCUT_MANAGER_H_
#define _QSHORTCUT_MANAGER_H_

#include "qcumber.h"

/*
    qshortcutmanager.h
    Definition of the QShortcutManager class

    QShortcutManager
*/

#include <QHash>
#include <QList>
#include <QObject>
#include <QString>
#include <QDomElement>
#include <QtXml>

class QAction;
class QDomDocument;
class QShortcutDialog;

class QCUMBER_EXPORT QShortcutManager : public QObject
{
    Q_OBJECT

    friend class QShortcutDialog;

    public:
        QShortcutManager();
        virtual ~QShortcutManager();

        QShortcutDialog* getShortcutDialog();

        void applyAll();
        void apply(const QString& s, const QString& t);

        bool contains(QAction *a) const;

        void registerAction(QAction *a, const QString& cxt, const QString& def);
        void unregisterAction(QAction *a);

    public slots:
        void readXml();
        void writeXml();

        void configure();
        void languageChanged(const QString& lang);

    private slots:
        void destroyed(QObject *o);

    private:
        QString file(const QString& lang);
        QDomElement node(QAction *a, const QString& cxt);
        QDomElement node(const QString& n, const QString& cxt);

        QString sLang;

        QDomDocument *pDoc;

        QHash<QString, QString> m_shortcuts;
        QHash<QString, QList<QAction*> > m_actions;

        QShortcutDialog *pDialog;
};

#endif
// _QSHORTCUT_MANAGER_H_
