/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _ISOMER_H
#define _ISOMER_H

#include <QApplication>
#include <QEvent>
//
#include "MainWindow.h"

class Isomer : public QApplication {
    Q_OBJECT

private:
        MainWindow *mainWindow;


public:
    Isomer( int & argc, char ** argv, bool GUIenabled );
    ~Isomer();
    MainWindow *getMainWindow();


protected:
    bool event(QEvent *event);

};

#endif
