/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
* ALL YOUR GL ARE BELONG TO US! no seriously, does the work of Gl window
*
* GLWidget.h
*
*/

#include "GLWidget.h"
#include "OSSISelection.h"

/*

  GLWidget
  The OpenGL viewport.

  This also handles selection


*/

OSSILocatorPtrArray GLWidget::sel_lptr_array;


GLWidget::GLWidget(int w, int h, OSSIRendererPtr rp, QColor color, QColor vcolor, OSSIObjectPtr op, const QGLFormat & format, QWidget * parent )
  : 	QGLWidget(format, parent, NULL), /*viewport(w,h,v),*/ object(op), patchObject(NULL), renderer(rp), renderObject(true),
    mRenderColor(color), mViewportColor(vcolor),/*grid(ZX,20.0,10),*/ showgrid(false), showaxes(false), mUseGPU(false), mAntialiasing(true) {
  mParent = parent;

  //take cross product of lookat and up vector - and you're given one that looks right...
  //then cross that with the lookat and you get the real up
  Vector3d Aim(0,0,0);
  Vector3d Pos(10,10,10);
  Vector3d Up(-0.40824829046,0.81649658093,-0.40824829046);

  mCamera = new PerspCamera( Vector3d(10,10,10), Vector3d(0,0,0), Vector3d(0,1,0));
}

GLWidget::~GLWidget(){
#ifdef GPU_OK
  cgDestroyProgram(cg->vertProgram);
  cgDestroyProgram(cg->fragProgram);
  cgDestroyContext(cg->context );
#endif
}

void GLWidget::redraw() {
  repaint(); //qt's repainting or reedrawing GUI, wrapped in a convinient function with a reasonable name.
}

void GLWidget::initializeGL( ) {


  setAutoBufferSwap( true );
  glClearColor(mViewportColor.redF(),mViewportColor.greenF(),mViewportColor.blueF(),mViewportColor.alphaF());
  mCamera->SetProjection(width(),height());

  if (mAntialiasing){
    glEnable( GL_LINE_SMOOTH );
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);	// Set Line Antialiasing
  }
  else {
    glDisable( GL_LINE_SMOOTH );
  }

  glShadeModel(GL_SMOOTH);
  glEnable(GL_BLEND);	// Enable Blending
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);// Type Of Blending To Use

  //initialize light position and color
  plight.position.set(50,25,0);
  plight.warmcolor.set(1,1,0.6);
  plight.coolcolor.set(0.2,0.2,0.4);
  plight.intensity = 2.0;

  mGlobalAmbient = QColor(0,0,0);

  //enable gl lighting for use with cg functions
#ifdef GPU_OK
  enableGLLights();
#endif

  mShowVertexIDs = false;
  mShowEdgeIDs   = false;
  mShowFaceIDs   = false;
  mShowFaceVertexIDs = false;
  mShowSelectedIDs = false;
  mShowHUD = false;
  mBrushSize = 2.5;
  mShowBrush = false;
  mShowSelectionWindow = false;
  setMouseTracking(true);

  locatorPtr = new OSSILocator();

#ifdef GPU_OK
  if (mUseGPU)
    initCg();
#endif

}

#ifdef GPU_OK
void GLWidget::enableGLLights(){

}
#endif

#ifdef GPU_OK

void GLWidget::initCg( ) {
  if (mUseGPU){
    cg = CgData::instance();
    // Create Context
    cg->context = cgCreateContext( );
    checkForCgError("creating context");
    // Vertex Profile
    cg->vertProfile = cgGLGetLatestProfile( CG_GL_VERTEX );
    cgGLSetOptimalOptions( cg->vertProfile );
    checkForCgError("selecting vertex profile");
    // Frag Profile
    cg->fragProfile = cgGLGetLatestProfile( CG_GL_FRAGMENT );
    cgGLSetOptimalOptions( cg->fragProfile );
    checkForCgError("selecting fragment profile");
    // Vertex Program
    char *programName = new char[256];
    sprintf( programName, "%s", "vertShader.cg" );
    cg->vertProgram = cgCreateProgramFromFile( cg->context, CG_SOURCE, programName, cg->vertProfile, NULL, NULL );
    checkForCgError("creating vertex program from file");
    // Fragment Program
    sprintf( programName, "%s", "fragShader.cg" );
    cg->fragProgram = cgCreateProgramFromFile( cg->context, CG_SOURCE, programName, cg->fragProfile, NULL, NULL );
    checkForCgError("creating fragment program from file");
    delete [] programName;
    programName = 0;

    // Bind Variables to Cg Parameters
    cg->camToWorld = cgGetNamedParameter( cg->vertProgram, "camToWorld");
    cg->camToWorldIT = cgGetNamedParameter( cg->vertProgram, "camToWorldIT");
    cg->worldToLight = cgGetNamedParameter( cg->vertProgram, "worldToLight");


    cg->attenDegrees = cgGetNamedParameter( cg->fragProgram, "attenDegrees" );
    cg->eyePosition = cgGetNamedParameter( cg->vertProgram, "eyePosition" );



    //from book
    cg->globalAmbient = cgGetNamedParameter( cg->vertProgram, "globalAmbient" );

    //lighting
    cg->lightWarmColor = cgGetNamedParameter( cg->vertProgram, "lightWarmColor" );
    cg->lightCoolColor = cgGetNamedParameter( cg->vertProgram, "lightCoolColor" );
    cg->lightIntensity = cgGetNamedParameter( cg->vertProgram, "lightIntensity" );
    cg->lightPosition  = cgGetNamedParameter( cg->vertProgram, "lightPosition" );
    //object material
    cg->Kd = cgGetNamedParameter( cg->vertProgram, "Kd" );
    cg->Ka = cgGetNamedParameter( cg->vertProgram, "Ka" );
    cg->Ks = cgGetNamedParameter( cg->vertProgram, "Ks" );
    cg->basecolor = cgGetNamedParameter( cg->vertProgram, "basecolor" );
    cg->shininess = cgGetNamedParameter( cg->vertProgram, "shininess" );

    cgGLLoadProgram( cg->vertProgram );
    checkForCgError("loading vertex program");
    cgGLLoadProgram( cg->fragProgram );
    checkForCgError("loading fragment program");
  }
}

#endif // GPU_OK

void GLWidget::paintEvent(QPaintEvent *event){



  QPainter painter;
  painter.begin(this);
  painter.setRenderHint(QPainter::Antialiasing);

  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();



  if( renderer ){
    renderer->initialize();
  }

  glClearColor(mViewportColor.redF(),mViewportColor.greenF(),mViewportColor.blueF(),mViewportColor.alphaF());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glEnable(GL_DEPTH_TEST);
  \
  mCamera->SetProjection(width(),height());

  // Draw the axes if required. Use thick lines
  if ( showaxes ) {
    glLineWidth(3.0);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(2,0xAAAA);
    glBegin(GL_LINES);
    glColor3f(0.5,0.25,0.25);                     // X Axis - Red
    glVertex3f(0,0,0); glVertex3f(-10,0,0);
    glColor3f(0.25,0.5,0.25);                     // Z Axis - Green
    glVertex3f(0,0,0); glVertex3f(0,0,-10);
    glColor3f(0.25,0.25,0.5);                     // Y Axis - Blue
    glVertex3f(0,0,0); glVertex3f(0,-10,0);
    glEnd();
    glDisable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
    glColor3f(0.6,0.25,0.25);                     // X Axis - Red
    glVertex3f(0,0,0); glVertex3f(10,0,0);
    glColor3f(0.25,0.6,0.25);                     // Z Axis - Green
    glVertex3f(0,0,0); glVertex3f(0,0,10);
    glColor3f(0.25,0.25,0.6);                     // Y Axis - Blue
    glVertex3f(0,0,0); glVertex3f(0,10,0);
    glEnd();
    glLineWidth(3.0);
  }
  glDepthRange(0,1);

  if ( renderer ) {
#ifdef GPU_OK
    if (mUseGPU){
      cgGLBindProgram( cg->vertProgram );
      checkForCgError("binding vertex program");
      cgGLBindProgram( cg->fragProgram );
      checkForCgError("binding fragment program");

      cgGLEnableProfile( cg->vertProfile );
      checkForCgError("enabling vertex profile");
      cgGLEnableProfile( cg->fragProfile );
      checkForCgError("enabling fragment profile");

      //set cg parameters here

      //global ambient light
      cgGLSetParameter3f(cg->globalAmbient, mGlobalAmbient.redF(), mGlobalAmbient.greenF(), mGlobalAmbient.blueF() );
      //send lighing info
      cgGLSetParameter3f(cg->lightWarmColor, plight.warmcolor.r, plight.warmcolor.g, plight.warmcolor.b );
      cgGLSetParameter3f(cg->lightCoolColor, plight.coolcolor.r, plight.coolcolor.g, plight.coolcolor.b );
      cgGLSetParameter1f(cg->lightIntensity, plight.intensity);
      cgGLSetParameter3f(cg->lightPosition, plight.position[0], plight.position[1], plight.position[2] );
      //object material
      cgSetParameter3f(cg->basecolor, 0.0, 0.0, 0.0);
      cgSetParameter3f(cg->Ka, mRenderColor.redF(), mRenderColor.greenF(), mRenderColor.blueF());
      cgSetParameter3f(cg->Kd, 0.0, 0.0, 0.0);
      cgSetParameter3f(cg->Ks, 1.0, 1.0, 1.0);
      cgSetParameter1f(cg->shininess, 50);


    }
#endif // GPU_OK
    if (renderObject){
      if(patchObject)
    renderer->render(patchObject);
      renderer->render(object);
    }
#ifdef GPU_OK
    if (mUseGPU){
      cgGLDisableProfile( cg->vertProfile );
      cgGLDisableProfile( cg->fragProfile );
    }
#endif // GPU_OK
  }
  // Adjust the depthrange so selected items are shown clearly
  glDepthRange(0,1-0.0005-0.0005);
  drawSelected();
  glDepthRange(0,1);

  GLdouble model[4][4], proj[4][4];
  GLint view[4];

#ifndef Q_WS_QWS
  glGetDoublev(GL_MODELVIEW_MATRIX, &model[0][0]);
  glGetDoublev(GL_PROJECTION_MATRIX, &proj[0][0]);
  glGetIntegerv(GL_VIEWPORT, &view[0]);
#endif

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0f,(GLfloat)width()/(GLfloat)height(),0.1f,100.0f);

  glPopAttrib();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();


  drawSelectionWindow(&painter);

  drawHUD(&painter);
  drawSelectedIDs(&painter, &model[0][0], &proj[0][0], &view[0]);
  drawIDs(&painter, &model[0][0], &proj[0][0], &view[0]); // draw vertex, edge and face ids



  glPushAttrib(GL_ALL_ATTRIB_BITS);

  //try out some code to put an axis in the bottom left corner of the screen
  glViewport(10.0,10.0,50.0,50.0);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(10.0, 1.0, 1.0, 100);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity( );
  Vector3d t = normalized(mCamera->getEye() - mCamera->getCenter())*10.0;
  gluLookAt(t[0], t[1], t[2],
        0.0,0.0,0.0,
        mCamera->getUp()[0],mCamera->getUp()[1],mCamera->getUp()[2]);

  glDisable(GL_BLEND);
  glLineWidth(1.5);
  glBegin(GL_LINES);
  glColor3f(1.0,0.25,0.25);                     // X Axis - Red
  glVertex3f(0,0,0); glVertex3f(1,0,0);
  glColor3f(0.25,1.0,0.25);                     // Z Axis - Green
  glVertex3f(0,0,0); glVertex3f(0,0,1);
  glColor3f(0.25,0.25,1.0);                     // Y Axis - Blue
  glVertex3f(0,0,0); glVertex3f(0,1,0);
  glEnd();
  glLineWidth(3.0);
  glEnable(GL_BLEND); // Enable Blending

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0f,(GLfloat)width()/(GLfloat)height(),0.1f,100.0f);

  glPopAttrib();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  painter.end();
}

void GLWidget::resizeGL( int width, int height ){
  if (height==0)	height=1;//Making Height Equal One
  glViewport( 0, 0, (GLint)width, (GLint)height );


}

void GLWidget::setupViewport(int width, int height){

}

void GLWidget::drawText(int width, int height ){
}

/*
 *  draw a selection window when in the proper mode
 *
 */
void GLWidget::drawSelectionWindow(QPainter *painter){
  if (mShowSelectionWindow){
    QBrush brush = QBrush(QColor(0,0,0,255));
    QPen pen = QPen(brush,1.0);
    painter->setPen(pen);
    painter->setBrush(QBrush(QColor(0,0,0,50)));
        int x = mapFromGlobal(QCursor::pos()).x();
    int y = mapFromGlobal(QCursor::pos()).y();

    painter->drawRect(mSelectionWindowStartX,height()-mSelectionWindowStartY, x-mSelectionWindowStartX, y-(height()-mSelectionWindowStartY));

  }
}

void GLWidget::drawBrush(QPainter *painter){
  if (mShowBrush){
    QBrush brush = QBrush(QColor(255,0,0,127));
    QPen pen = QPen(brush,4.0);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);
    int x = mapFromGlobal(QCursor::pos()).x();
    int y = mapFromGlobal(QCursor::pos()).y();
    painter->drawEllipse(x - mBrushSize/2, y - mBrushSize/2, mBrushSize, mBrushSize);
    painter->drawLine(x-10,y,x+10,y);
    painter->drawLine(x,y-10,x,y+10);
  }
}

//the Heads up display should show the current object's
//number of vertices, faces, edges, the genus
//number of selected vertices, edges, faces, face vertices etc
void GLWidget::drawHUD(QPainter *painter){
  if (mShowHUD){

    if (mSelectionMaskString == "Faces"){
      //show info about a selected face
      if (numSelectedFaces() == 1) {
    OSSIFacePtr fp = object->sel_fptr_array[0];
      }
    }
    if (mSelectionMaskString == "Edges"){
      //show info about a selected edge
      if (numSelectedEdges() == 1) {
    OSSIEdgePtr ep = object->sel_eptr_array[0];
      }
    }
    if (mSelectionMaskString == "Vertices"){
      //show info about a selected vertex
      if (numSelectedVertices() == 1) {
    OSSIVertexPtr vp = object->sel_vptr_array[0];
      }
    }
    if (mSelectionMaskString == "Corners"){
      //show info about a selected corner
      if (numSelectedCorners() == 1) {
    OSSIFaceVertexPtr fvp = object->sel_fvptr_array[0];
    // std::cout << fvp << "\n";
      }
    }

    QString s1 = "Vertices: " + QString("%1").arg((uint)object->num_vertices()) +
      "\nEdges: " + QString("%1").arg((uint)object->num_edges()) +
      "\nFaces: " + QString("%1").arg((uint)object->num_faces()) +
      "\nMaterials: " + QString("%1").arg((uint)object->num_materials()) +
      "\nGenus: " + QString("%1").arg(object->genus());

    QString s2 = "Sel. Vertices:" + QString("%1").arg(numSelectedVertices()) +
      "\nSel. Edges: " + QString("%1").arg(numSelectedEdges()) +
      "\nSel. Faces: " + QString("%1").arg(numSelectedFaces()) +
      "\nSel. Corners: " + QString("%1").arg(numSelectedCorners());

    QString s3 = 	"Mode: " + mModeString +
      "\nRemeshing Mode: " + mRemeshingSchemeString +
      "\nExtrusion Mode: " + mExtrusionModeString +
      "\nSelection Mode: " + mSelectionMaskString;




    QFont font("Arial", 11);
    QFontMetrics fm(font);


    QRect r1(fm.boundingRect(s1));
    QRect r2(fm.boundingRect(s2));
    QRect r3(fm.boundingRect(s3));

    painter->setPen(Qt::NoPen);
    QBrush brush = QBrush(QColor(0,0,0,127));
    painter->setBrush(brush);
    QRectF rectangle(0.0, height()-r1.height()*7, width(), height());
    painter->drawRect(rectangle);
    painter->setPen(Qt::white);
    painter->drawText(rectangle.left()+65,rectangle.top()+5,r3.width(),rectangle.height(),Qt::AlignLeft, s3);
    painter->drawText(/*fm.width(s3)*/280,rectangle.top()+5,r1.width(), rectangle.height(),Qt::AlignLeft, s1);
    painter->drawText(/*fm.width(s3)+fm.width(s1)*/460,rectangle.top()+5,r2.width(),rectangle.height(),Qt::AlignLeft, s2);
  }
}

void GLWidget::drawIDs( QPainter *painter, const GLdouble *model, const GLdouble *proj, const GLint	*view) {
  glDisable(GL_DEPTH_TEST);
  GLdouble win_x = 0, win_y = 0, win_z = 0;
  double min_dist=100000000.0, max_dist=-100000000.0, x,y,z,d;
  int count, min_alpha = 25, max_alpha = 255;
  Vector3d coord = mCamera->getEye(); //use this to find the dist from the camera
  QBrush brush;

  Vector3d offset = object->position;

  /* Draw Vertex IDs */
  if( mShowVertexIDs ) {
    OSSIVertexPtrArray vparray;
    object->getVertices(vparray);
    double vlsqr_array[vparray.size()];
    count = 0;
    OSSIVertexPtrArray::iterator it;
    for( it = vparray.begin(); it != vparray.end(); it++) {
      Vector3d dist = (*it)->coords - coord;
      vlsqr_array[count] = dist.lengthsqr();
      max_dist = (vlsqr_array[count] > max_dist ) ? vlsqr_array[count] : max_dist;
      min_dist = (vlsqr_array[count] < min_dist ) ? vlsqr_array[count] : min_dist;
      count++;
    }
    count = 0;
    for( it = vparray.begin(); it != vparray.end(); it++) {
      Vector3d point = (*it)->coords;
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      QString id = QString::number((*it)->getID() );
      if(max_dist == min_dist)
    d = min_alpha;
      else
    d = (vlsqr_array[count]-min_dist)*(max_alpha-min_alpha)/(max_dist - min_dist) + min_alpha;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      brush = QBrush(QColor(mVertexIDBgColor.red(),mVertexIDBgColor.green(),mVertexIDBgColor.blue(),min_alpha+max_alpha-d));
      painter->setBrush(brush);
      QRectF rectangle(win_x, win_y, 35, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(QColor(255,255,255,min_alpha+max_alpha-d));
      painter->drawText(rectangle, Qt::AlignCenter,id);
      count++;
    }
  }

  /* Draw Edge IDs */
  if( mShowEdgeIDs ) {
    OSSIEdgePtrArray eparray;
    object->getEdges(eparray);
    double elsqr_array[eparray.size()];
    count = 0;
    OSSIEdgePtrArray::iterator it;
    for( it = eparray.begin(); it != eparray.end(); it++) {
      Vector3d dist = (*it)->getMidPoint() - coord;
      elsqr_array[count] = dist.lengthsqr();
      max_dist = (elsqr_array[count] > max_dist ) ? elsqr_array[count] : max_dist;
      min_dist = (elsqr_array[count] < min_dist ) ? elsqr_array[count] : min_dist;
      count++;
    }
    count=0;
    for( it = eparray.begin(); it != eparray.end(); it++) {
      QString id = QString::number( (*it)->getID() );
      Vector3d point = (*it)->getMidPoint();
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      if(max_dist == min_dist)
    d = min_alpha;
      else
    d = (elsqr_array[count]-min_dist)*(max_alpha-min_alpha)/(max_dist - min_dist) + min_alpha;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      brush = QBrush(QColor(mEdgeIDBgColor.red(),mEdgeIDBgColor.green(),mEdgeIDBgColor.blue(),min_alpha+max_alpha-d));
      painter->setBrush(brush);
      QRectF rectangle(win_x, win_y, 35, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(QColor(255,255,255,min_alpha+max_alpha-d));
      painter->drawText(rectangle, Qt::AlignCenter,id);
      count++;
    }
  }

  /* Draw Face IDs */
  if( mShowFaceIDs ) {
    OSSIFacePtrArray fparray;
    object->getFaces(fparray);
    double flsqr_array[fparray.size()];
    count = 0;
    OSSIFacePtrArray::iterator it;
    for( it = fparray.begin(); it != fparray.end(); it++) {
      Vector3d dist = (*it)->geomCentroid() - coord;
      flsqr_array[count] = dist.lengthsqr();
      max_dist = (flsqr_array[count] > max_dist ) ? flsqr_array[count] : max_dist;
      min_dist = (flsqr_array[count] < min_dist ) ? flsqr_array[count] : min_dist;
      count++;
    }
    count=0;
    for( it = fparray.begin(); it != fparray.end(); it++) {
      QString id = QString::number( (*it)->getID() );
      Vector3d point = (*it)->geomCentroid();// + (*it)->getNormal();
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      if(max_dist == min_dist)
    d = min_alpha;
      else
    d = (flsqr_array[count]-min_dist)*(max_alpha-min_alpha)/(max_dist - min_dist) + min_alpha;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      brush = QBrush(QColor(mFaceIDBgColor.red(),mFaceIDBgColor.green(),mFaceIDBgColor.blue(),min_alpha+max_alpha-d));
      painter->setBrush(brush);
      QRectF rectangle(win_x, win_y, 35, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(QColor(255,255,255,min_alpha+max_alpha-d));
      painter->drawText(rectangle, Qt::AlignCenter,id);
      count++;
    }
  }

  glPopMatrix( );
  glEnable(GL_DEPTH_TEST);
}

void GLWidget::drawSelectedIDs( QPainter *painter, const GLdouble *model, const GLdouble *proj, const GLint	*view) {
  if (mShowSelectedIDs){
    glDisable(GL_DEPTH_TEST);

    glPushMatrix( );
    double mat[16];
    object->tr.fillArrayColumnMajor(mat);
    glMultMatrixd(mat);

    double x,y,z;
    QBrush brush;
    QRectF rectangle;
    QString id;
    GLdouble win_x = 0, win_y = 0, win_z = 0;

    if( !mShowVertexIDs ) {
      if ( !object->sel_vptr_array.empty() ){
    OSSIVertexPtrArray::iterator first, last;
    first = object->sel_vptr_array.begin(); last = object->sel_vptr_array.end();
    while ( first != last ){
      QString id = QString::number( (*first)->getID() );
      double x,y,z;
      Vector3d point = (*first)->coords;// + (*it)->getNormal();
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      win_x = 0, win_y = 0, win_z = 0;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      brush = QBrush(QColor(mVertexIDBgColor));
      painter->setBrush(brush);
      rectangle = QRectF(win_x, win_y, 40, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(Qt::white);
      painter->drawText(rectangle, Qt::AlignCenter,id);
      ++first;
    }
      }
    }

    if( !mShowEdgeIDs ) {
      if ( !object->sel_eptr_array.empty() ){
    OSSIEdgePtrArray::iterator first, last;
    first = object->sel_eptr_array.begin(); last = object->sel_eptr_array.end();
    while ( first != last ){
      QString id = QString::number( (*first)->getID() );
      Vector3d point = (*first)->getMidPoint(true);
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      win_x = 0, win_y = 0, win_z = 0;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      QBrush brush = QBrush(QColor(mEdgeIDBgColor));
      painter->setBrush(brush);
      rectangle = QRectF(win_x, win_y, 40, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(Qt::white);
      painter->drawText(rectangle, Qt::AlignCenter,id);
      ++first;
    }
      }
    }

    if( !mShowFaceIDs ) {
      if ( !object->sel_fptr_array.empty() ){
    OSSIFacePtrArray::iterator first, last;
    first = object->sel_fptr_array.begin(); last = object->sel_fptr_array.end();
    while ( first != last )	{
      QString id = QString::number( (*first)->getID() );
      double x,y,z;
      Vector3d point = (*first)->geomCentroid();// + (*it)->getNormal();
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      win_x = 0, win_y = 0, win_z = 0;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      brush = QBrush(QColor(mFaceIDBgColor));
      painter->setBrush(brush);
      rectangle = QRectF(win_x, win_y, 40, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(Qt::white);
      painter->drawText(rectangle, Qt::AlignCenter,id);
      ++first;
    }
      }
    }

    if( !mShowFaceVertexIDs ) {
      if ( !object->sel_fvptr_array.empty() ) {
    OSSIFaceVertexPtrArray::iterator first, last;
    first = object->sel_fvptr_array.begin(); last = object->sel_fvptr_array.end();
    while ( first != last ){
      QString id = QString::number( (*first)->vertex->getID() );
      double x,y,z;
      Vector3d point = (*first)->vertex->coords;
      point = object->tr.applyTo(point);
      point.get(x,y,z);
      win_x = 0, win_y = 0, win_z = 0;
      gluProject(x, y, z, model, proj, view, &win_x, &win_y, &win_z);
      win_y = height() - win_y; // y is inverted
      painter->setPen(Qt::NoPen);
      brush = QBrush(QColor(mVertexIDBgColor));
      painter->setBrush(brush);
      rectangle = QRectF(win_x, win_y, 40, 20);
      painter->drawRoundRect(rectangle,6,6);
      painter->setPen(Qt::white);
      painter->drawText(rectangle, Qt::AlignCenter,id);
      ++first;
    }
      }
    }
    glPopMatrix( );
    glEnable(GL_DEPTH_TEST);
  }
}

// Subroutine for selecting a Vertex
OSSIVertexPtr GLWidget::selectVertex(int mx, int my, int w, int h) {
  GLuint selectBuf[8192];
  uint closest;
  GLuint dist;
  long hits, index;
  OSSIVertexPtr sel(NULL);

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp); // Reduced sensitivity for picking points

  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderVerticesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 ){
    closest = 0; dist = 0xffffffff;
    while ( hits ){
      index = (hits-1)*4;
      if ( selectBuf[index+1] < dist ){
    dist = selectBuf[index+1];
    closest = selectBuf[index+3];
      }
      hits--;
    }
    // closest now has the id of the selected vertex
    sel = OSSIObject::vparray[closest];
  }
  return sel;
}

// Subroutine for selecting a Vertex
OSSIVertexPtrArray GLWidget::selectVertices(int mx, int my, int w, int h) {
  GLuint selectBuf[8192];
  long hits, index;
  OSSIVertexPtrArray vparray;

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp); // Reduced sensitivity for picking points

  // Make sure earlier matrices are preserved
  // seem to be sharing the same matrix stacks
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderVerticesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);

  if ( hits > 0 ){
    while ( hits ){
      index = (hits-1)*4;

      vparray.push_back(OSSIObject::vparray[selectBuf[index+3]]);
      hits--;
    }
    return vparray;
  }
  vparray.clear();
  return vparray;
}

// Subroutine for selecting a locator
OSSILocatorPtr GLWidget::selectLocator(int mx, int my, int w, int h) // brianb
{
  GLuint selectBuf[8192];
  uint closest;
  GLuint dist;
  long hits, index;

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);

  mCamera->enterSelectionMode(mx,my,w,h,vp); // Reduced sensitivity for picking points

  // Make sure earlier matrices are preserved

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderLocatorsForSelect();
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();
  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 )
    {
      closest = 0; dist = 0xffffffff;
      while ( hits )
    {
      index = (hits-1)*4;
      if ( selectBuf[index+1] < dist )
        {
          dist = selectBuf[index+1];
          closest = selectBuf[index+3];
        }
      hits--;
    }

      // closest now has the hit axis
      GLWidget::locatorPtr->setSelectedAxis(closest);

      // Only one locator for now, return pointer to OSSIViewport::locator
      return GLWidget::locatorPtr;
    }

  return NULL;
}

// Subroutine for selecting an Edge
OSSIEdgePtr GLWidget::selectEdge(int mx, int my,int w, int h) {
  GLuint selectBuf[8192];
  uint closest;
  GLuint dist;
  long hits, index;
  OSSIEdgePtr sel(NULL);

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp);

  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderEdgesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 ){
    closest = 0; dist = 0xffffffff;
    while ( hits ){
      index = (hits-1)*4;
      if ( selectBuf[index+1] < dist ){
    dist = selectBuf[index+1];
    closest = selectBuf[index+3];
      }
      hits--;
    }
    // closest now has the id of the selected edge
    sel = OSSIObject::eparray[closest];
  }
  return sel;
}

// Subroutine for selecting an Edge
OSSIEdgePtrArray GLWidget::selectEdges(int mx, int my,int w, int h) {
  GLuint selectBuf[8192];
  long hits, index;
  OSSIEdgePtr sel(NULL);
  OSSIEdgePtrArray eparray;

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp);

  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  mCamera->SetProjection(width(),height());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderEdgesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 ){
    while ( hits ){
      index = (hits-1)*4;

      eparray.push_back(OSSIObject::eparray[selectBuf[index+3]]);
      hits--;
    }
    return eparray;
  }
  eparray.clear();
  return eparray;
}

// Subroutine for selecting a Face
OSSIFacePtr GLWidget::selectFace(int mx, int my, int w, int h) {
  GLuint selectBuf[8192];
  uint closest;
  GLuint dist;
  long hits, index;
  OSSIFacePtr sel(NULL);

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp);

  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderFacesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 ){
    closest = 0; dist = 0xffffffff;
    while ( hits ){
      index = (hits-1)*4;
      if ( selectBuf[index+1] < dist ){
    dist = selectBuf[index+1];
    closest = selectBuf[index+3];
      }
      hits--;
    }
    // closest now has the id of the selected face
    sel = OSSIObject::fparray[closest];
  }
  return sel;
}

// Subroutine for selecting multiple faces at once
OSSIFacePtrArray GLWidget::selectFaces(int mx, int my, int w, int h) {

  GLuint selectBuf[8192];
  long hits, index;
  OSSIFacePtr sel(NULL);
  OSSIFacePtrArray fparray;
  fparray.clear();

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp);

  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  mCamera->SetProjection(width(),height());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderFacesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);

  if ( hits > 0 ){
    while ( hits ){
      index = (hits-1)*4;
      fparray.push_back(OSSIObject::fparray[selectBuf[index+3]]);
      hits--;
    }

    return fparray;
  }

  fparray.clear();
  return fparray;
}


// Subroutine for selecting multiple faces at once
  OSSIFacePtr GLWidget::deselectFaces(int mx, int my, int w, int h) {
  glEnable(GL_CULL_FACE);
  GLuint selectBuf[8192];
  uint closest;
  GLuint dist;
  long hits, index;
  OSSIFacePtr sel(NULL);
  OSSIFacePtrArray fparray;

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,mBrushSize,mBrushSize,vp);

  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderFacesForSelect(object);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 ){
    closest = 0; dist = 0xffffffff;
    while ( hits ){
      index = (hits-1)*4;
      if ( selectBuf[index+1] < dist ){
    dist = selectBuf[index+1];
    closest = selectBuf[index+3];
      }


      hits--;
    }
    // closest now has the id of the selected face
    sel = OSSIObject::fparray[closest];
  }
  glDisable(GL_CULL_FACE);

  return sel;
}

// Subroutine for selecting a FaceVertex (Corner) within a Face
OSSIFaceVertexPtr GLWidget::selectFaceVertex(OSSIFacePtr fp, int mx, int my, int w, int h) {
  GLuint selectBuf[8192];
  uint closest;
  GLuint dist;
  long hits, index;
  OSSIFaceVertexPtr sel(NULL);

  glSelectBuffer(8192,selectBuf);
  glRenderMode(GL_SELECT);

  glInitNames(); glPushName(0);

  GLint vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);
  mCamera->enterSelectionMode(mx,my,w,h,vp);
  // Make sure earlier matrices are preserved
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();


  mCamera->SetProjection(width(),height());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  renderFaceVerticesForSelect(fp);
  glFlush();

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  mCamera->leaveSelectionMode();

  hits = glRenderMode(GL_RENDER);
  if ( hits > 0 ){
    closest = 0; dist = 0xffffffff;
    while ( hits ){
      index = (hits-1)*4;
      if ( selectBuf[index+1] < dist ){
    dist = selectBuf[index+1];
    closest = selectBuf[index+3];
      }
      hits--;
    }
    // closest now has the id of the selected face vertex
    sel = OSSIObject::fvparray[closest];
  }
  return sel;
}

// Draw the selected items
void GLWidget::drawSelected(void) {
  if ( !sel_lptr_array.empty() ) {
    sel_lptr_array[0]->render();
  }

  if ( !object->sel_vptr_array.empty() ) {
    glPointSize(mSelectedVertexThickness);
    //glBegin(GL_POINTS);
    glColor4f(mSelectedVertexColor.redF(),mSelectedVertexColor.greenF(),mSelectedVertexColor.blueF(),mSelectedVertexColor.alphaF());
    OSSIVertexPtrArray::iterator first, last;
    first = object->sel_vptr_array.begin(); last = object->sel_vptr_array.end();
    while ( first != last ){
      GeometryRenderer::instance()->renderVertex(*first);
      ///(*first)->render();
      ++first;
    }

    glPointSize(1.0);
  }

  if ( !object->sel_eptr_array.empty() ){
    glLineWidth(mSelectedEdgeThickness);
    glColor4f(mSelectedEdgeColor.redF(),mSelectedEdgeColor.greenF(),mSelectedEdgeColor.blueF(),mSelectedEdgeColor.alphaF());
    OSSIEdgePtrArray::iterator first, last;
    first = object->sel_eptr_array.begin(); last = object->sel_eptr_array.end();
    while ( first != last ){
      glBegin(GL_LINES); {
    GeometryRenderer::instance()->renderEdge(*first);
      } glEnd();
      ///(*first)->render();
      ++first;
    }
    glLineWidth(3.0);
  }

  if ( !object->sel_fptr_array.empty() ){
    glLineWidth(mSelectedEdgeThickness);
    glColor4f(mSelectedFaceColor.redF(),mSelectedFaceColor.greenF(),mSelectedFaceColor.blueF(),mSelectedFaceColor.alphaF());
    OSSIFacePtrArray::iterator first, last;
    first = object->sel_fptr_array.begin(); last = object->sel_fptr_array.end();
    while ( first != last )	{
      GeometryRenderer::instance()->renderFace(*first,false);
      ///(*first)->render_FVN();
      ++first;
    }
    glLineWidth(3.0);
  }

  if ( !object->sel_fvptr_array.empty() ) {
    glPointSize(mSelectedVertexThickness);
    glColor4f(mSelectedVertexColor.redF(),mSelectedVertexColor.greenF(),mSelectedVertexColor.blueF(),mSelectedVertexColor.alphaF());
    glBegin(GL_POINTS);
    OSSIFaceVertexPtrArray::iterator first, last;
    first = object->sel_fvptr_array.begin(); last = object->sel_fvptr_array.end();
    while ( first != last ){
      GeometryRenderer::instance()->renderFaceVertex(*first,false);
      ///(*first)->render();
      ++first;
    }
    glEnd();
    glPointSize(1.5);
  }
}

void GLWidget::wheelEvent(QWheelEvent *event) {

  mCamera->HandleMouseWheel(event->delta(), width(),height());
  repaint();

}
void GLWidget::mousePressEvent(QMouseEvent *event) {

  if ( QApplication::keyboardModifiers() == Qt::AltModifier ||
       (event->buttons() == Qt::LeftButton && QApplication::keyboardModifiers() == (Qt::ShiftModifier | Qt::AltModifier)) ){
    mCamera->HandleMouseEvent(event->button(), event->type(), event->x(), event->y());
    repaint();
  }
  else event->ignore();


}

// this has been modified for the QT interface, so it handles focus automatically
void GLWidget::mouseMoveEvent(QMouseEvent *event) {
  if ( QApplication::keyboardModifiers() == Qt::AltModifier ||
       (event->buttons() == Qt::LeftButton && QApplication::keyboardModifiers() == (Qt::ShiftModifier | Qt::AltModifier)) ){
    mCamera->HandleMouseMotion(event->x(),event->y(), width(), height() );
    repaint();
  }
  else event->ignore();

}

void GLWidget::mouseReleaseEvent(QMouseEvent *event) {
  if ( QApplication::keyboardModifiers() == Qt::AltModifier ||
       (event->buttons() == Qt::LeftButton && QApplication::keyboardModifiers() == (Qt::ShiftModifier | Qt::AltModifier)) ){
    mCamera->HandleMouseEvent(event->button(), event->type(), event->x(), event->y());
    repaint();
  }
  else event->ignore();


}

//for vertex locator moving...
void erase_dlp(OSSILocatorPtr lp)  { delete lp; } // brianb

// Global operations (don't require selection)
void GLWidget::recomputeNormals(void)     // Recompute normals and lighting
{
  object->computeNormals();
  computeLighting( object, patchObject, &plight, mUseGPU);
}

void GLWidget::recomputeLighting(void)                // Recompute lighting
{
  computeLighting( object, patchObject, &plight, mUseGPU);
}

void GLWidget::recomputePatches(void) // Recompute the patches for patch rendering
{
  if(patchObject)
    patchObject->updatePatches(object);
}

void GLWidget::setWarmLightColor(QColor c){
  plight.warmcolor.set(c.redF(),c.greenF(),c.blueF());
  recomputeLighting();
  redraw();
}

void GLWidget::setCoolLightColor(QColor c){
  plight.coolcolor.set(c.redF(),c.greenF(),c.blueF());
  recomputeLighting();
  redraw();
}

void GLWidget::setLightIntensity(double i){
  plight.intensity = i;
  recomputeLighting();
  redraw();
}

void GLWidget::setRenderColor(QColor c){
  mRenderColor = c;
  GeometryRenderer *gr = GeometryRenderer::instance();
  gr->renderColor[0] = c.redF();
  gr->renderColor[1] = c.greenF();
  gr->renderColor[2] = c.blueF();
  gr->renderColor[3] = c.alphaF();
  redraw();
}

