/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _GEOMETRY_RENDERER_H_
#define _GEOMETRY_RENDERER_H_

#include <OSSIObject.h>
#include "CgCompute.h"


#ifdef GPU_OK
using namespace Cg;
#endif // GPU_OK

using namespace OSSI;

/*
    GeometryRenderer.h
    Definition of the GeometryRenderer class

    GeometryRenderer
*/

class GeometryRenderer {
public :
  /** Singleton Methods **/
  static GeometryRenderer* instance( );
  ~GeometryRenderer( ) { delete renderColor; renderColor = 0; } //delete rendercolor, set it to 0


  static void glBeginFace( int num, bool outline = false );

  void render( OSSIObjectPtr obj ) const;
  void renderFace( OSSIFacePtr ofp, bool useAttrs = true ) const;
  void renderFaceVertex( OSSIFaceVertexPtr ofvp, bool useAttrs = true ) const; //face vertex is a unique way of representing vertex ert face, so that we can color faces individually.
  void renderEdge( OSSIEdgePtr oep ) const;
  void renderVertex( OSSIVertexPtr ovp ) const;

  void renderVertices( OSSIObjectPtr obj, double size = 5.0 ) const;
  void renderEdges( OSSIObjectPtr obj, double width = 1.0 ) const;
    void renderFaceNormals( OSSIObjectPtr obj, double width, double length ) const; //aa perpendicular normal unit vector to the surface of the face
    void renderFaceCentroids( OSSIObjectPtr obj, double size ) const; //face 'center' that is center of mass/gravity for uniform density object.


  bool useMaterial;
  bool useLighting;
    bool useColorable;
    bool useNormal;
  bool useTexture;
  bool useOutline;
    bool drawWireframe;
    bool drawSilhouette;
    bool drawVertices;
  bool drawFaceCentroids;
  bool drawFaceNormals;
  bool isReversed;
  bool useGPU;
    bool antialiasing;

  bool drawPatchWireframe;
  bool drawPatchBoundaries;
  bool drawCVs;
  bool drawPatchNormals;

  GLdouble* renderColor;

private :


  static GeometryRenderer *mInstance;
  GeometryRenderer( bool gpu = false) : useMaterial(false), useColorable(false), useLighting(false),
                       useNormal(false), useTexture(false), antialiasing(false),
                            useOutline(false), drawFaceCentroids(false), drawVertices(false),
                            drawSilhouette(false),drawWireframe(true),
                       drawFaceNormals(false), isReversed(false), useGPU(gpu) {
    renderColor = new GLdouble[4];
  }
};

#endif // _GEOMETRY_RENDERER_H_
