/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _QSHORTCUT_DIALOG_H_
#define _QSHORTCUT_DIALOG_H_

#include "qcumber.h"

#include "ui_shortcutdialog.h"

#include <QtXml>

/*
    qshortcutdialog.h
    Definition of the QShortcutDialog class

    QShortcutDialog
*/


#include <QObject>
#include <QDialog>

class QTreeWidgetItem;
class QShortcutManager;

class QCUMBER_EXPORT QShortcutDialog : public QWidget, private Ui::ShortcutDialog
{
    Q_OBJECT

    public:
        QShortcutDialog(QShortcutManager *m, QWidget *p = 0);
        void retranslate();
        void exec();

    private slots:
        void on_twShortcuts_itemDoubleClicked(QTreeWidgetItem *i, int col);

    private:
        QShortcutManager *pManager;
};

#endif // _SHORTCUT_DIALOG_H_
