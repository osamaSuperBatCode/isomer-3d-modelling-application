/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

/****************************************************************************
 ** The Basics Mode Header File.
 **
 ****************************************************************************/

#ifndef BASICSMODE_H //include guards
#define BASICSMODE_H

#include <QWidget> //base class for all interfaces.
#include <QPushButton> //provides command button
#include <QGridLayout> //lays out widgets in a grid.

/*
     BasicsMode.h
     Definition of the BasicsMode class


*/

#include "MainWindow.h" //inluce MainWindow.h

//define classes

class MainWindow;

class QComboBox;
class QPushButton;
class QGroupBox;
class QCheckBox;
class QLabel;

//publicly inherit QWidget Class
class BasicsMode : public QWidget {
    Q_OBJECT //The Q_OBJECT macro must appear in the private section of a class definition that declares its own signals and slots or that uses other services provided by Qt's meta-object system.

public:
    BasicsMode(QWidget *parent, QShortcutManager *sm, QWidget *actionList); //constructor to accept the QWidget object as parent, the QShortcutManager Object as sm, and the QWidget for an actionList.
    void addActions(QActionGroup *actionGroup, QToolBar *toolBar, QStackedWidget *stackedWidget); //adds actions, via QActionGroup, which groups actions together, toolbar is a moveable panel that accepts controls, and stacked widgets, where only one widget is available at a time.
    QMenu* getMenu(); //The QMenu class provides a menu widget for use in menu bars, context menus, and other popup menus.
    void retranslateUi(); //retranslates the ui according to the language specefied.

    //The QAction class provides an abstract user interface action that can be inserted into widgets.
    QAction *mInsertEdgeAction; //insert edge
    QAction *mDeleteEdgeAction;	//delete edge
    QAction *mCollapseEdgeAction; //collapse Edge
    QAction *mSubdivideEdgeAction; //subdivide edge
    QAction *mConnectEdgesAction;	//connect edges
    QAction *mSpliceCornersAction; //splice corners (Join Corners)
    QAction *mTransformsAction; // transformation

    //The QWidget class is the base class of all user interface objects.
    QWidget *mInsertEdgeWidget;
    QWidget *mDeleteEdgeWidget;
    QWidget *mCollapseEdgeWidget;
    QWidget *mSubdivideEdgeWidget;
    QWidget *mConnectEdgesWidget;
    QWidget *mSpliceCornersWidget;
    QWidget *mTransformsWidget;

    //The QGridLayout class lays out widgets in a grid.
    QGridLayout *mInsertEdgeLayout;
    QGridLayout *mDeleteEdgeLayout;
    QGridLayout *mCollapseEdgeLayout;
    QGridLayout *mSubdivideEdgeLayout;
    QGridLayout *mConnectEdgesLayout;
    QGridLayout *mSpliceCornersLayout;
    QGridLayout *mTransformsLayout;

protected:
    void setupInsertEdge();
    void setupDeleteEdge();
    void setupCollapseEdge();
    void setupSubdivideEdge();
    void setupConnectEdges();
    void setupSpliceCorners();
    void setupTransforms();

    QDoubleSpinBox *createDoubleSpinBox(QGridLayout *layout, QLabel *label, QString s, double low, double high, double step, double value, double decimals, int row, int col);


private:

    QWidget *mParent;
    QMenu *mBasicsMenu;

    QLabel *numSubdivsLabel;
    QDoubleSpinBox *numSubdivsSpinBox;
    QLabel *transformLabel;
    QLabel *xPosLabel;
    QLabel *yPosLabel;
    QLabel *zPosLabel;
    QLabel *scaleLabel;
    QLabel *xScaleLabel;
    QLabel *yScaleLabel;
    QLabel *zScaleLabel;

    QLabel *noOptionsInsertEdgeLabel;
    QCheckBox *cleanupDeleteEdgeCheckBox;
    QLabel *noOptionsCollapseEdgeLabel;
    QLabel *noOptionsConnectEdgesLabel;
    QLabel *noOptionsSpliceCornersLabel;

    QPushButton *freezeTransformsButton;

    //transform spinboxes
    QDoubleSpinBox *xScaleSpinBox;
    QDoubleSpinBox *yScaleSpinBox;
    QDoubleSpinBox *zScaleSpinBox;
    QDoubleSpinBox *xPosSpinBox;
    QDoubleSpinBox *yPosSpinBox;
    QDoubleSpinBox *zPosSpinBox;


public slots:
    void freezeTransforms();

    void triggerInsertEdge();
    void triggerDeleteEdge();
    void triggerCollapseEdge();
    void triggerSubdivideEdge();
    void triggerConnectEdges();
    void triggerSpliceCorners();
    void triggerTransforms();

};

#endif
