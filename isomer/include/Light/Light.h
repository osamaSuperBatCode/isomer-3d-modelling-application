/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/



#ifndef _LIGHT_H_

#define _LIGHT_H_

// Abstract base class for a generic Light source
// Each light has a warm color, cool color and intensity. The intensity need not
// be used at all. And for lights which have only one color, there is a reference
// variable 'color' which points to the warm color. This can be used without having
// to worry about warm and cool colors
// Every light also has a position (although it might be meaningless for directional lights)
// and a boolean variable which indicates active/inactive state of light

#include "../Base/BaseObject.h"
#include "../Graphics/Color.h"
#include "../vecmat/Vector3d.h"

#include <QColor>

enum LightType { Ambient=0, Directional=1, PtLight=2, Spot=3 };

class Light;
typedef Light * LightPtr;

class Light : public BaseObject
{
  public :

     Vector3d  position;                               // Position

        /* NOTE: ORDER DEPENDENCY HERE! 'color' has to come *AFTER* warmcolor */
     RGBColor  warmcolor;                              // Warm color - RGB
     RGBColor  coolcolor;                              // Cool color - RGB
     RGBColor& color;                                  // Reference to warm color

     double    intensity;                              // Intensity of color

     bool      state;                                  // true/1 = on, false/0 = off

  public :

        // Default constructor
     Light()
       : BaseObject(), position(0,0,0),
         warmcolor(1,1,1), coolcolor(1,1,1), color(warmcolor),
         intensity(1), state(true)
       {}

        // 1-arg constructor
     Light(const Vector3d& pos)
       : BaseObject(), position(pos),
         warmcolor(1,1,1), coolcolor(1,1,1), color(warmcolor),
         intensity(1), state(true)
       {}

        // 1-arg constructor. specifies color
     Light(const RGBColor& col)
       : BaseObject(), position(0,0,0),
         warmcolor(col), coolcolor(col), color(warmcolor),
         intensity(1), state(true)
       {}

        // Copy constructor
     Light(const Light& light)
       : BaseObject(light), position(light.position),
         warmcolor(light.warmcolor), coolcolor(light.coolcolor), color(warmcolor),
         intensity(light.intensity), state(light.state)
       {}

        // Destructor
     virtual ~Light()
       {}

        // Assignment operator
     Light& operator = (const Light& light)
       {
         BaseObject :: operator = (light);
         position = light.position;
         warmcolor = light.warmcolor; coolcolor = light.coolcolor;
         intensity = light.intensity; state = light.state;
         return (*this);
       }

        // Return type of light.
     virtual LightType type(void) const = 0;

        // Does this light illuminate given point?
     virtual bool illuminates(const Vector3d& p) const = 0;

        // Compute the cosine factor for given point/normal. Meaningful only for some lights
     virtual double cosfactor(const Vector3d& p, const Vector3d& n) const = 0;

        // Illuminate a given point with given normal using this light and return the color
     virtual RGBColor illuminate(const Vector3d& p, const Vector3d& n) const = 0;

        // Illuminate a given point with given normal using this light and return the color
        // The eye position is also given to allow specular computations
     virtual RGBColor illuminate(const Vector3d& p, const Vector3d& n, const Vector3d& e) const = 0;
};

#endif // #ifndef _LIGHT_H_


