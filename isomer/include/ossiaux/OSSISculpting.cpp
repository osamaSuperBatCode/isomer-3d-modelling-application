/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#include "OSSISculpting.h"
#include "OSSIConnect.h"

namespace OSSI {

    void createConvexHull( OSSIObjectPtr obj ){
        Vector3dArray varray;
        varray.resize(obj->num_vertices());
        OSSIVertexPtrList::const_iterator first = obj->beginVertex(), last = obj->endVertex();
        int i = 0;
        while ( first != last ) {
            varray[i++] = (*first)->getCoords();
            ++first;
        }
        OSSIConvexHull chull;
        chull.createHull(varray);
        obj->reset();
        obj->splice(chull);
    }

    void createDualConvexHull( OSSIObjectPtr obj ){

        OSSIVertexPtrArray verts;
        Vector3dArray ovarray;
        obj->getVertices(verts);
        for(int i = 0; i < (int)verts.size(); i++)
            ovarray.push_back(verts.at(i)->getCoords());

        //find min and max points
        float fmax=9999999, fmin = -9999999;
        Vector3d cvmin(fmax,fmax,fmax),cvmax(fmin,fmin,fmin);
        for(int i = 0; i < (int)ovarray.size(); i++) {
            Vector3d v = ovarray.at(i);
            for(int j=0;j<3;j++) {
                if (v[j]<cvmin[j]) cvmin[j] = v[j];
                if (v[j]>cvmax[j]) cvmax[j] = v[j];
            }
        }

        //slice object uniformly
        int SLICENUM[3] = {1,1,1};
        int DUAL = 1;
        float EPS = 0.001;

        obj->reset();
        Vector3d step = (cvmax - cvmin + Vector3d(2*EPS,2*EPS,2*EPS));
        step[0]/=SLICENUM[0];
        step[1]/=SLICENUM[1];
        step[2]/=SLICENUM[2];



        float x=cvmin[0]-EPS;
        for(int ix = 0; ix<SLICENUM[0]; ix++){
            float y=cvmin[1]-EPS;
            for(int iy = 0; iy<SLICENUM[1]; iy++){
                float z=cvmin[2]-EPS;
                for(int iz = 0; iz<SLICENUM[2]; iz++){
                    Vector3dArray coords;

                    for(int i=0;i<ovarray.size();i++)
                        if ( (ovarray.at(i)[0]  > x)&&(ovarray.at(i)[0]  < x+step[0]+EPS) &&
                        (ovarray.at(i)[1]  > y)&&(ovarray.at(i)[1]  < y+step[1]+EPS) &&
                        (ovarray.at(i)[2]  > z)&&(ovarray.at(i)[2]  < z+step[2]+EPS) )
                        coords.push_back(ovarray.at(i));
                    if (coords.size()>3){
                        if (!DUAL){
                            OSSIConvexHull chull;
                            chull.createHull(coords);
                            obj->splice(chull);
                        } else {
                            OSSIObjectPtr object = createDualConvexHull(obj,coords);
                            obj->splice(*object);
                        }
                    }
                    z+=step[2];
                }
                y+=step[1];
            }
            x+=step[0];
        }
    }

    OSSIObjectPtr createDualConvexHull( OSSIObjectPtr obj, const Vector3dArray &ovarray){


        OSSIVertexPtrArray cverts;

        if (convexhull==NULL) convexhull=new OSSIConvexHull();

        convexhull->createHull(ovarray);
        convexhull->getVertices(cverts);
        float fmax=9999999, fmin = -9999999;
        Vector3d cvmin(fmax,fmax,fmax),cvmax(fmin,fmin,fmin);
        for(int i=0;i<ovarray.size();i++){
            Vector3d v = ovarray.at(i);
            for(int j=0;j<3;j++){
                if (v[j]<cvmin[j]) cvmin[j] = v[j];
                if (v[j]>cvmax[j]) cvmax[j] = v[j];
            }
        }

        OSSIObjectPtr cube= new OSSIObject();
        Vector3dArray verts;
        Vector3d p;
        double x = 0.5;

        verts.resize(4);

        // Create top face
        p.set(cvmax[0],cvmax[1],cvmax[2]); verts[0] = p;
        p.set(cvmax[0],cvmax[1],cvmin[2]); verts[1] = p;
        p.set(cvmin[0],cvmax[1],cvmin[2]); verts[2] = p;
        p.set(cvmin[0],cvmax[1],cvmax[2]); verts[3] = p;
        cube->createFace(verts);
        OSSIFacePtr fp1 = cube->lastFace();

        // Create bottom face
        p.set(cvmax[0],cvmin[1],cvmax[2]); verts[0] = p;
        p.set(cvmin[0],cvmin[1],cvmax[2]); verts[1] = p;
        p.set(cvmin[0],cvmin[1],cvmin[2]); verts[2] = p;
        p.set(cvmax[0],cvmin[1],cvmin[2]); verts[3] = p;
        cube->createFace(verts);
        OSSIFacePtr fp2 = cube->lastFace();

        // fp1 and fp2 will be inward facing
        // Connect them using the first corners in each
        OSSIFaceVertexPtr fvp1, fvp2;
        fvp1 = fp1->firstVertex(); fvp2 = fp2->firstVertex();
        connectFaces(cube,fvp1,fvp2);

        for(int j=0;j<cverts.size();j++){


            OSSIVertexPtr vp= cverts.at(j);
            Vector3d normal = vp->computeNormal();
            peelByPlane(cube,normal,vp->getCoords());
        }

        return cube;
    }

    void peelByPlane( OSSIObjectPtr obj, Vector3d normal,Vector3d P0) {

        OSSIVertexPtr vp1, vp2, vnew;
        normal = normalized(normal);

        OSSIEdgePtrArray edgestodel,edges;
        OSSIEdgePtr ep;
        OSSIFaceVertexPtrArray corner_list, newcorners;

        Vector3d p1, p2,pnew;
        float d1, d2;
        int c=0;

        edgestodel.clear(); edges.clear();

        obj->getEdges(edges);
        float EPS = 0.000001;

        //check interssection of all edges w slicing plane
        for(int i=0; i<edges.size();i++){
            ep = edges.at(i);
            ep->getVertexPointers(vp1,vp2);
            p1 = vp1->getCoords();
            p2 = vp2->getCoords();
            float d1 = normal*(p1 - P0);
            float d2 = normal*(p2 - P0);

            //if both vertices of the edge are on (+) side
            if ((d1>0)&&(d2>0))
                edgestodel.push_back(ep);

            //if one end is - one end is +
            if ( ((d1*d2)<0) || ((d1>0)&&(d2==0)) || ((d1==0)&&(d2>0)) ){
                vnew = subdivideEdge(obj,ep);
                if (d1>0)
                    edgestodel.push_back(vnew->getEdgeTo(vp1));
                else
                    edgestodel.push_back(vnew->getEdgeTo(vp2));

    //make planar
                pnew = vnew->getCoords();
                Vector3d ne = normalized(p2-p1);
                float t = ( normal * ( P0 - pnew) ) / (normal * ne);
                vnew->setCoords(pnew + ne*t);

                vnew->getFaceVertices(corner_list);
                newcorners.push_back(corner_list.at(0));
                newcorners.push_back(corner_list.at(1));
            }
        }
        OSSIFaceVertexPtr cp1,cp2;

        for(int i =0; i<newcorners.size();i++){
            cp1 = newcorners.at(i);
            for(int j =0; j<newcorners.size();j++){
                if (i==j) continue;
                cp2 = newcorners.at(j);
                if (cp1->getFacePtr()==cp2->getFacePtr()){
                    insertEdge(obj,cp1,cp2);
                    continue;
                }
            }
        }

        for(int i=0;i<edgestodel.size();i++)
            deleteEdge(obj,edgestodel.at(i));

        edgestodel.clear();
    }

    void visitVertex(OSSIObjectPtr obj, OSSIVertexPtr vp,OSSIEdgePtr from,Vector3d normal,Vector3d P0){

        if (vp->isvisited) return;
        vp->isvisited = 1;
        Vector3d p1 = vp->getCoords();
        float d1 = normal*(p1 - P0);

        OSSIEdgePtrArray vedges;
        if (vp){
            vp->getEdges(vedges);
            int size = vedges.size();

            //check interssection of all edges w slicing plane
          for(int i=0; i<size;i++){
                OSSIEdgePtr ep = vedges.at(i);
                if (ep==from) continue;
                if (ep->isvisited) continue;
                ep->isvisited = 1;
                OSSIVertexPtr evp1,evp2,vp2;
                ep->getVertexPointers(evp1,evp2);
                vp2 = (vp==evp1)?evp2:evp1;
                Vector3d p2 = vp2->getCoords();
                float d2 = normal*(p2 - P0);

                //if both vertices of the edge are on (+) side
                if ((d1>0)&&(d2>0)){

                    edges2del[e2dsize++] = ep;
                    visitVertex(obj, vp2, ep, normal, P0);
                }

                //if (ep->istodel) continue;
                //if one end is - one end is +
                if ( ((d1*d2)<0) || ((d1>0)&&(d2==0)) || ((d1==0)&&(d2>0)) ){
                    int id = getCutIndex(ep);
                    OSSIVertexPtr vnew = subdivideEdge(obj, ep);
                    if (d1>0){

                        edges2del[e2dsize++] = vnew->getEdgeTo(vp);
                        if (id!=-1){
                            sedges[id] = vnew->getEdgeTo(vp2);
                            sverts[id] = vnew;
                        }

                    }else{

                    }
                    //make planar
                    Vector3d pnew = vnew->getCoords();
                    Vector3d ne = normalized(p2-p1);
                    float t = ( normal * ( P0 - pnew) ) / (normal * ne);
                    vnew->setCoords(pnew + ne*t);
                    OSSIFaceVertexPtrArray fvlist;
                    vnew->getFaceVertices(fvlist);
                    newcorners[ncsize++] = fvlist.at(0);
                    newcorners[ncsize++] = fvlist.at(1);
                }
          }
        }
    }

    void localCut(OSSIObjectPtr obj, OSSIVertexPtr vp,Vector3d normal,Vector3d P0){

        if(!vp) return;
        OSSIVertexPtrArray verts;
        OSSIEdgePtrArray edges;
        obj->getVertices(verts);
        obj->getEdges(edges);

        for(int i=0;i<verts.size();i++)
            verts.at(i)->isvisited=0;

        for(int i=0;i<edges.size();i++)
            edges.at(i)->isvisited=0;

        int size = obj->num_edges();
        newcorners = new OSSIFaceVertexPtr[size];
        edges2del  = new OSSIEdgePtr[size];
        e2dsize=0;
        ncsize=0;
        visitVertex(obj, vp,0,normal,P0);

    //connect new corners
        OSSIFaceVertexPtr cp1,cp2;
        for(int i =0; i<ncsize;i++){
            cp1 = newcorners[i];
            for(int j =0; j<ncsize;j++){
                if (i==j) continue;
                cp2 = newcorners[j];
                if ( cp1->getFacePtr() == cp2->getFacePtr() ){
                    insertEdge(obj, cp1,cp2);
                    continue;
                }
            }
        }

    //delete edges
        for(int i=0;i<e2dsize;i++){

            edges2del[i]->isdummy=0;
        }

        for(int i=0;i<e2dsize;i++){
            OSSIEdgePtr ep = edges2del[i];

            for(int j=i+1;j<e2dsize;j++){
                if (ep==edges2del[j]) {

                    ep->isdummy=1;
                }
            }
        }

        for(int i=0;i<e2dsize;i++){
            deleteEdge(obj, edges2del[i]);
        }

    }


    void performCutting( OSSIObjectPtr obj, int type,float offsetE,float offsetV,bool global,bool selected) {



        OSSIEdgePtrArray edges;
        OSSIVertexPtrArray verts;
        OSSIFacePtrArray faces;

        cutcount = 0;
        int vcount = 0;
        int num=0;

        Vector3d * locs, * norms;
        obj->getEdges(edges);
        obj->getVertices(verts);
        obj->getFaces(faces);
        int esize 	= edges.size();
        int vsize		= verts.size();
        int fsize 	= faces.size();
        int totalsize 	= esize+vsize+fsize;
        locs   		= new Vector3d[totalsize];
        norms  		= new Vector3d[totalsize];
        sverts 		= new OSSIVertexPtr[totalsize];
        sedges 		= new OSSIEdgePtr[esize];

        //this loops through all edges
        for(int i=0;i<esize;i++){
            sverts[cutcount]=0;

            OSSIEdgePtr e = edges.at(i);
            if ( ((selected)&&(!e->ismarked)) || (type==201) ) continue;

            OSSIVertexPtr v1,v2,v;
            e->getVertexPointers(v1,v2);

            int vnum=0;
            Vector3d * vlocs = new Vector3d[v1->valence() + v2->valence()];

            for(int j=0;j<2;j++){
                v =(j)?v2:v1;
                OSSIEdgePtrArray vedges;
                v->getEdges(vedges);
                for(int k=0;k<vedges.size();k++){
                    OSSIEdgePtr ve = vedges.at(k);
                    if (ve==e) continue;
                    OSSIVertexPtr ev1,ev2;
                    ve->getVertexPointers(ev1,ev2);
                    if (ev2==v){
                        ev2 = ev1;
                        ev1 = v;
                    }
                    vlocs[vnum++] = (ev2->getCoords() - ev1->getCoords())*offsetE + ev1->getCoords();
                }
            }

            //calculate the normal and position
            Vector3d vecp,vec,n(0,0,0),nprev,norm(0,0,0),mid(0,0,0);
            for(int j=0;j<vnum;j++)
                mid = mid + vlocs[j];
            mid = mid / ((float)vnum);

            for(int j=0;j<vnum;j++){
                vec = (mid - vlocs[j]);
                if (j==0){
                    vecp = vec;
                    continue;
                }
                n = n + vecp % vec;
            }

            OSSIFacePtr f1,f2;
            e->getFacePointers(f1,f2);
            norms[cutcount] = normalized(f1->computeNormal()+f2->computeNormal());
            locs[cutcount] = mid;
            sverts[cutcount] = v1;
            sedges[cutcount] = e;
            cutcount++;
        }

        //loop through all vertices
        for(int i = 0; i<vsize;i++){
            sverts[cutcount]=0;

            OSSIVertexPtr vp = verts.at(i);
            if ( ((selected)&&(!vp->ismarked))||(type==200) ) continue;

            OSSIEdgePtrArray vedges;
            vp->getEdges(vedges);

            int vnum=0;
            Vector3d * vlocs = new Vector3d[vp->valence()];

            for(int k=0;k<vedges.size();k++){

                OSSIEdgePtr ve = vedges.at(k);
                OSSIVertexPtr ev1,ev2;
                ve->getVertexPointers(ev1,ev2);
                if (ev2==vp){
                    ev2 = ev1;
                    ev1 = vp;
                }
                vlocs[vnum++] = (ev2->getCoords() - ev1->getCoords())*offsetV + ev1->getCoords();
            }

            //calculate the normal and position for bevelling
            Vector3d vecp,vec,n(0,0,0),nprev,norm(0,0,0),mid(0,0,0);
            for(int j=0;j<vnum;j++)
                mid = mid + vlocs[j];
            mid = mid / ((float)vnum);

            norms[cutcount] = vp->computeNormal();
            locs[cutcount] = mid;
            sverts[cutcount] = vp;
            cutcount++;
        }

        //cut by face mode
        if (type==203) {
            //loop through all faces
            for(int i = 0; i<fsize;i++){
                sverts[cutcount]=0;

                OSSIFacePtr fp = faces.at(i);
                if ( ((selected)&&(!fp->ismarked))) continue;

                OSSIVertexPtrArray fverts;
                OSSIFaceVertexPtrArray fcorners;
                OSSIVertexPtr v;
                int vnum=0;
                fp->getCorners(fcorners);

                for(int j=0;j<fcorners.size();j++){
                    OSSIFaceVertexPtr fv = fcorners.at(j);
                    fverts.push_back(fv->getVertexPtr());
                }

                Vector3d * vlocs = new Vector3d[fverts.size()*10];

                for(int j=0;j<fverts.size();j++){
                    v = fverts.at(j);
                    OSSIEdgePtrArray vedges;
                    v->getEdges(vedges);

                    for(int k=0;k<vedges.size();k++){
                        OSSIEdgePtr ve = vedges.at(k);
                        OSSIFacePtr ef1,ef2;
                        ve->getFacePointers(ef1,ef2);
                        if ((ef1==fp )||(ef2==fp)) continue;
                        OSSIVertexPtr ev1,ev2;
                        ve->getVertexPointers(ev1,ev2);
                        if (ev2==v){
                            ev2 = ev1;
                            ev1 = v;
                        }
                        vlocs[vnum++] = (ev2->getCoords() - ev1->getCoords())*offsetV + ev1->getCoords();
                    }
                }


            //calculate the normal and position for beveling
                Vector3d vecp,vec,n(0,0,0),nprev,norm(0,0,0),mid(0,0,0);
                for(int j=0;j<vnum;j++)
                    mid = mid + vlocs[j];
                mid = mid / ((float)vnum);

                norms[cutcount] = fp->computeNormal();
                locs[cutcount] = mid;
                float dmax = -999;
                OSSIVertexPtr vp,svp;
                for(int j=0;j<fverts.size();j++){
                    OSSIVertexPtr vp = fverts.at(j);
                    float d = norms[cutcount]*(vp->getCoords() - mid);

                    if (d>dmax){
                        dmax = d;
                        svp = vp;
                    }
                }
                sverts[cutcount] = svp;
                cutcount++;
            }
            }//end

            for(int i = 0; i<cutcount;i++){
                if (global)
                    peelByPlane(obj,norms[i],locs[i]);
                else	localCut(obj, sverts[i],norms[i],locs[i]);
            }

            delete [] norms;
            delete [] locs;
            delete [] sverts;
            delete [] sedges;
    }//end performCutting Function

    void cutSelectedFaces( OSSIObjectPtr obj, float offsetE,float offsetV, bool global,bool selected) {



                vector<OSSIVertexPtr>::iterator vit;
                vector<OSSIEdgePtr>::iterator eit,eit2;
                vector<OSSIFacePtr>::iterator fit,fit2;
                OSSIFacePtrArray sfptrarr = obj->sel_fptr_array;

                OSSIEdgePtrArray edges;
                OSSIVertexPtrArray verts;
                OSSIFacePtrArray faces;

                cutcount = 0;
                int vcount = 0;
                int num=0;

                Vector3d * locs, * norms;
                obj->getEdges(edges);
                obj->getVertices(verts);
                obj->getFaces(faces);
                int esize 	= edges.size();
                int vsize		= verts.size();
                int fsize 	= faces.size();
                int totalsize 	= esize+vsize+fsize;
                locs   		= new Vector3d[totalsize];
                norms  		= new Vector3d[totalsize];
                sverts 		= new OSSIVertexPtr[totalsize];
                sedges 		= new OSSIEdgePtr[esize];


            //loop through all faces
                for(fit = sfptrarr.begin(); fit != sfptrarr.end(); fit++, cutcount++){

                    sverts[cutcount]=0;

                    OSSIVertexPtrArray fverts;
                    OSSIFaceVertexPtrArray fcorners;
                    OSSIVertexPtr v;
                    int vnum=0;
                    (*fit)->getCorners(fcorners);

                    for(int j=0;j<fcorners.size();j++){
                        OSSIFaceVertexPtr fv = fcorners.at(j);
                        fverts.push_back(fv->getVertexPtr());
                    }

                    Vector3d * vlocs = new Vector3d[fverts.size()*10];

                    for(int j=0;j<fverts.size();j++){
                        v = fverts.at(j);
                        OSSIEdgePtrArray vedges;
                        v->getEdges(vedges);

                        for(int k=0;k<vedges.size();k++){
                            OSSIEdgePtr ve = vedges.at(k);
                            OSSIFacePtr ef1,ef2;
                            ve->getFacePointers(ef1,ef2);
                            if ((ef1==(*fit) )||(ef2==(*fit))) continue;
                            OSSIVertexPtr ev1,ev2;
                            ve->getVertexPointers(ev1,ev2);
                            if (ev2==v){
                                ev2 = ev1;
                                ev1 = v;
                            }
                            vlocs[vnum++] = (ev2->getCoords() - ev1->getCoords())*offsetV + ev1->getCoords();
                        }
                    }

            //calculate the normal and position for beveling
                    Vector3d vecp,vec,n(0,0,0),nprev,norm(0,0,0),mid(0,0,0);
                    for(int j=0;j<vnum;j++)
                        mid = mid + vlocs[j];
                    mid = mid / ((float)vnum);

                    norms[cutcount] = (*fit)->computeNormal();
                    locs[cutcount] = mid;
                    float dmax = -999;
                    OSSIVertexPtr vp,svp;
                    for(int j=0;j<fverts.size();j++){
                        OSSIVertexPtr vp = fverts.at(j);
                        float d = norms[cutcount]*(vp->getCoords() - mid);

                        if (d>dmax){
                            dmax = d;
                            svp = vp;
                        }
                    }
                    sverts[cutcount] = svp;
                }

                for(int i = 0; i<cutcount;i++){
                    if (global)
                        peelByPlane(obj,norms[i],locs[i]);
                    else	localCut(obj, sverts[i],norms[i],locs[i]);
                }

                delete [] norms;
                delete [] locs;
                delete [] sverts;
                delete [] sedges;
    }//end cutselectedFaces Function

    void cutSelectedEdges( OSSIObjectPtr obj, float offsetE,float offsetV, bool global,bool selected) {


        vector<OSSIVertexPtr>::iterator vit;
        vector<OSSIEdgePtr>::iterator eit,eit2;
        OSSIEdgePtrArray septrarr = obj->sel_eptr_array;

        OSSIEdgePtrArray edges;
        OSSIVertexPtrArray verts;
        OSSIFacePtrArray faces;

        cutcount = 0;
        int vcount = 0;
        int num=0;

        Vector3d * locs, * norms;
        obj->getEdges(edges);
        obj->getVertices(verts);
        obj->getFaces(faces);
        int esize 	= edges.size();
        int vsize		= verts.size();
        int fsize 	= faces.size();
        int totalsize 	= esize+vsize+fsize;
        locs   		= new Vector3d[totalsize];
        norms  		= new Vector3d[totalsize];
        sverts 		= new OSSIVertexPtr[totalsize];
        sedges 		= new OSSIEdgePtr[esize];

        //this loops through all edges
        for(eit = septrarr.begin(); eit != septrarr.end(); eit++, cutcount++){
        sverts[cutcount]=0;

        OSSIVertexPtr vp1,vp2,v;
        (*eit)->getVertexPointers(vp1,vp2);

        int vnum=0;
        Vector3d * vlocs = new Vector3d[vp1->valence() + vp2->valence()];

        for(int j=0;j<2;j++){
        v =(j)?vp2:vp1;
        OSSIEdgePtrArray vedges;
        v->getEdges(vedges);

        for(eit2 = vedges.begin(); eit2 != vedges.end(); eit2++){
            if ( (*eit2) == (*eit) ) continue;
            OSSIVertexPtr ev1,ev2;
            (*eit2)->getVertexPointers(ev1,ev2);
            if (ev2==v){
                ev2 = ev1;
                ev1 = v;
            }
            vlocs[vnum++] = (ev2->getCoords() - ev1->getCoords())*offsetE + ev1->getCoords();
        }
        }

        //calculate the normal and position
        Vector3d vecp,vec,n(0,0,0),nprev,norm(0,0,0),mid(0,0,0);
        for(int j=0;j<vnum;j++)
        mid += vlocs[j];
        mid /= ((float)vnum);

        for(int j=0;j<vnum;j++){
        vec = (mid - vlocs[j]);
        if (j==0){
            vecp = vec;
            continue;
        }
        n = n + vecp % vec;
        }

        OSSIFacePtr f1,f2;
        (*eit)->getFacePointers(f1,f2);
        norms[cutcount] = normalized(f1->computeNormal()+f2->computeNormal());
        locs[cutcount] = mid;
        sverts[cutcount] = vp1;
        sedges[cutcount] = (*eit);

        }


        for(int i = 0; i<cutcount;i++){
        if (global)
        peelByPlane(obj,norms[i],locs[i]);
        else	localCut(obj, sverts[i],norms[i],locs[i]);
        }

        delete [] norms;
        delete [] locs;
        delete [] sverts;
        delete [] sedges;
    }//end performCutting Function

    void cutSelectedVertices( OSSIObjectPtr obj, float offsetE,float offsetV, bool global,bool selected) {


        vector<OSSIVertexPtr>::iterator vit;
        vector<OSSIEdgePtr>::iterator eit;
        OSSIVertexPtrArray svptrarr = obj->sel_vptr_array;

        OSSIEdgePtrArray edges;
        OSSIVertexPtrArray verts;
        OSSIFacePtrArray faces;

        cutcount = 0;
        int vcount = 0;
        int num=0;

        Vector3d * locs, * norms;
        obj->getEdges(edges);
        obj->getVertices(verts);
        obj->getFaces(faces);
        int esize 	= edges.size();
        int vsize		= verts.size();
        int fsize 	= faces.size();
        int totalsize 	= esize+vsize+fsize;
        locs   		= new Vector3d[totalsize];
        norms  		= new Vector3d[totalsize];
        sverts 		= new OSSIVertexPtr[totalsize];
        sedges 		= new OSSIEdgePtr[esize];

        //loop through all selected vertices
        cutcount = 0;
        for(vit = svptrarr.begin(); vit != svptrarr.end(); vit++, cutcount++){
            sverts[cutcount]=0;

            OSSIEdgePtrArray vedges;
            (*vit)->getEdges(vedges);

            int vnum=0;
            Vector3d * vlocs = new Vector3d[(*vit)->valence()];

        //loop through the edges for the current vertex
            for(eit = vedges.begin(); eit != vedges.end(); eit++){

                OSSIVertexPtr vp1,vp2;
                (*eit)->getVertexPointers(vp1,vp2);
        //swap the vertex pointers if necessary
                if (vp2== (*vit)){
                    vp2 = vp1;
                    vp1 = (*vit);
                }
                vlocs[vnum++] = (vp2->getCoords() - vp1->getCoords())*offsetV + vp1->getCoords();
            }

        //calculate the normal and position for bevelling
            Vector3d vecp,vec,n(0,0,0),nprev,norm(0,0,0),mid(0,0,0);
            for(int j=0;j<vnum;j++)
                mid += vlocs[j];
            mid /= ((float)vnum);

            norms[cutcount] = (*vit)->computeNormal();
            locs[cutcount] = mid;
            sverts[cutcount] = (*vit);
        }

        for(int i = 0; i<cutcount;i++){
            if (global)
                peelByPlane(obj,norms[i],locs[i]);
            else
                localCut(obj, sverts[i],norms[i],locs[i]);
        }

        delete [] norms;
        delete [] locs;
        delete [] sverts;
        delete [] sedges;
    }//end cutSelectedVertices Function

    int isMarked(OSSIVertexPtr vp){
        OSSIEdgePtrArray vedges;
        OSSIEdgePtr ep;
        vp->getEdges(vedges);
        for(int j=0;j<vedges.size();j++){
            ep = vedges.at(j);
            if (ep->isdummy) return 1;
        }
        return 0;
    }

    void autoMarkEdges(OSSIObjectPtr obj){
        OSSIEdgePtrArray edges;
        OSSIVertexPtr vp,vp1,vp2;
        OSSIEdgePtrArray vedges;
        OSSIVertexPtrArray verts;
        OSSIEdgePtr ep;
        int E = 1;
        if(E){
            obj->getEdges(edges);
            //sort edge list from big to small
            OSSIEdgePtr emax,e,etmp;
            int size = edges.size();
            OSSIEdgePtr sorted[size];

            for(int i=0;i<size;i++)
                sorted[i] = edges.at(i);
        for(int j = 1; j < size; j++){
        e = sorted[j];
                int i;
          for(i = j - 1; (i >= 0) && (sorted[i]->length() < e->length()); i--)
                    sorted[i+1] = sorted[i];
                sorted[i+1] = e;
            }
            //sort edges
            for(int i=0;i<size;i++){
                ep = sorted[i];
                if (ep->isdummy) continue;
                ep->getVertexPointers(vp1,vp2);
                if ((isMarked(vp1))||(isMarked(vp2))) continue;
                ep->isdummy = 1;
            }
        }
        else {
            obj->getVertices(verts);
            for(int i=0;i<verts.size();i++){
                vp = verts.at(i);
                vp->getEdges(vedges);
                for(int j=0;j<vedges.size();j++){
                    ep = vedges.at(j);
                    if (ep->isdummy) continue;
                    ep->getVertexPointers(vp1,vp2);
                    if ((isMarked(vp1))||(isMarked(vp2))) continue;
                    ep->isdummy = 1;
                }
            }
        }
    }

    int getCutIndex(OSSIVertexPtr vp){
        for(int i=0;i<cutcount;i++)
            if (sverts[i]==vp) return i;
        return -1;
    }

    int getCutIndex(OSSIEdgePtr ep){
        for(int i=0;i<cutcount;i++)
            if (sedges[i]==ep) return i;
        return -1;
    }
} // end namespace

