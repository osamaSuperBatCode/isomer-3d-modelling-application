/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#include <OSSIObject.h>
#include <OSSICore.h>
#include <OSSICoreExt.h>
#include "OSSIConvexHull.h"

namespace OSSI {

  static int s = 0;
  static OSSIConvexHull * convexhull = NULL;

  //these needs to be declared outside the function for local-cut
  static OSSIVertexPtr * sverts;
  static OSSIEdgePtr * sedges;
  static int cutcount;
    static OSSIFaceVertexPtr * newcorners;
    static OSSIEdgePtr * edges2del;

    static int e2dsize, ncsize;

  void createConvexHull( OSSIObjectPtr obj );

  void createDualConvexHull( OSSIObjectPtr obj );
  OSSIObjectPtr createDualConvexHull( OSSIObjectPtr obj, const Vector3dArray &ovarray);

  void peelByPlane( OSSIObjectPtr obj, Vector3d normal,Vector3d P0);
    void localCut(OSSIObjectPtr obj, OSSIVertexPtr vp,Vector3d normal,Vector3d P0);

  void performCutting( OSSIObjectPtr obj, int type,float offsetE,float offsetV,bool global,bool selected) ;
    void cutSelectedFaces( OSSIObjectPtr obj, float offsetE,float offsetV,bool global=false,bool selected=false);
    void cutSelectedEdges( OSSIObjectPtr obj, float offsetE,float offsetV,bool global=false,bool selected=false);
    void cutSelectedVertices( OSSIObjectPtr obj, float offsetE,float offsetV,bool global=false,bool selected=false);

    int isMarked(OSSIVertexPtr vp);
    void autoMarkEdges(OSSIObjectPtr obj);
    int getCutIndex(OSSIVertexPtr vp);
    int getCutIndex(OSSIEdgePtr ep);

} // end namespace
