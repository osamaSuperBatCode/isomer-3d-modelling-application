/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSICAST_H_
#define _OSSICAST_H_

#include <OSSIObject.h>

/*
 * Cast Objects to primitive shapes. Currently only sphere and plane
 */
namespace OSSI {
  void spheralize( OSSIObjectPtr obj );
  void planarize ( OSSIObjectPtr obj );
} // end namespace

#endif // _OSSICAST_H_
