/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSISUBDIV_H_
#define _OSSISUBDIV_H_

#include <OSSIObject.h>

namespace OSSI {

  void loopSubdivide( OSSIObjectPtr obj );
  void checkerBoardRemeshing(OSSIObjectPtr obj, double thickness=0.33);
  void simplestSubdivide( OSSIObjectPtr obj );
  void vertexCuttingSubdivide(OSSIObjectPtr obj, double offset=0.25);
  void pentagonalSubdivide2(OSSIObjectPtr obj, double scale_factor=0.75);
  void pentagonalSubdivide(OSSIObjectPtr obj, double offset=0);
  void honeycombSubdivide(OSSIObjectPtr obj);
  bool dooSabinSubdivide(OSSIObjectPtr obj, bool check=true /*,QProgressDialog *progress = 0*/);
  void dooSabinSubdivideBC(OSSIObjectPtr obj, bool check=true);
  void dooSabinSubdivideBCNew(OSSIObjectPtr obj, double sf, double length);
  void cornerCuttingSubdivide(OSSIObjectPtr obj, float alpha);
  void modifiedCornerCuttingSubdivide(OSSIObjectPtr obj, double thickness);
  void modifiedCornerCuttingSubdivide2(OSSIObjectPtr obj, double thickness);
  void root4Subdivide(OSSIObjectPtr obj, double a=0.0, double twist=0.0);
  void catmullClarkSubdivide( OSSIObjectPtr obj );
  void starSubdivide(OSSIObjectPtr obj, double offset = 0.0);
  void sqrt3Subdivide( OSSIObjectPtr obj );
  void fractalSubdivide(OSSIObjectPtr obj, double offset = 1.0);
  void stellateSubdivide( OSSIObjectPtr obj );
  void twostellateSubdivide(OSSIObjectPtr obj, double offset, double curve);
  void domeSubdivide(OSSIObjectPtr obj, double length, double sf);
  void dual1264Subdivide(OSSIObjectPtr obj, double sf);
  void loopStyleSubdivide(OSSIObjectPtr obj,double length);

  void setOldVertexType(OSSIObjectPtr obj);
  void setNewSubVertexType(OSSIObjectPtr obj);
  void setNormalVertexType(OSSIObjectPtr obj);

  // Face Subdivision
  void subdivideFace(OSSIObjectPtr obj, OSSIFacePtr faceptr, bool usequads=true);
  void subdivideFace(OSSIObjectPtr obj, uint face_index, bool usequads=true);
  void subdivideFaces(OSSIObjectPtr obj, OSSIFacePtrArray fparray, bool usequads=true);
  void subdivideFaces(OSSIObjectPtr obj, OSSIFacePtrList fplist, bool usequads=true);
  void subdivideAllFaces(OSSIObjectPtr obj, bool usequads=true);
    void triangulateFaces(OSSIObjectPtr obj, OSSIFacePtrList fplist);
    void triangulateAllFaces(OSSIObjectPtr obj);

} // end namespace

#endif // _OSSISUBDIV_H_
