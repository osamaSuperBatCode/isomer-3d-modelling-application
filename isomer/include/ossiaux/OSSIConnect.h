/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSICONNECT_H_
#define _OSSICONNECT_H_

// Non-inline function definitions for class OSSIObject
// Subroutines that are related to connections between edges/faces

#include <OSSIObject.h>

namespace OSSI {

  void connectEdges( OSSIObjectPtr obj, OSSIEdgePtr eptr1, OSSIFacePtr fptr1,
             OSSIEdgePtr eptr2, OSSIFacePtr fptr2, bool check=true);
  void connectEdgesWithoutLoopCheck( OSSIObjectPtr obj, OSSIEdgePtr eptr1, OSSIFacePtr fptr1,
                     OSSIEdgePtr eptr2, OSSIFacePtr fptr2, bool check=true);
  void connectFaces( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2);
  void dualConnectFaces( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2);
  void connectFaces( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2,
             int numsegs, int maxconn = -1, double pinch=1, double pinchCenter=0.5, double bubble=1.0 );
  void connectFaces( OSSIObjectPtr obj, OSSIFacePtr fp1, OSSIFacePtr fp2, int numsegs = 1);
  void hermiteConnectFaces( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2,
                int numsegs, double wt1, double wt2, int maxconn=-1, int numtwists=0,
                double pinch=1, double pinchCenter=0.5, double bubble=1.0);
  void bezierConnectFaces( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2,
               int numsegs, double wt1, double wt2);
} // end namespace

#endif // _OSSICONNECT_H_
