TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib #dll # build shared library
# CONFIG += debug warn_off create_prl
CONFIG += debug warn_off create_prl
TARGET = ossiaux
INCLUDEPATH += .. ../vecmat ../ossicore
DESTDIR = ../../lib

macx {
 # compile release + universal binary
 #QMAKE_LFLAGS += -F../../lib
 #LIBS += -framework vecmat -framework ossicore
 CONFIG += x86 ppc
 #CONFIG += lib_bundle
 #QMAKE_BUNDLE_EXTENSION = .framework
#} else:unix {
 QMAKE_LFLAGS += -L../../lib
 #LIBS += -lvecmat -lossicore
}

HEADERS += \
        OSSICast.h  \
        OSSIConnect.h  \
        OSSIConvexHull.h  \
        OSSICrust.h  \
        OSSIDual.h  \
        OSSIExtrude.h  \
        OSSIMeshSmooth.h  \
        OSSIMultiConnect.h  \
        OSSISculpting \
        OSSISubdiv.h \
    OSSISculpting.h

SOURCES += \
        OSSICast.cpp  \
        OSSIConnect.cpp  \
        OSSIConvexHull.cpp  \
        OSSICrust.cpp  \
        OSSIDual.cpp  \
        OSSIExtrude.cpp  \
        OSSIMeshSmooth.cpp  \
        OSSIMultiConnect.cpp \
        OSSISculpting.cpp \
        OSSISubdiv.cpp
