/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSIEXTRUDE_H_
#define _OSSIEXTRUDE_H_

#include <OSSICoreExt.h>
#include <OSSIObject.h>

namespace OSSI {

  OSSIFacePtr duplicateFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double offset, double rot, double sf);
  OSSIFacePtr duplicateFace(OSSIObjectPtr obj, OSSIFacePtr fptr, const Vector3d& dir, double offset, double rot, double sf);

  OSSIFacePtr duplicateFacePlanarOffset(OSSIObjectPtr obj, OSSIFacePtr fptr, double offset, double rot, double thickness, bool fractionalthickness);
  OSSIFacePtr duplicateFacePlanarOffset(OSSIObjectPtr obj, OSSIFacePtr fptr, const Vector3d& dir, double offset, double rot, double thickness, bool fractionalthickness);



  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, int num);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double rot, double sf = 1.0);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, int num, double rot, double sf = 1.0);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, int num);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, double rot, double sf = 1.0);
  OSSIFacePtr extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, int num, double rot, double sf = 1.0);

  OSSIFacePtr extrudeFacePlanarOffset(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double rot, double thickness, bool fractionalthickness);
  OSSIFacePtr extrudeFacePlanarOffset(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, double rot, double thickness, bool fractionalthickness);

  OSSIFacePtr extrudeFaceDS(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double twist = 0.0, double sf = 1.0);
  OSSIFacePtr extrudeFaceDS(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, int num, double twist = 0.0, double sf = 1.0);
  OSSIFacePtr extrudeFaceDS(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, double twist = 0.0, double sf = 1.0);
  OSSIFacePtr extrudeFaceDS(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, int num, double twist = 0.0, double sf = 1.0);

  OSSIFacePtr extrudeDualFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double rot=0.0, double sf=1.0, bool mesh=false);
  OSSIFacePtr extrudeDualFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, int num, double rot=0.0, double sf=1.0, bool mesh=false);
  OSSIFacePtr extrudeDualFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, double rot=0.0, double sf=1.0, bool mesh=false);
  OSSIFacePtr extrudeDualFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir, int num, double rot=0.0, double sf=1.0, bool mesh=false);

  void stellateFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d);
  void stellateFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, const Vector3d& dir);

  void doubleStellateFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d);

  OSSIFacePtr extrudeHexTileFace(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double rot=0.0, double sf = 1.0);

  OSSIFacePtr extrudeFaceDodeca(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, int num, double rot=0.0, double sf = 1.0, bool hexagonalize=false);
  OSSIFacePtr extrudeFaceDodeca(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double rot=0.0, double sf = 1.0);

    OSSIFacePtr extrudeFaceDodeca(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, int num, double ex_dist1, double ex_dist2, double ex_dist3, bool hexagonalize);
    OSSIFacePtr extrudeFaceDodeca(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, double ex_dist1, double ex_dist2, double ex_dist3);
    OSSIFacePtr extrudeDodeca_Symmetric(OSSIObjectPtr obj, OSSIFacePtr fptr, double d, double rot, double sf, int modfactor);

    OSSIFacePtr extrudeFaceCubOcta(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, int num, double ex_dist1, double ex_dist2, double ex_dist3);
    OSSIFacePtr extrudeFaceCubOcta(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, double ex_dist1, double ex_dist2, double ex_dist3);
    OSSIFacePtr extrudeFaceSmallRhombiCubOcta(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, int num, double ex_dist1, double ex_dist2, double ex_dist3);
    OSSIFacePtr extrudeFaceSmallRhombiCubOcta(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, double ex_dist1, double ex_dist2, double ex_dist3);

    Vector3d GetRingPosition(OSSIVertexPtrList :: iterator vl_ring, int position, int numfaces);
    OSSIVertexPtr GetRingVertex(OSSIVertexPtrList :: iterator vl_ring, int position, int numfaces);

    OSSIFacePtr extrudeFaceIcosa(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, int num, double ex_dist1, double ex_dist2, double ex_dist3);
    OSSIFacePtr extrudeFaceIcosa(OSSIObjectPtr obj, OSSIFacePtr fptr, double angle, double ex_dist1, double ex_dist2, double ex_dist3);




  void extrudeFace(OSSIObjectPtr obj, OSSIFacePtr fptr);

    void extrudeFaceDome(OSSIObjectPtr obj, OSSIFacePtr fptr, double length, double rot=0.0, double sf=1.0);

} // end namespace OSSI

#endif // _OSSIEXTRUDE_H_
