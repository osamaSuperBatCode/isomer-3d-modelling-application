/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSICRUST_H_
#define _OSSICRUST_H_

#include <OSSIObject.h>

namespace OSSI {

  static OSSIFacePtrArray crustfp1;
  static OSSIFacePtrArray crustfp2;
  static int crust_num_old_faces;
  static int crust_min_face_id;

  static OSSIVertexPtrArray crustvp1;
  static OSSIVertexPtrArray crustvp2;
  static int crust_min_vert_id;
  static int crust_num_old_verts;

  /*
    Create a crust for this object.
    Creates an inner (outer if thickness is negative) surface
    duplicating the existing surface and moving every vertex along
    the average normal at the vertex for the given distance.
    Boolean flag indicates if crust should be of uniform thickness
    which means thickness at vertices will be adjusted to account
    for normal averaging.
  */
  void createCrust(const OSSIObjectPtr obj, double thickness, bool uniform = true);

  /*
    Create a crust for this object.
    Creates an inner (outer if scale_factor is negative) surface
    duplicating the existing surface and scaling the inner or outer surface
    w.r.t centroid of object.
  */
  void createCrustWithScaling(OSSIObjectPtr obj, double scale_factor = 0.9 );
  void cmMakeHole(OSSIObjectPtr obj, OSSIFacePtr fp, bool cleanup = true );
  void createCrustForWireframe(OSSIObjectPtr obj, double thickness = 0.1 );
  void createCrustForWireframe2(OSSIObjectPtr obj, double scale_factor = 0.1 );
  void createWireframeWithSegments(OSSIObjectPtr obj, double thickness = 0.1, int numSides = 4);
    void tagMatchingFaces(OSSIObjectPtr obj, OSSIFacePtr fptr);
    void selectMatchingFaces(OSSIObjectPtr obj, OSSIFacePtr fptr, OSSIFacePtrArray &fparray);
    void selectMatchingEdges(OSSIObjectPtr obj, OSSIEdgePtr eptr, OSSIEdgePtrArray &eparray);
    void selectMatchingVertices(OSSIObjectPtr obj, OSSIVertexPtr vptr, OSSIVertexPtrArray &vparray);
    void selectFacesByArea(OSSIObjectPtr obj, OSSIFacePtr fptr, OSSIFacePtrArray &fparray, float delta = 0.1);
    void selectFacesByColor(OSSIObjectPtr obj, OSSIFacePtr fptr, OSSIFacePtrArray &fparray, float delta = 0.0);
  void punchHoles(OSSIObjectPtr obj);
  void makeWireframe(OSSIObjectPtr obj, double crust_thickness = 0.1, bool split = true );
  void makeWireframe2(OSSIObjectPtr obj, double crust_thickness = 0.1, double crust_width = 0.1, bool split = true );
  void makeWireframeWithColumns(OSSIObjectPtr obj, double wireframe_thickness, int wireframe_segments);

} // end namespace

#endif // _OSSICRUST_H_
