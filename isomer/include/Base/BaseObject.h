/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/



#ifndef _OBJECT_H_

#define _OBJECT_H_

// Abstract base class for all classes
// Can use this class to build container classes, which use object pointers
// So the same container classes can be used with any derived classes also
// without need for additional code - share the same code

// BaseObject has no member data. It only has protected constructors (to prevent
// instantiation), virtual destructors and an assignment operator

#ifndef __GNUG__
#include <bool.h>
#include <iostream.h>
#else
#include <iostream>
#endif

#include <stdlib.h>

class BaseObject;
typedef BaseObject * BaseObjectPtr;

class BaseObject
{
  protected :

        // Default constructor - protected to prevent instantiation
     BaseObject()
       {
            // Nothing to do
       }

        // Copy constructor - protected to prevent instantiation
     BaseObject(const BaseObject&)
       {
            // Nothing to do
       }

  public :

        // Assignment operator
     BaseObject& operator = (const BaseObject&)
       {
            // Nothing to do

         return *this;
       }

        // Destructor
     virtual ~BaseObject()
       {
            // Nothing to do
       }

        // Derived class should give a meaningful implementation
        // for the following functions.
        // Classes such as List which use BaseObject pointers
        // will use these functions, for memory management
        // These functions may be made pure virtual later

        // Make a copy of the BaseObject and return a pointer to the new one
     virtual BaseObjectPtr copy(void) const = 0;
};

// A reference class for use in container classes
// Based on http://www.cs.southwestern.edu/~owensb/Algo/STL/ref2.html

class BaseObjectReference
{
  protected :

     BaseObjectPtr referent;                           // What does this object refer to?
     bool          owner;                              // Does this object own the referent?

  public :

        //--- Constructors ---//

     BaseObjectReference()
       : referent(NULL), owner(true)
       {}

     BaseObjectReference(const BaseObject& obj)
       : referent(obj.copy()), owner(true)
       {}

     BaseObjectReference(BaseObjectPtr obj)
       : referent(obj), owner(false)
       {}

     BaseObjectReference(const BaseObjectReference& ref)
       : referent(ref.referent), owner(ref.owner)
       {
         if ( owner && ref.referent ) referent = ref.referent->copy();
       }

        //--- Destructor ---//

     ~BaseObjectReference()
       {
         if ( owner ) delete referent;
       }

        //--- Assignment operator ---//

     BaseObjectReference& operator = (const BaseObjectReference& ref)
       {
         if ( owner ) delete referent;
         owner = true;
         if ( ref.referent ) referent = ref.referent->copy();
         return (*this);
       }

        //--- Operators ---//

     BaseObjectPtr operator -> () const
       {
         return referent;
       }

     operator BaseObject& () const
       {
         return *referent;
       }

     operator BaseObject * () const
       {
         return referent;
       }

     BaseObject& operator * () const
       {
         return *referent;
       }
};

#endif // #ifndef _OBJECT_H_

