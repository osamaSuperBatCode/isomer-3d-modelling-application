/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/



#ifndef _STREAMIO_H_

#define _STREAMIO_H_

// Miscellaneous functions for operating on IO streams in C++

#ifndef __GNUG__
#include <iostream.h>
#include <iomanip.h>
#include <ctype.h>
#include <bool.h>
#else
#include <iostream>
#include <iomanip>
#include <ctype.h>

using namespace std;

#endif


inline void removeWhiteSpace(istream& i)
{
  char c;

  while ( i )
     {
       i.get(c);
       if ( c != ' ' && c != '\t' && c != '\n' )
          {
            i.putback(c);
            break;
          }
     }
}

inline void removeSpaces(istream& i)
{
  char c;

  while ( i )
     {
       i.get(c);
       if ( c != ' ' )
          {
            i.putback(c);
            break;
          }
     }
}

inline void removeSpacesAndTabs(istream& i)
{
  char c;

  while ( i )
     {
       i.get(c);
       if ( c != ' ' && c != '\t' )
          {
            i.putback(c);
            break;
          }
     }
}

inline bool readTillEOL(istream& i)
{
     // Read till newline character occurs or EOF is reached
     // newline character is also read from the stream
     // Returns false if EOF is reached before EOL, true otherwise
  char c;

  i.get(c);
  while ( i && c != '\n' )
     i.get(c);
  if ( c == '\n' ) return true;
  return false;
}

inline bool readTill(istream& i, char c)
{
     // Read till specified character occurs or EOL/EOF is reached
     // Specified character is left in the stream
     // If EOF/EOL occurs before c, returns false, true otherwise
  char ic = i.peek();
  while ( i && ic != c && ic != '\n' )
     {
       i.get(ic); ic = i.peek();
     }
  if ( ic == c ) return true;
  return false;
}

inline bool readTillNumeric(istream& i)
{
     // Read till a numeric digit (0-9) occurs or EOL/EOF is reached
     // If EOL/EOF is reached before a digit occurs, returns false
     // and leaves the EOL/EOF character in i. If a digit is reached
     // returns true and leaves the digit in i
  char ic = i.peek();
  while ( i && !isdigit(ic) && ic != '\n' )
     {
       i.get(ic); ic = i.peek();
     }
  if ( isdigit(ic) ) return true;
  return false;
}

inline bool readTillDigit(istream& i)
{
     // Read till a digit or plus/minus sign occurs or EOL/EOF is reached
     // If EOL/EOF is reached before a digit occurs, returns false
     // and leaves the EOL/EOF character in i. If a digit is reached
     // returns true and leaves the digit in i
  char ic = i.peek();
  while ( i && !isdigit(ic) && ic != '+' && ic != '-' && ic != '\n' )
     {
       i.get(ic); ic = i.peek();
     }
  if ( isdigit(ic) || ic == '+' || ic == '-' ) return true;
  return false;
}

inline bool readTillFloat(istream& i)
{
     // Same as previous subroutine, except that '.' is also allowed
  char ic = i.peek();
  while ( i && !isdigit(ic) && ic != '+' && ic != '-' && ic != '.' && ic != '\n' )
     {
       i.get(ic); ic = i.peek();
     }
  if ( isdigit(ic) || ic == '+' || ic == '-' || ic == '.' ) return true;
  return false;
}

#endif // #ifndef _STREAMIO_H_

