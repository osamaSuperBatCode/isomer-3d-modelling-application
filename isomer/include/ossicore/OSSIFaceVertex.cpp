/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/


/*
 * OSSIFaceVertex.cpp
 */

// Non-inline function definitions for class OSSIFaceVertex

#include "OSSIFaceVertex.h"
#include "OSSIFace.h"

namespace OSSI {

  uint OSSIFaceVertex::suLastID = 0;

  // Default constructor
  OSSIFaceVertex::OSSIFaceVertex( bool bf )
    : vertex(NULL), normal(), color(1), texcoord(), backface(bf), index(0),
      epEPtr(NULL), fpFPtr(NULL), fvpNext(NULL), fvpPrev(NULL), fvtType(FVTNormal),
      auxcoords(), auxnormal()
  { assignID(); fvpNext = this; fvpPrev = this; }

  // 2 arg-constructor - copy the pointers
  OSSIFaceVertex::OSSIFaceVertex( OSSIVertexPtr vptr, OSSIEdgePtr eptr, bool bf )
    : vertex(vptr), normal(), color(1), texcoord(), backface(bf), index(0),
      epEPtr(eptr), fpFPtr(NULL), fvpNext(NULL), fvpPrev(NULL),
      fvtType(FVTNormal), auxcoords(), auxnormal()
  { assignID(); fvpNext = this; fvpPrev = this; }

  // Copy constructor
  OSSIFaceVertex::OSSIFaceVertex( const OSSIFaceVertex& ofv )
    : uID(ofv.uID), vertex(ofv.vertex), normal(ofv.normal), color(ofv.color), texcoord(ofv.texcoord),
      backface(false), index(ofv.index), epEPtr(ofv.epEPtr), fpFPtr(ofv.fpFPtr),
      fvpNext(NULL), fvpPrev(NULL), fvtType(ofv.fvtType), auxcoords(ofv.auxcoords), auxnormal(ofv.auxnormal)
  { fvpNext = this; fvpPrev = this; }

  // Destructor
  OSSIFaceVertex::~OSSIFaceVertex() {}

  // Assignment operator
  OSSIFaceVertex& OSSIFaceVertex::operator=( const OSSIFaceVertex& ofv ) {
    vertex = ofv.vertex; normal = ofv.normal; color = ofv.color; texcoord = ofv.texcoord;
    backface = ofv.backface; index = ofv.index;
    epEPtr = ofv.epEPtr; fpFPtr = ofv.fpFPtr;
    fvpNext = ofv.fvpNext; fvpPrev = ofv.fvpPrev;
    fvtType = ofv.fvtType; auxcoords = ofv.auxcoords; auxnormal = ofv.auxnormal; //tmpp = ofv.tmpp;
    return (*this);
  }

    bool OSSIFaceVertex::operator==(const OSSIFaceVertex &other) const {
        if( getVertexID() == other.getVertexID() &&
                getFaceID() == other.getFaceID() &&
                uID == other.uID ) { return true; }
        else
            return false;
  }

  OSSIFaceVertexPtr OSSIFaceVertex::copy( ) const {
    OSSIFaceVertexPtr fvptr = new OSSIFaceVertex(*this);
    return fvptr;
  }

  // Dump contents of this object
  void OSSIFaceVertex::dump(ostream& o) const {
    o << "     OSSIFaceVertex" << endl
      << "       VertexPtr : " << vertex << endl
      << "       EdgePtr : " << epEPtr << endl
      << "       FacePtr : " << fpFPtr << endl

      << endl;
  }

  void average(const OSSIFaceVertex& ofv1, const OSSIFaceVertex& ofv2, OSSIFaceVertex& ave) {
    // Average 2 face vertexes. Coordinates, normals, texture coordinates and color are averaged
    // Assumes that all 3 face vertexes have valid vertex pointers
    ave.vertex->coords = ((ofv1.vertex->coords)+(ofv2.vertex->coords))/2.0;
    ave.normal = (ofv1.normal + ofv2.normal)/2.0;
    ave.color = (ofv1.color + ofv2.color)/2.0;
    ave.texcoord = (ofv1.texcoord + ofv2.texcoord)/2.0;
  }

  void average(OSSIFaceVertexPtr ofvp1, OSSIFaceVertexPtr ofvp2, OSSIFaceVertexPtr avep) {
    // Average 2 face vertexes. Coordinates, normals, texture coordinates and color are averaged
    // Assumes that all 3 face vertexes have valid vertex pointers
    avep->vertex->coords = ((ofvp1->vertex->coords)+(ofvp2->vertex->coords))/2.0;
    avep->normal = (ofvp1->normal + ofvp2->normal)/2.0;
    avep->color = (ofvp1->color + ofvp2->color)/2.0;
    avep->texcoord = (ofvp1->texcoord + ofvp2->texcoord)/2.0;
  }

  bool coFacial(OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2) {
    return ( fvptr1->getFacePtr() == fvptr2->getFacePtr() );
  }

  bool coFacial(const OSSIFaceVertex& fv1, const OSSIFaceVertex& fv2) {
    return ( fv1.getFacePtr() == fv2.getFacePtr() );
  }

  bool areEqual(OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2) {
    // Test to see if 2 OSSIFaceVertexes are "equal"
    // They are "equal" iff they belong to the same Face AND they
    // refer to the same vertex

    // If either pointer is NULL returns false unless both are NULL in which
    // case it returns true
    if (fvptr1 == NULL) {
      if ( fvptr2 == NULL ) return true;
      return false;
    }  else if ( fvptr2 == NULL ) {
      return false;
    } else {
      // Both pointers are not NULL
      if ( coFacial(fvptr1,fvptr2) && ( fvptr1->getVertexID() == fvptr2->getVertexID() ) )
    return true;
    }
    return false;
  }

  bool areEqual(const OSSIFaceVertex& fv1, const OSSIFaceVertex& fv2) {
    // Test to see if 2 OSSIFaceVertexes are "equal"
    // They are "equal" iff they belong to the same Face AND they
    // refer to the same Position
    if ( coFacial(fv1,fv2) && ( fv1.getVertexID() == fv2.getVertexID() ) )
      return true;
    return false;
  }

  // Check if given corner is a concave corner or not.
  // Will not work properly for non-planar faces.
  // Will not work properly for faces which have edges which belong entirely to that face.
  // should not work properly for highly skewed geometry.
  bool OSSIFaceVertex::isConcaveCorner( ) const {
    // If face is a triangle or a 2-gon or a point-sphere return false
    if ( fpFPtr->size() <= 3 ) return false;

    // Compute the normalized edge vectors starting from the specified corner
    Vector3d ppos = fvpPrev->getVertexCoords();
    Vector3d npos = fvpNext->getVertexCoords();
    Vector3d pos = getVertexCoords();
    Vector3d pvec = ppos-pos; normalize(pvec);
    Vector3d nvec = npos-pos; normalize(nvec);

    // If angle between the 2 edge vectors is 0 degrees or 180 degrees
    // the corner is assumed to be a convex corner
    double sinangle = norm(pvec % nvec);
    if ( sinangle < 1.0e-5 )
      return false;

    // Compute a new point by adding the 2 edge vectors, scaled to a very small length.
    // The choice of the length of the vector is arbitrary and must be chosen to avoid
    // miscalculations because of rounding errors in floating point computations.
    // For a concave corner this point will end up lying outside the polygon
    // For a convex corner this point will end up lying inside the polygon
    Vector3d p = pos + (pvec + nvec) * 0.01;

    // Normal at this corner - for computing projection plane
    // not corrected for concavity
    Vector3d fvn = nvec % pvec; // pvec and nvec are already normalized

    // Check if p is inside the face
    Vector3dArray facepoints;
    fpFPtr->getVertexCoords(facepoints);

    if ( pointInPolygon(p,facepoints,fvn) )
      return false;

    return true;
  }

  // Check if this corner is a winged corner or not
  // A winged corner is a degenerate corner
  // i.e. the angle at the corner is 180 degrees
  bool OSSIFaceVertex::isWingedCorner( ) const {
    // If face is a triangle or a 2-gon or a point-sphere return false
    if ( fpFPtr->size() <= 3 )
      return false;

    // Compute the normalized edge vectors starting from the specified corner
    Vector3d ppos = fvpPrev->getVertexCoords();
    Vector3d npos = fvpNext->getVertexCoords();
    Vector3d pos = getVertexCoords();
    Vector3d pvec = ppos-pos; normalize(pvec);
    Vector3d nvec = npos-pos; normalize(nvec);

    // If angle between the 2 edge vectors is 0 degrees or 180 degrees
    // the corner is a winged corner
    // We can also check this by adding the two vectors and checking if
    // they sum to 0
    if ( norm(pvec+nvec) < 1.0e-5 )
      return true;
    return false;
  }

  // Find the next non-winged corner starting from this corner
  // If none can be found, returns NULL
  OSSIFaceVertexPtr OSSIFaceVertex::nextNonWingedCorner( ) const {
    OSSIFaceVertexPtr fvp = next();
    while ( fvp->isWingedCorner() && fvp != this )
      fvp = fvp->next();
    if ( fvp == this )
      return NULL;
    return fvp;
  }

  // Find the CLOSEST non-winged corner starting from this corner
  // If none can be found, returns NULL
  // Search both forward and backward till we find one.
  OSSIFaceVertexPtr OSSIFaceVertex::closestNonWingedCorner( ) const {
    OSSIFaceVertexPtr fvpn = next(), fvpp = prev(), fvp = NULL;
    while ( fvpn->isWingedCorner() && fvpp->isWingedCorner() && fvpp != fvpn ) {
      fvpn = fvpn->next(); fvpp = fvpp->prev();
    }
    if ( fvpp == fvpn ) {
      if ( ! fvpp->isWingedCorner() )
    fvp = fvpp;
      else if ( ! fvpn->isWingedCorner() )
    fvp = fvpn;
      // WARNING! If the code reaches this point, this function will return NULL
      // This will cause other parts of the program to fail
    } else if ( ! fvpp->isWingedCorner() ) {
      fvp = fvpp;
    } else {
      fvp = fvpn;
    }
    return fvp;
  }


  // Get a vector along the edge starting at this corner
  Vector3d OSSIFaceVertex::getEdgeVector( ) const {
    Vector3d pos = getVertexCoords();
    Vector3d npos = fvpNext->getVertexCoords();
    Vector3d evec = npos-pos;
    return evec;
  }

  // Get 2 vectors along the 2 edges which form this corner
  // Both vectors will originate at this corner
  void OSSIFaceVertex::getEdgeVectors(Vector3d& pvec, Vector3d& nvec) {
    nvec = getEdgeVector();
    pvec = - (fvpPrev->getEdgeVector()); // Reverse direction of edgevector from previous corner
  }

  void OSSIFaceVertex::updateNormal( ) {
    // If this is a winged corner, assign normal of nearest non-winged corner
    // Otherwise compute for this corner and adjust for concave corners
    if ( isWingedCorner() ) {
      OSSIFaceVertexPtr fvp = closestNonWingedCorner();
      normal = fvp->computeNormal();
    } else {
      // compute the normal using adjacent vertices
      Vector3d pvec, nvec;

      getEdgeVectors(pvec,nvec); // Don't have to be normalized
      normal = nvec % pvec;
      normalize(normal);

      // If this corner is a concave corner w.r.t. the face it is in,
      // then reverse the normal.
      if ( isConcaveCorner() )
    normal = -normal;
    }
  }

  void OSSIFaceVertex::addSelfToVertex( ) {
    if ( vertex ) vertex->addToFaceVertexList(this);
  }

  void OSSIFaceVertex::deleteSelfFromVertex( )
  {
    if ( vertex ) vertex->deleteFromFaceVertexList(this);
  }

  void OSSIFaceVertex::addSelfToEdge( ) {
    // Add self to Edge if there is a NULL OSSIFaceVertex pointer in the Edge
    if ( epEPtr ) epEPtr->setNullFaceVertexPtr(this);
  }

  void OSSIFaceVertex::deleteSelfFromEdge( ) {
    // Set the OSSIFaceVertexPtr of Edge which refers to this OSSIFaceVertex to NULL
    if ( epEPtr ) epEPtr->resetFaceVertexPtr(this);
  }

  void OSSIFaceVertex::deleteSelfFromFace( ) {
    // Remove the OSSIFaceVertexPtr from the face to which it belongs.
    // Simply calls the deleteVertexPtr method in the face
    // This can't be properly implemented here since the head pointer of the OSSIFace might
    // need to be adjusted too
    fpFPtr->deleteFaceVertexPtr(this);
  }

  OSSIFaceVertexPtr OSSIFaceVertex::vnext( ) {
    OSSIFaceVertexPtr vn = this;
    if ( epEPtr )
      {
    vn = epEPtr->getOtherFaceVertexPtr(vn);
    vn = vn->next();
      }
    return vn;
  }

  OSSIFaceVertexPtr OSSIFaceVertex::vprev( ) {
    OSSIFaceVertexPtr vn = this;
    if ( epEPtr ) {
      vn = vn->prev();
      vn = vn->epEPtr->getOtherFaceVertexPtr(vn);
    }
    return vn;
  }

  void OSSIFaceVertex::print(bool printadjacent) const {
    cout << "OSSIFaceVertex : "
     << "Vertex (" << vertex->getID() << ") "
     << "Face (" << fpFPtr->getID() << ") "
     << "Edge (" << epEPtr->getID() << ") " << endl;
    if ( printadjacent ) {
      cout << "Previous "; fvpPrev->print();
      cout << "Next "; fvpNext->print();
    }
  }

  void OSSIFaceVertex::writeOSSI(ostream& o, uint newindex) {
    o << "fv " << vertex->getIndex() << ' '
      << normal << ' '
      << texcoord << ' ' << endl;
    index = newindex;
  }

  uint OSSIFaceVertex::readOSSI(istream& i) {
    // Assume that the "fv" characters have already been read
    // Read the vertex index and return it, so the caller can set the vertex pointer
    // using the index to reference the vertex list
    uint vindex;
    i >> vindex >> normal >> texcoord;
    return vindex;
  }

  uint OSSIFaceVertex::getFaceID( ) const { return fpFPtr->getID(); }

  void advance(OSSIFaceVertexPtr& fvp)
  {
    fvp = fvp->fvpNext;
  }

  void backward(OSSIFaceVertexPtr& fvp)
  {
    fvp = fvp->fvpPrev;
  }


} // end namespace

