/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSIFace.h
 */

#ifndef _OSSI_FACE_H_
#define _OSSI_FACE_H_

// Class for a Face to be used in the OSSIObject class
// Based on the OBJFace class

// A OSSIFace is just a list of OSSIFaceVertexes
// The ordering of the OSSIFaceVertexes is important
// A OSSIFace can be implemented as a 2-3 Tree or as a linked list

#include "OSSICommon.h"
#include "OSSIFaceVertex.h"
#include "OSSIMaterial.h"


namespace OSSI {

  class OSSIFace {
  public :
    static void setLastID( uint id ) {
      if( id > suLastID )
                suLastID = id;
    }
  protected :

    static uint suLastID;                             // Distinct ID for each instance

    static uint newID(void) {                           // Generate a new unique ID
      uint temp = suLastID;
      suLastID++;
      return temp;
    }

    uint uID;                                         // ID for this Face
    OSSIFaceVertexPtr     head;                       // Head of list of face-vertex pointers
    OSSIMaterialPtr       matl_ptr;                   // Pointer to material for this face
    OSSIFaceType          ftType;                     // For use in subdivision surfaces
    Vector3d              auxcoords;                  // Coords for use during subdivs, etc.
    Vector3d              auxnormal;                  // Extra storage for normal

  public :

    Vector3d              centroid;                   // Centroid of this face (not always current)
    Vector3d              normal;                     // Normal to this face. Not always correct/current
    unsigned long         flags;                      // Variable for general use to store flags, etc.
    uint ismarked;

  protected :

     // Assign a unique ID for this instance
    void assignID(void) {
      uID = OSSIFace :: newID();
      ismarked = 0;
    }

    // Advance the pointer by given number of nodes. Similar to STL advance algorithm
    OSSIFaceVertexPtr advance(OSSIFaceVertexPtr ptr, uint num) const;

    // Copy all face-vertices from another face specified by it's head pointer
    void copy(OSSIFaceVertexPtr ptr);

  public :

    // Default and 1 arg constructor
    OSSIFace( OSSIMaterialPtr tmp = NULL);

    // Copy constructor
    OSSIFace(const OSSIFace& face);

    // Destructor
    ~OSSIFace();

    // Assignment operator
    OSSIFace& operator = (const OSSIFace& face);

    OSSIFacePtr copy(void) const {
      OSSIFacePtr new_face = new OSSIFace(*this);
      return new_face;
    }

    // Dump contents of object
    void dump(ostream& o) const;

    Vector3d geomSum(void) const;

    // Sum of texture coordinates
    Vector2d textureSum(void) const;

    // Color sum
    RGBColor colorSum(void) const;

    // Normal sum
    Vector3d normalSum(void) const;

    // Get all sums
    void getSums(Vector3d& sumg, Vector2d& sumtc, RGBColor& sumc, Vector3d& sumn) const;

    // Update the centroid of this face
    void updateCentroid(void);

    // Update the centroid of this face and return it
    Vector3d geomCentroid(void) {
      updateCentroid();
      return centroid;
    }

    // Compute the texture centroid
    Vector2d textureCentroid(void) const;

    // Compute the color centroid
    RGBColor colorCentroid(void) const;

    // Update the normal of this face
    void updateNormal(void);

    // Compute the normal centroid
    Vector3d normalCentroid(void) {
      //updateNormal();
      return normal;
    }

    // Compute the normal for this face and send it to the face-vertices
    Vector3d computeNormal(void);

    // Compute the normal for each face-vertex and store it in the face-vertex itself
    // Don't do any averaging, etc.
    void storeNormals(void);

    // Compute all centroids
    void getCentroids(Vector3d& cen, Vector2d& texc, RGBColor& colc, Vector3d& nc) const;

    //--- Query Functions ---//

    uint getID(void) const {
      return uID;
    }

    OSSIFaceType getType(void) const {
      return ftType;
    }

    OSSIMaterialPtr material(void) const {
      return matl_ptr;
    }

    Vector3d getAuxCoords(void) const {
      return auxcoords;
    }

    Vector3d getAuxNormal(void) const {
      return auxnormal;
    }

    Vector3d getNormal(bool update=false) {
            if(update)
                updateNormal();
      return normal;
    }

    Vector3d getCentroid(void) const {
      return centroid;
    }

    //--- Mutative Functions ---//

    void makeUnique(void) {
      assignID();
    }

    // Delete all the face-vertices of this face
    void destroy(void);

    void reset(void) {
      destroy();
      matl_ptr = NULL;
    }

    OSSIFaceVertexPtr addVertex(const OSSIFaceVertex& dfv);        // Insert a copy
    OSSIFaceVertexPtr addVertex(OSSIFaceVertexPtr dfvp);           // Insert a copy
    void addVertexPtr(OSSIFaceVertexPtr dfvp);        // Insert the pointer

    // Delete given FaceVertex from face. Adjust pointers of adjacent face-vertices
    // Memory for given face-vertex is not freed. Given FaceVertex must belong to this Face
    // If given face-vertex is the only face-vertex in this face, nothing is done (for now)
    void deleteVertexPtr(OSSIFaceVertexPtr dfvp);

    // Same as above, but more correct function name
    void deleteFaceVertexPtr(OSSIFaceVertexPtr dfvp) {
      deleteVertexPtr(dfvp);
    }

    // Get the coordinates of the vertices of the face
    void getVertexCoords(Vector3dArray& verts);

    // STL list type access to the Face
    OSSIFaceVertexPtr front(void) {
      return head;
    }

    OSSIFaceVertexPtr back(void) {
      OSSIFaceVertexPtr b = NULL;
      if ( head ) b = head->prev();
      return b;
    }

    // Return the first/last element of the list
    OSSIFaceVertexPtr firstVertex(void) {
      return front();
    }

    OSSIFaceVertexPtr lastVertex(void) {
      return back();
    }

    // Set the material pointer
    void setMaterial(OSSIMaterialPtr mptr);

    void setType(OSSIFaceType type) {
      ftType = type;
    }

    void resetType(void) {
      ftType = FTNormal;
    }

    void setAuxCoords(const Vector3d& p) {
      auxcoords = p;
    }

    void setAuxNormal(const Vector3d& n) {
      auxnormal = n;
    }

    void addToAuxCoords(const Vector3d& p) {
      auxcoords += p;
    }

    void addToAuxNormal(const Vector3d& n) {
      auxnormal += n;
    }

    void resetAuxCoords(void) {
      auxcoords.reset();
    }

    void resetAuxNormal(void) {
      auxnormal.reset();
    }

    uint size(void) const;                             // No. of vertices in this face
    uint numFaceVertexes(void) const {
      return size();
    }

    void resetTypeDeep(void); // Reset type of Face and all FaceVertexes in this Face

    // Compute color for each corner using given light and using material of Face
    //void computeLighting(LightPtr lightptr);

    // Update the OSSIFacePtr for each OSSIFaceVertex
    void updateFacePointers(void);
    void resetFacePointers(void);

    // Update the OSSIFaceVertexList for each OSSIVertex referred to by each OSSIFaceVertex
    void addFaceVerticesToVertices(void);
    void deleteFaceVerticesFromVertices(void);

    // Update the OSSIFaceVertexPtr for each OSSIEdge connected to each OSSIFaceVertex
    void addFaceVerticesToEdges(void);
    void deleteFaceVerticesFromEdges(void);

    // Update OSSIEdges and OSSIVertices - above functions combined for efficiency
    void addFaceVerticesToVerticesAndEdges(void);
    void deleteFaceVerticesFromVerticesAndEdges(void);

    // Create an array of OSSIEdges for this OSSIFace. Number of OSSIEdges in the array
    // is returned. Memory will be allocated inside this function, which should
    // be freed by the caller. Pass a pointer to the array (OSSIEdge **)
    int getEdges(OSSIEdge ** edges);

    // Get the edges in the face. Different from previous function. Doesn't create
    // new edges, simply stores existing edge pointer from the face vertices into
    // the STL array or STL linked list
    void getEdges(OSSIEdgePtrArray& edges);
    void getEdges(OSSIEdgePtrList& edges);

    // Get the corners in the face. Stores the existing OSSIFaceVertexPtr's in the face
    // into the array
    void getCorners(OSSIFaceVertexPtrArray& corners);

        // Helpful for python interface
        OSSIFaceVertexPtr findFaceVertex( uint vertexId ) const;
        OSSIFaceVertexPtr findFaceVertexByID( uint faceVertexId ) const;

    // Get the corners and the coordinates
    void getCornersAndCoords(OSSIFaceVertexPtrArray& corners, Vector3dArray& coords);

    // Does this Face contain the given face-vertex?
    bool contains(OSSIFaceVertexPtr dfvp);

    // Does this face share only one vertex with the given face?
    bool sharesOneVertex(OSSIFacePtr dfp);

    void getNeighboringFaces(OSSIFacePtrArray& fparray);

    // Find the OSSIFaceVertex following the given OSSIFaceVertex in this OSSIFace
    // If only one OSSIFaceVertex is there in the OSSIFace returns itself
    OSSIFaceVertexPtr nextFaceVertex(OSSIFaceVertexPtr fvptr);

    // Find the OSSIFaceVertex preceding the given OSSIFaceVertex in this OSSIFace
    // If only one OSSIFaceVertex is there in the OSSIFace returns itself
    OSSIFaceVertexPtr prevFaceVertex(OSSIFaceVertexPtr fvptr);

    // Find the FaceVertex which is closest to the given point
    OSSIFaceVertexPtr findClosest(const Vector3d& p);

    // Find the two corners which are closest to each other in the two faces
    // The passed OSSIFaceVertex pointers will be set to the closest corners,
    // the first one from the first face and the second one from the second face
    static void findClosestCorners(OSSIFacePtr fp1, OSSIFacePtr fp2,
                   OSSIFaceVertexPtr& fvp1, OSSIFaceVertexPtr& fvp2);

    // Add a new OSSIFaceVertex after/before a given OSSIFaceVertex in this OSSIFace
    OSSIFaceVertexPtr insertAfter(OSSIFaceVertexPtr fvptr, OSSIFaceVertexPtr new_fvp, bool copy=true);
    OSSIFaceVertexPtr insertBefore(OSSIFaceVertexPtr fvptr, OSSIFaceVertexPtr new_fvp, bool copy=true);

    // Reorder the face-vertices so that the given face-vertex is the first one
    // Given face vertex should belong to this face
    void reorder(OSSIFaceVertexPtr fvptr);

  public :
    // Reverse the face - reverses the order of the face-vertices
    // WARNING!! This operation invalidates the OSSI structure. All edges
    // associated with this face will be in an invalid state.
    void reverse(void);

  protected :
    // Classes which need to access the reverse method should be declared as friends here
    friend class OSSIObject;

  public :

    // Output the boundary walk of the face
    vector<int> vertexWalk( ) const;
    vector<int> edgeWalk( ) const;
    void boundaryWalk(void) const;
    void print(void) const {
      boundaryWalk();
    }

    friend void boundaryWalk(OSSIFacePtr faceptr);

    friend void boundaryWalk(const OSSIFace& face);


    // For debugging
    void printPointers(void) const;

    // Write out the Face in OBJ format to an output stream - source for more info
    // min_id is the minimum ID value. It will subtracted from the ID before output
    void objWrite(ostream& o, uint min_id) const;
    void objWriteWithNormals(ostream& o, uint min_id, uint& normal_id_start) const;
    void objWriteWithTexCoords(ostream& o, uint min_id,
                   uint& tex_id_start) const;
    void objWriteWithNormalsAndTexCoords(ostream& o, uint min_id,
                     uint& normal_id_start,
                     uint& tex_id_start) const;

    // Write out the normals for each vertex in the Face in OBJ format
    void objWriteNormals(ostream& o) const;

    // Write out the texture coordinates for each vertex in the Face in OBJ format
    void objWriteTexCoords(ostream& o) const;

    // Write out the face in OSSI format
    void writeOSSI(ostream& o) const;

    // Write out the face in OSSI format but in reverse order. Useful for crust modeling
    void writeOSSIReverse(ostream& o) const;

    // Access the face-vertex specified by index. No range checks done
    OSSIFaceVertexPtr facevertexptr(uint index);
    OSSIFaceVertex& facevertex(uint index);
    OSSIFaceVertex facevertex(uint index) const;

    // Access the vertex specified by index. No range checks done
    OSSIVertexPtr vertexptr(uint index);
    OSSIVertex& vertex(uint index);
    OSSIVertex vertex(uint index) const;

    // Access the coordinates of the vertex specified by index. No range checks done
    Vector3d& vertex_coords(uint index);
    Vector3d vertex_coords(uint index) const;

    // Access color and normal of face vertex specified by index
    RGBColor& fv_color(uint index);
    RGBColor fv_color(uint index) const;

    Vector3d& fv_normal(uint index);
    Vector3d fv_normal(uint index) const;

    // Set the color of all face vertices of this face
    void setColor(const RGBColor& col);
    void setColor(double r, double g, double b);

    // FOR QUADS ONLY - Randomly assign texture coordinates to the 4 corners to match a 1,1 square
    void randomAssignTexCoords(void);

    void updateMaterial(void);

        float getArea();


    void for_each(void (*func)(OSSIFaceVertexPtr)) const;


    friend ostream& operator << (ostream& o, const OSSIFace& face);
  };

  void boundaryWalk(OSSIFacePtr faceptr);
  void boundaryWalk(const OSSIFace& face);


} // end namespace

#endif /* #ifndef _OSSI_FACE_H_ */

