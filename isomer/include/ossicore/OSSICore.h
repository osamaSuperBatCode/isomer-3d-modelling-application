/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/


/*
 * OSSICore.h
 */

#ifndef _OSSICORE_H_
#define _OSSICORE_H_

#include "OSSIObject.h"

namespace OSSI {

    //* Insert Edge *


  int insertEdge( OSSIObjectPtr obj,
                                    uint &faceId1, uint &vertId1,
                                    uint &faceId2, uint &vertId2,
                                    uint &faceId1b, uint &vertId1b,
                                    uint &faceId2b, uint &vertId2b,
                                    bool check,
                                    bool set_type = false );

  /*
   *
   * The general case insertEdge subroutine. Calls one of the insertEdge implementations,
   * depending on whether the corners are cofacial or not. If the 2 corners are cofacial
   * checks to see if the 2 pointers refer to the same corner, if so doesn't do insert.
   */
  OSSIEdgePtr insertEdge( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2, bool set_type = false, OSSIMaterialPtr matl = NULL  );
  OSSIEdgePtr insertEdgeCoFacial( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2, bool set_type = false );
  OSSIEdgePtr insertEdgeNonCoFacial( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2, OSSIMaterialPtr matl = NULL );
  OSSIEdgePtr insertEdgeWithoutCheck( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2, bool set_type = false, OSSIMaterialPtr matl = NULL );


  // * Delete Edge *


  // If the cleanup flag is true, any point-spheres created
  // because of the edge deletion will be removed from the object
    std::vector<int>  deleteEdgeID( OSSIObjectPtr obj, uint edgeId, bool cleanup = true );
  void deleteEdge( OSSIObjectPtr obj, uint edge_index, bool cleanup = true );
  OSSIFacePtrArray deleteEdge( OSSIObjectPtr obj, OSSIEdgePtr edgeptr, bool cleanup = true );


  // * Collapse Edge *


  int  collapseEdgeID( OSSIObjectPtr obj, const uint edgeId, bool cleanup = true );
  void collapseEdge( OSSIObjectPtr obj, uint edge_index, bool cleanup = true );
  OSSIVertexPtr collapseEdge( OSSIObjectPtr obj, OSSIEdgePtr edgeptr, bool cleanup = true );


  // * Subdivide Edge *


  // Return pointer to the newly added vertex
  int subdivideEdgeID( OSSIObjectPtr obj, uint edgeId, bool set_type = false );
  OSSIVertexPtr subdivideEdge( OSSIObjectPtr obj, OSSIEdgePtr edgeptr, bool set_type = false );
  OSSIVertexPtr subdivideEdge( OSSIObjectPtr obj, uint edge_index );

  // Subdivide into multiple edges
  vector<int> subdivideEdgeID( OSSIObjectPtr obj, int num_divs, uint edgeId, bool set_type = false );
  OSSIVertexPtrArray subdivideEdge( OSSIObjectPtr obj, int num_divs, OSSIEdgePtr edgeptr, bool set_type = false );
  void subdivideEdge( OSSIObjectPtr obj, int num_divs, uint edge_index );

  // Trisect an edge - new points will be calculated based on scale factor
  // with respect to mid point of edge OR offset from end points
  // Boolean flag indicates if specified number is a scale factor or offset distance
  // Offset distance is clamped to lie between 0.01 and edgelength/2 - 0.01
  // Scale factor is clamped to lie between 0.01 and 0.99 (inclusive)
  void trisectEdge( OSSIObjectPtr obj, OSSIEdgePtr edgeptr, double num, bool scale, bool set_type );

  // Subdivide all edges in the object
  void subdivideAllEdges( OSSIObjectPtr obj, int num_divs = 2, bool set_type = false );

  // Subdivide all edges in the object which are not self loops
  void subdivideNonLoopingEdges( OSSIObjectPtr obj, int num_divs = 2, bool set_type = false );

  // Split valence (degree of vertex, no of edges co-incident on it) vertices into 2 vertices
  void splitValence2Vertices( OSSIObjectPtr obj, double offset = -0.1 );

  // Splice two corners (connect)
  // Insert edge and then collapse edge
  void spliceCorners( OSSIObjectPtr obj, OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2 );

  /*
   * If both sides of an edge are co-planar, the edge will be removed.
   * First version looks at all edges in the object.
   * Second and third version looks at the specified list/array of edges
   */
  void edgeCleanup( OSSIObjectPtr obj );
  void edgeCleanup( OSSIObjectPtr obj, const OSSIEdgePtrList& edges );
  void edgeCleanup( OSSIObjectPtr obj, const OSSIEdgePtrArray& edges );

  void cleanup2gons( OSSIObjectPtr obj ); //cleanup digon, or ploygon with 2 sides and 2 vertices. A digon is considered a degenerate face of a polyhedron because it has no geometric area and edges are overlapping
  /*
   * Cleanup Valence 2 vertices (winged vertices) [each edge points to two vertices, two faces, and the fouredges that touch it.]
   * Removes all winged vertices
   */
  void cleanupWingedVertices( OSSIObjectPtr obj );


    // Read In Object Data

  /* Create an OSSI object from an input stream which should contain
   * an object in OBJ format */
  OSSIObject* readObjectFile( char* filename, char *mtlfilename =NULL); //mtl material filename for the .obj file
  bool writeObjectFile( OSSIObject *obj, char* filename = NULL, char *mtlfilename=NULL );


     // Create/Remove Vertex

    uint* createVertex( double x, double y, double z, OSSIObjectPtr &obj, bool set_type = false );
    void removeVertex( const OSSIObjectPtr &obj, uint vertId, uint faceId );

} // end namespace OSSI

#endif // _OSSICORE_H_
