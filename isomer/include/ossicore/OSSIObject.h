/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSIObject.h
 */

#ifndef _OSSI_OBJECT_H_
#define _OSSI_OBJECT_H_

/*
* TRANSLATOR OSSI::OSSIObject
*
* Necessary for lupdate.
*/

/*
*  OSSIObject class - The Osama's Structure for Storage in Isomer object classs
*
* The Osama's Structure for Storage in Isomer object classs
* Contains a vertex list, edge list and a face list
* The lists actually store pointers
*
*/

#include "OSSICommon.h"
#include "OSSIVertex.h"
#include "OSSIFaceVertex.h"
#include "OSSIEdge.h"
#include "OSSIFace.h"
#include "OSSIMaterial.h"
#include <Graphics/Transform.h>



namespace OSSI {

class OSSIObject {
public :

  // Constructor
  OSSIObject()
    : position(), scale_factor(1), rotation(),
      vertex_list(), edge_list(), face_list()/*, patch_list(), patchsize(4)*/ {
    assignID();
    // Add a default material
    matl_list.push_back(new OSSIMaterial("default",0.5,0.5,0.5));
    mFilename = NULL;
        mDirname = NULL;
  }

  // Destructor
  ~OSSIObject( ) {
    clearLists();
    if( mFilename ) { delete [] mFilename; mFilename = NULL; }
    if( mDirname ) { delete [] mDirname; mDirname = NULL; }
  }

protected :

  // Distinct ID for each instance
  static uint suLastID;

  // Generate a new unique ID
  static uint newID( ) {
    uint temp = suLastID;
    suLastID++;
    return temp;
  }

public :

    static Transformation tr;                         // For doing GL transformations

  OSSIVertexPtrArray sel_vptr_array; // List of selected OSSIVertex pointers

  OSSIEdgePtrArray sel_eptr_array; // List of selected OSSIEdge pointers
  OSSIFacePtrArray sel_fptr_array; // List of selected OSSIFace pointers
  OSSIFaceVertexPtrArray sel_fvptr_array; // List of selected OSSIFaceVertex pointers

  void clearSelected( ) {
    sel_vptr_array.clear();
    sel_eptr_array.clear();
    sel_fptr_array.clear();
    sel_fvptr_array.clear();
  }

    HashMap faceMap;
    HashMap edgeMap;

  static OSSIVertexPtrArray vparray;                // For selection
  static OSSIEdgePtrArray   eparray;                // For selection
  static OSSIFacePtrArray   fparray;                // For selection
  static OSSIFaceVertexPtrArray fvparray;           // For selection

  Vector3d           position;                      // Position of object
  Vector3d           scale_factor;                  // Scale of object
  Quaternion         rotation;                      // Rotation of object

  inline void removeVertex( OSSIVertexPtr vp ) { vertex_list.remove(vp); }
  inline void removeEdge( OSSIEdgePtr ep ) { edgeMap.erase(ep->getID()); edge_list.remove(ep); }
  inline void removeFace( OSSIFacePtr fp ) { faceMap.erase(fp->getID()); face_list.remove(fp); }

  void computeNormals( );

protected :

  OSSIVertexPtrList          vertex_list;           // The vertex list
  OSSIEdgePtrList            edge_list;             // The edge list
  OSSIFacePtrList            face_list;             // The face list
  OSSIMaterialPtrList        matl_list;             // Material list (for rendering)



  uint uID;                                      // ID for this object
  char *mFilename;
  char *mDirname;
  // Assign a unique ID for this instance
  void assignID( ) { uID = OSSIObject::newID(); }



  // Free all the pointers in the lists and clear the lists
  void clearLists( ) {
    clear(vertex_list);
    clear(edge_list);
    clear(face_list);
    clear(matl_list);
    //destroyPatches();
        edgeMap.clear();
        faceMap.clear();
  }

private :

  /// Copy Constructor - make proper copy, don't just copy pointers
  OSSIObject( const OSSIObject& ossi )
    : position(ossi.position), scale_factor(ossi.scale_factor), rotation(ossi.rotation),
      vertex_list(ossi.vertex_list), edge_list(ossi.edge_list), face_list(ossi.face_list), matl_list(ossi.matl_list),
      //patch_list(ossi.patch_list), patchsize(ossi.patchsize),
      uID(ossi.uID) { }

  // Assignment operator
  OSSIObject& operator=( const OSSIObject& ossi ) {
    position = ossi.position;
    scale_factor = ossi.scale_factor;
    rotation = ossi.rotation;

    // Destroy the existing object
    clearLists();

    // Copy the lists from the new object
    vertex_list = ossi.vertex_list;
    edge_list = ossi.edge_list;
    face_list = ossi.face_list;
    matl_list = ossi.matl_list;
    //patch_list = ossi.patch_list;
    //patchsize = ossi.patchsize;

        edgeMap = ossi.edgeMap;
        faceMap = ossi.faceMap;

    uID = ossi.uID;
    return (*this);
  }

public :

  // Dump contents of this object
  void dump(ostream& o) const;

  //--- Query Functions ---//

  // Compute the genus of the mesh using Euler formula
  int genus( ) const {
    int v = num_vertices();
    int e = num_edges();
    int f = num_faces();
    int g = 1 - ( v - e +  f )/2;
    return g;
  }

  inline uint getID( ) const { return uID; }
  inline size_t num_vertices( ) const { return vertex_list.size(); }
  inline size_t num_edges( ) const { return edge_list.size(); }
  inline size_t num_faces( ) const { return face_list.size(); }
  inline size_t num_materials( ) const { return matl_list.size(); }

  // Probably should switch to just using these in the future
  // then you could get rid of all these accessors below.
  const OSSIVertexPtrList& getVertexList( ) const { return vertex_list; }
  const OSSIEdgePtrList& getEdgeList( ) const { return edge_list; }
  //const OSSIFacePtrList& getFaceList( ) const { return face_list; };
  OSSIFacePtrList& getFaceList( ) { return face_list; } // needed not const for subdivideAllFaces

  //-- List based access to the 3 lists --//
  OSSIVertexPtr firstVertex( ) { return vertex_list.front(); }
  OSSIEdgePtr firstEdge( ) { return edge_list.front(); }
  OSSIFacePtr firstFace( ) { return face_list.front(); }
  OSSIMaterialPtr firstMaterial( ) { return matl_list.front(); }

  OSSIVertexPtr lastVertex( ) { return vertex_list.back(); }
  OSSIEdgePtr lastEdge( ) { return edge_list.back(); }
  OSSIFacePtr lastFace( ) { return face_list.back(); }
  OSSIMaterialPtr lastMaterial( ) { return matl_list.back(); }

  OSSIVertexPtr findVertex(const uint vid);
  OSSIEdgePtr findEdge(const uint eid);
  OSSIFacePtr findFace(const uint fid);
    OSSIFaceVertexPtr findFaceVertex(const uint fvid );

  OSSIVertexPtrList::iterator beginVertex( ) { return vertex_list.begin(); }
  OSSIVertexPtrList::iterator endVertex( ) { return vertex_list.end(); }

  OSSIEdgePtrList::iterator beginEdge( ) { return edge_list.begin(); }
  OSSIEdgePtrList::iterator endEdge( ) { return edge_list.end(); }

  OSSIFacePtrList::iterator beginFace( ) { return face_list.begin(); }
  OSSIFacePtrList::iterator endFace( ) { return face_list.end(); }

  OSSIMaterialPtrList::iterator beginMaterial( ) { return matl_list.begin(); }
  OSSIMaterialPtrList::iterator endMaterial( ) { return matl_list.end(); }

  OSSIFacePtrList::reverse_iterator rbeginFace( ) { return face_list.rbegin(); }
  OSSIFacePtrList::reverse_iterator rendFace( ) { return face_list.rend(); }

  //--- Access the lists through arrays ---//
  void getVertices(OSSIVertexPtrArray& vparray) {
    vparray.clear(); vparray.reserve(vertex_list.size());
    OSSIVertexPtrList::const_iterator first = vertex_list.begin(), last = vertex_list.end();
    while ( first != last ) {
      vparray.push_back(*first);
      ++first;
    }
  };

  void getEdges(OSSIEdgePtrArray& eparray) {
    eparray.clear(); eparray.reserve(edge_list.size());
    OSSIEdgePtrList::const_iterator first = edge_list.begin(), last = edge_list.end();
    while ( first != last ) {
      eparray.push_back(*first);
      ++first;
    }
  }

  void getFaces(OSSIFacePtrArray& fparray) {
    fparray.clear(); fparray.reserve(face_list.size());
    OSSIFacePtrList::const_iterator first = face_list.begin(), last = face_list.end();
    while ( first != last ) {
      fparray.push_back(*first);
      ++first;
    }
  }

  //--- Terminal printing functions : useful for debugging ---//
  void print( ) const {
    // Print a summary of the OSSIObject
    cout << "Number of vertices : " << vertex_list.size() << endl;
    cout << "Number of faces : " << face_list.size() << endl;
    cout << "Number of edges : " << edge_list.size() << endl;
    cout << "Number of materials : " << matl_list.size() << endl;
    cout << "Genus : " << genus() << endl;
  }

  void printVertexList( ) const {
    cout << "Vertex List" << endl;
    OSSIVertexPtrList::const_iterator first = vertex_list.begin(), last = vertex_list.end();
    while ( first != last ) {
      (*first)->print();
      ++first;
    }
  }

  void printEdgeList( ) const {
    cout << "Edge List" << endl;
    OSSIEdgePtrList::const_iterator first = edge_list.begin(), last = edge_list.end();
    while ( first != last ) {
      (*first)->print();
      ++first;
    }
  }

  void printFaceList( ) const {
    cout << "Face List" << endl;

    OSSIFacePtrList::const_iterator first = face_list.begin(), last = face_list.end();
    while ( first != last ) {
      cout << *(*first) << endl;
      ++first;
      }
  }

  void printFaces( ) const {
    cout << "Faces" << endl;

    OSSIFacePtrList::const_iterator first = face_list.begin(), last = face_list.end();
    while ( first != last ) {
      (*first)->print();
      ++first;
    }
  }

  //--- Mutative Functions ---//
  // Reset the whole object
  void reset( ) {
    position.reset(); scale_factor.set(1,1,1); rotation.reset();
    clearLists();
    // Add a default material
    matl_list.push_back(new OSSIMaterial("default",0.5,0.5,0.5));
  }

  void makeVerticesUnique( ) {
    // Make vertices unique
    OSSIVertexPtrList::iterator vfirst=vertex_list.begin(), vlast=vertex_list.end();
    while ( vfirst != vlast ) {
      (*vfirst)->makeUnique();
      ++vfirst;
    }
  }

  void makeEdgesUnique( ) {
    // Make edges unique
    OSSIEdgePtrList::iterator efirst=edge_list.begin(), elast=edge_list.end();
    while ( efirst != elast ) {
            edgeMap.erase((*efirst)->getID());
      (*efirst)->makeUnique();
            edgeMap[(*efirst)->getID()] = (unsigned int)(*efirst);
      ++efirst;
    }
  }

  void makeFacesUnique( ) {
    // Make faces unique
    OSSIFacePtrList::iterator ffirst=face_list.begin(), flast=face_list.end();
    while ( ffirst != flast ) {
            faceMap.erase((*ffirst)->getID());
      (*ffirst)->makeUnique();
            faceMap[(*ffirst)->getID()] = (unsigned int)(*ffirst);
      ++ffirst;
    }
  }

  void makeUnique( ) {
    assignID();
    makeVerticesUnique();
    makeEdgesUnique();
    makeFacesUnique();
  }

  void destroy( ) {
    // Clear the OSSI structures
    clearLists();
  }

  void assignTileTexEdgeFlags(int n);
  void assignTileTexCoords(int n);
  void randomAssignTexCoords( );

  // Combine two OSSIObject instances into 1 object
  // Lists of second object are cleared. Otherwise when that object is destroyed,
  // pointers in this object will become invalid.
  void splice(OSSIObject& object);

  // Reverse the orientation of all faces in the object
  // This also requires reversing all edges in the object
  void reverse( );

  void addVertex(const OSSIVertex& vertex);         // Insert a copy
  void addVertex(OSSIVertexPtr vertexptr);          // Insert a copy
  void addVertexPtr(OSSIVertexPtr vertexptr) {
    // Insert the pointer.
    // **** WARNING!!! **** Pointer will be freed when list is deleted
    vertex_list.push_back(vertexptr);
  }

  void addEdge(const OSSIEdge& edge);               // Insert a copy
  void addEdge(OSSIEdgePtr edgeptr);                // Insert a copy
  void addEdgePtr(OSSIEdgePtr edgeptr) {
    // Insert the pointer.
    // **** WARNING!!! **** Pointer will be freed when list is deleted
    edge_list.push_back(edgeptr);
        edgeMap[edgeptr->getID()] = (unsigned int)edgeptr;
  }

  void addFace(const OSSIFace& face);               // Insert a copy
  void addFace(OSSIFacePtr faceptr);                // Insert a copy
  void addFacePtr(OSSIFacePtr faceptr) {
    // Insert the pointer.
    // **** WARNING!!! **** Pointer will be freed when list is deleted
    if ( faceptr->material() == NULL )
      // If Face doesn't have a material assigned to it, assign the default material
        faceptr->setMaterial(matl_list.front());
    face_list.push_back(faceptr);
        faceMap[faceptr->getID()] = (unsigned int)faceptr;
  }

  OSSIVertexPtr getVertexPtr(uint index) const {
    if ( index >= vertex_list.size() ) return NULL;
    OSSIVertexPtrList::const_iterator i=vertex_list.begin();
    advance(i,index);
    return (*i);
  }

  OSSIVertexPtr getVertexPtrID(uint id) const {
    OSSIVertexPtrList::const_iterator first=vertex_list.begin(), last=vertex_list.end();
    OSSIVertexPtr vptr = NULL;
    while (first != last) {
      if ( (*first)->getID() == id ) {
    vptr = (*first);
    break;
      }
      ++first;
    }
    return vptr;
  }

  void updateEdgeList( ) {
    OSSIEdgePtrList::iterator first=edge_list.begin(), last=edge_list.end();
    while ( first != last ) {
      (*first)->updateFaceVertices();
      ++first;
    }
  }

  // Check if an edge exists between two given vertices
  bool edgeExists(OSSIVertexPtr vptr1, OSSIVertexPtr vptr2);

  // Check if an edge exists between vertices given by two corners
  // Simply calls above function
  inline bool edgeExists(OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2) {
    return edgeExists(fvptr1->vertex,fvptr2->vertex);
  }

  // Check if a given edge exists in the edge list. If it does pointer is set to that edge
  bool edgeExists(const OSSIEdge& e, OSSIEdgePtr& eptr) {
    OSSIEdgePtrList::iterator first=edge_list.begin(), last=edge_list.end();
    while ( first != last ) {
      if ( (*(*first)) == e ) {
    eptr = *first;
    return true;
      }
      ++first;
    }
    eptr = NULL;
    return false;
  }

  void addEdges(OSSIEdge * edges, int num_edges) {
    OSSIEdgePtr eptr;

    for (int i=0; i < num_edges; ++i) {
      if ( edgeExists(edges[i],eptr) == false ) {
    addEdge(edges[i]);
      } else {
    // If Edge already exists, then the second FaceVertexPtr in the Edge must
    // be changed to that from the new Edge with the same ID as the second one
    // in this Edge
    int id2 = (eptr->getFaceVertexPtr2())->getVertexID();
    int eid1 = (edges[i].getFaceVertexPtr1())->getVertexID();

    if (eid1 == id2)
      eptr->setFaceVertexPtr2(edges[i].getFaceVertexPtr1());
    else
      eptr->setFaceVertexPtr2(edges[i].getFaceVertexPtr2());
      }
      eptr = NULL;
    }
  }

  void addEdgesWithoutCheck(OSSIEdge * edges, int num_edges) {
    for (int i=0; i < num_edges; ++i)
      addEdge(edges[i]);
  }

  void updateFaceList( ) {
    OSSIFacePtrList::iterator first=face_list.begin(), last=face_list.end();
    while (first != last) {
      (*first)->updateFacePointers();
      (*first)->addFaceVerticesToVertices();
      ++first;
    }
  }

  OSSIFacePtrArray createFace(const Vector3dArray& verts, OSSIMaterialPtr matl = NULL, bool set_type = false);

  OSSIFaceVertexPtr createPointSphere(const Vector3d& v, OSSIMaterialPtr matl = NULL );
    void removePointSphere( OSSIFaceVertexPtr fvp );

  // Compute the bounding box for this object
  void boundingBox(Vector3d& min, Vector3d& max) const;
  bool boundaryWalkID( uint faceId );
    void walk( uint faceId, vector<int> &verts, vector<int> &edges );

  void boundaryWalk(uint face_index);
  void vertexTrace(uint vertex_index);

  void readObject( istream& i, istream &imtl );
  void readObjectAlt( istream& i );
  void readOSSI( istream& i, istream &imtl , bool clearold = true );
    bool readMTL( istream &i);
    bool writeMTL( ostream& o )  const;

  void writeObject( ostream& o, ostream &omtl , bool with_normals = true, bool with_tex_coords = true ) const;
  void writeOSSI(ostream& o, ostream &omtl, bool reverse_faces = false) const;
  void writeSTL(ostream& o) const;
  void writeLG3d(ostream& o, bool select = false) const ; //for LiveGraphics3D support to embed 3d models into html
  inline void setFilename( const char *filename ) {

         if ( filename ) {
          mFilename = new char[strlen(filename)+1]; strcpy(mFilename,filename);
        } else {
          mFilename = new char[8]; strcpy(mFilename,"default");
        }
  }
  inline char* getFilename( ) { return mFilename; }

  inline void setDirname( const char *dirname ) {
         if ( dirname ) {
          mDirname = new char[strlen(dirname)+1]; strcpy(mDirname,dirname);
        } else {
          mDirname = new char[3]; strcpy(mDirname,"$HOME");
        }
  }

  OSSIMaterialPtr findMaterial( const RGBColor& color ) {
    OSSIMaterialPtr matl = NULL;
    OSSIMaterialPtrList::iterator first, last;
    first = matl_list.begin(); last = matl_list.end();
    while ( first != last ) {
      if ( (*first)->equals(color) ) {
                matl = (*first); break;
      }
      ++first;
    }

    return matl;
  }

  OSSIMaterialPtr findMaterial( const char *mtlname ) {
    OSSIMaterialPtr matl = NULL;
    OSSIMaterialPtrList::iterator first, last;
    first = matl_list.begin(); last = matl_list.end();
    while ( first != last ) {
            if ( mtlname && !strcasecmp((*first)->name,mtlname) ) {
                matl = (*first); break;
      }
      ++first;
    }
    return matl;
  }

    void clearMaterials(){
        //iterate through faces...


        OSSIFacePtrList::iterator ffirst=face_list.begin(), flast=face_list.end();
    while ( ffirst != flast ) {
      (*ffirst)->setMaterial(matl_list.front());
            ++ffirst;
    }
        //clear materials
        while (matl_list.size() > 1){

            matl_list.pop_back();

        }

    }

    OSSIMaterialPtr addMaterial(RGBColor color){
        //first search for the material to see if it exists already or not
        char matl_name[10];
        OSSIMaterialPtr mtl = findMaterial(color);

        // No matching material found
        if ( mtl == NULL ) {
            //add this as a new material
            sprintf(matl_name,"material%d",matl_list.size());
            matl_list.push_back(new OSSIMaterial(matl_name,color));
            mtl = matl_list.back();
            return mtl;
        }
        else {
            //it already exists... return it
            return mtl;
        }
    }

  void setColor( const RGBColor& col ) {
    // matl_list[0] is always the default material
    matl_list.front()->setColor(col);
  }

  //-- Geometric Transformations --//

  // Freeze any stored transformations and reset the transformations
  // Order of application : rotation, scale, translate
  void freezeTransformations(void) {
    tr.reset();
    tr.scale(scale_factor);
    tr.rotate(rotation);
    tr.translate(position);
    Matrix4x4 tmat = tr.matrix();
    OSSIVertexPtrList :: iterator vfirst = vertex_list.begin(), vlast = vertex_list.end();
    OSSIVertexPtr vp;
    while ( vfirst != vlast ) {
      vp = (*vfirst); ++vfirst;
      vp->transform(tmat);
    }
  }

  // Apply GL transformations before rendering
  void transform(void) const {
    tr.reset();
    tr.scale(scale_factor);
    tr.rotate(rotation);
    tr.translate(position);
    tr.apply();
  }

  // Apply the transformations for this object on the given vector
  void transform(Vector3d& p) {
    tr.reset();
    tr.scale(scale_factor);
    tr.rotate(rotation);
    tr.translate(position);
    Vector4d tp(p); tp[3] = 1.0;
    tp = tr.matrix() * tp;
    tp /= tp[3];
    p = tp;
  }

};

} // end namespace
#endif /* _OSSI_OBJECT_H_ */
