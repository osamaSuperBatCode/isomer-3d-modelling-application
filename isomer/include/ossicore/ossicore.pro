TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib #dll # build shared library
CONFIG += debug warn_off create_prl
# CONFIG += debug warn_off create_prl
TARGET = ossicore
INCLUDEPATH += .. ../vecmat
DESTDIR = ../../lib

macx {
 # compile release + universal binary
 #QMAKE_LFLAGS += -F../../lib
 #LIBS += -framework vecmat
 CONFIG += x86 ppc
 #CONFIG += lib_bundle
 #QMAKE_BUNDLE_EXTENSION = .framework
#} else:unix {
 QMAKE_LFLAGS += -L../../lib
 #LIBS += -lvecmat
}
else:win32 {

}

HEADERS += \
        OSSICommon.h \
        OSSICore.h \
        OSSICoreExt.h \
        OSSIEdge.h \
        OSSIFace.h \
        OSSIFaceVertex.h \
        OSSIMaterial.h \
        OSSIObject.h \
        OSSIVertex.h

SOURCES += \
        OSSICommon.cpp \
        OSSICore.cpp \
        OSSICoreExt.cpp \
        OSSIEdge.cpp \
        OSSIFace.cpp \
        OSSIFaceVertex.cpp \
        OSSIFile.cpp \
        OSSIFileAlt.cpp \
        OSSIObject.cpp \
        OSSIVertex.cpp
