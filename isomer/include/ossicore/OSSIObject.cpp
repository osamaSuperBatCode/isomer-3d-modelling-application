/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSIObject.cpp
 */

#include "OSSIObject.h"

namespace OSSI {

  uint OSSIObject::suLastID = 0;
  Transformation OSSIObject::tr;

  void OSSIObject::dump(ostream& o) const {
    o << "OSSIObject" << endl;

    int i=0;

    o << "VertexList" << endl;

    OSSIVertexPtrList::const_iterator vfirst=vertex_list.begin(), vlast=vertex_list.end();

    while ( vfirst != vlast ) {
      o << i << " : " << (*vfirst) << endl;
      (*vfirst)->dump(o);
      ++vfirst; ++i;
    }

    i = 0;
    o << "EdgeList" << endl;
    OSSIEdgePtrList::const_iterator efirst=edge_list.begin(), elast=edge_list.end();
    while ( efirst != elast ) {
      o << i << " : " << (*efirst) << endl;
      (*efirst)->dump(o);
      ++efirst; ++i;
    }

    i = 0;
    o << "FaceList" << endl;
    OSSIFacePtrList::const_iterator ffirst=face_list.begin(), flast=face_list.end();
    while ( ffirst != flast ) {
      o << i << " : " << (*ffirst) << endl;
      (*ffirst)->dump(o);
      ++ffirst; ++i;
    }
  }

  void OSSIObject::splice(OSSIObject& object) {
    // Combine 2 objects. The lists are simply spliced together.
    // Entities must be removed from the second object to prevent dangling pointers
    // when it is destroyed.
    vertex_list.splice(vertex_list.end(),object.vertex_list);
    edge_list.splice(edge_list.end(),object.edge_list);
    face_list.splice(face_list.end(),object.face_list);
    matl_list.splice(matl_list.end(),object.matl_list);
  }

  // Reverse the orientation of all faces in the object
  // This also requires reversing all edges in the object
  void OSSIObject::reverse(void)
  {
    // Reverse the edges first, since they depend on the ordering of the
    // original faces.
    OSSIEdgePtrList::iterator efirst=edge_list.begin(), elast=edge_list.end();
    while ( efirst != elast ) {
      (*efirst)->reverse();
      ++efirst;
    }

    OSSIFacePtrList::iterator ffirst=face_list.begin(), flast=face_list.end();
    while ( ffirst != flast ) {
      (*ffirst)->reverse();
      ++ffirst;
    }
  }

  bool OSSIObject::edgeExists(OSSIVertexPtr vptr1, OSSIVertexPtr vptr2) {
    // Check if an edge exists between two given vertices
    bool edgeexists = false;

    // Try to find an edge from vptr1 to vptr2
    if ( vptr1->getEdgeTo(vptr2) != NULL ) edgeexists = true;

    return edgeexists;
  }

  /*
   * Tile Texturing (choppy implementation, might have to scrap texturing alltogether
   */

  void OSSIObject::assignTileTexEdgeFlags(int n) {
    // Assign flags to edge corners based on tiling factor
    // Flags are stored in the first component of the texture coordinate in each face vertex
    int randomvariable1, randomvariable2;
    OSSIEdgePtrList :: const_iterator first = edge_list.begin(), last = edge_list.end();
    while ( first != last ) {
      OSSIFaceVertexPtr fvp1,fvp2;
      (*first)->getFaceVertexPointers(fvp1,fvp2);

      randomvariable1 = rand() % (n/2);
      randomvariable2 = rand() % 2;

      fvp1->texcoord[0] = 2*randomvariable1 + randomvariable2;
      fvp2->texcoord[0] = 2*randomvariable1 + ((randomvariable2+1) % 2);

      ++first;
    }
  }

  void OSSIObject::assignTileTexCoords(int n) {
    // Compute the texture coordinates for each corner in each face

    // Assign edge flags first
    assignTileTexEdgeFlags(n);

    // Compute texture coordinates using the edge flags
    OSSIFaceVertexPtrArray corners;
    int flag[10];
    double u,v;

    OSSIFacePtrList :: const_iterator first = face_list.begin(), last = face_list.end();
    while ( first != last ) {
      (*first)->getCorners(corners);

      for (int i=0; i < corners.size(); ++i) {
                flag[i] = int(corners[i]->texcoord[0]);
      }
      for (int i=0; i < corners.size(); ++i) {
                u = (float)( n*flag[0] + flag[1]  + (int)( ((i+1)%4) / 2 ) ) / (n*n);

                // Subtract from 1.0 since image origin is at top-left, instead of bottom-left
                v = 1.0 - (float)( n*flag[2] + flag[3]  + (int)(i/2) ) / (n*n);

                corners[i]->texcoord.set(u,v);
      }
      ++first;
    }
  }

  void OSSIObject::randomAssignTexCoords( ) {
    // FOR QUADS ONLY - randomly assign texture coordinates from a unit square to the 4 corners
    OSSIFacePtrList :: iterator first, last;
    first = face_list.begin(); last = face_list.end();
    while ( first != last ) {
      (*first)->randomAssignTexCoords();
      ++first;
    }
  }

  OSSIFacePtrArray OSSIObject::createFace(const Vector3dArray& verts, OSSIMaterialPtr matl, bool set_type) {
    // Create 2 new faces with the given vertex coordinates. The 2 faces will have the same
    // vertices and share the same edges, but will have opposite rotation orders.
    // This essentially creates a 2 manifold with 2 faces with no volume.
    int numverts = verts.size();
    OSSIFacePtr newface1, newface2;
    OSSIVertexPtr vptr, tempvptr;
    OSSIFaceVertex fv;
    OSSIFaceVertexPtr fvptr;

    if ( matl == NULL ) matl = firstMaterial();

    newface1 = new OSSIFace; newface2 = new OSSIFace;

    if ( set_type ) {
      newface1->setType(FTNew); newface2->setType(FTNew);
    }

    for (int i=0; i < numverts; ++i) {
      vptr = new OSSIVertex(verts[i]); if ( i == 0 ) tempvptr = vptr;
      addVertexPtr(vptr);
      fv.setVertexPtr(vptr);
      newface1->addVertex(fv); newface2->addVertex(fv);
    }

    // Reverse one of the new faces
    newface2->reverse();

    // These updates have to be done before the reorder
    newface1->addFaceVerticesToVertices(); newface2->addFaceVerticesToVertices();

    // Reorder the second new face so that the first vertex is the first vertex in the array
    OSSIFaceVertexPtrList fvplist = tempvptr->getFaceVertexList();
    fvptr = fvplist.front();
    if ( fvptr->getFacePtr() != newface2 ) fvptr = fvplist.back();
    newface2->reorder(fvptr);

    // Add the new faces to the list
    addFacePtr(newface1); addFacePtr(newface2);
    newface1->setMaterial(matl); newface2->setMaterial(matl);

    newface1->computeNormal(); newface2->computeNormal();

    // Create the new edges
    // Traverse the 2 faces in opposite orders
    OSSIFaceVertexPtr head1, current1;
    OSSIFaceVertexPtr head2, current2;
    OSSIEdgePtr eptr;

    head1 = newface1->front(); head2 = newface2->back();

    // Assume that head1 and head2 are not NULL
    current1 = head1; current2 = head2;
    eptr = new OSSIEdge(current1,current2);
    eptr->updateFaceVertices();
    addEdgePtr(eptr);
    current1 = current1->next(); current2 = current2->prev();
    while ( current1 != head1 && current2 != head2 ) {
      eptr = new OSSIEdge(current1,current2,false);
      eptr->updateFaceVertices();
      addEdgePtr(eptr);
      current1 = current1->next(); current2 = current2->prev();
    }

        OSSIFacePtrArray newverts;
        newverts.push_back( newface1 );
        newverts.push_back( newface2 );

        return newverts;
  }

  OSSIFaceVertexPtr OSSIObject::createPointSphere(const Vector3d& v, OSSIMaterialPtr matl) {
    // Create a point sphere - a face with only 1 vertex
    OSSIFacePtr newface = new OSSIFace();

    if ( matl == NULL ) matl = firstMaterial();

    OSSIVertexPtr vp = new OSSIVertex(v);
    OSSIFaceVertexPtr fvp = new OSSIFaceVertex(vp,NULL);
    fvp->addSelfToVertex();
    newface->addVertexPtr(fvp);
    newface->setMaterial(matl);

    addVertexPtr(vp);
    addFacePtr(newface);

    return fvp;
  }

    void OSSIObject::removePointSphere( OSSIFaceVertexPtr fvp ) {
        if( fvp->getEdgePtr() == NULL ) {
            OSSIVertexPtr vp = fvp->getVertexPtr( );
            OSSIFacePtr fp = fvp->getFacePtr( );

            if( vp->numEdges() > 0 )
                return;

            removeVertex( vp );
            delete vp;
            removeFace( fp );
            delete fp;

            delete fvp;
        }
    }

  void OSSIObject::boundingBox(Vector3d& min, Vector3d& max) const {
    OSSIVertexPtrList::const_iterator vf,vl;
    double minx,miny,minz;
    double maxx,maxy,maxz;
    double x,y,z;
    OSSIVertexPtr vptr;

    vf = vertex_list.begin(); vl = vertex_list.end();
    vptr = (*vf); ++vf;

    vptr->coords.get(minx,miny,minz);
    maxx = minx; maxy = miny; maxz = minz;

    while ( vf != vl ) {
      vptr = (*vf); ++vf;
      vptr->coords.get(x,y,z);

      (x < minx) ? minx = x : ( (x > maxx) ? maxx = x : 1);
      (y < miny) ? miny = y : ( (y > maxy) ? maxy = y : 1);
      (z < minz) ? minz = z : ( (z > maxz) ? maxz = z : 1);
    }
    min.set(minx,miny,minz); max.set(maxx,maxy,maxz);
  }

  bool OSSIObject::boundaryWalkID( uint faceId ) {
    OSSIFacePtr fp = findFace( faceId );
    if( fp ) {
      fp->boundaryWalk();
      return true;
    } else { return false; }
  }

  void OSSIObject::walk( uint faceId,
                                                 vector<int> &verts,
                                                 vector<int> &edges ) {
    OSSIFacePtr fp = findFace( faceId );

    if( fp ) {
      verts = fp->vertexWalk();
      edges = fp->edgeWalk();
    }

  }

  void OSSIObject::boundaryWalk(uint face_index) {
    //Find the Face with the given face_index from the FaceList and do a boundary walk on it
    uint i = 0;
    OSSIFacePtrList::iterator first = face_list.begin();
    OSSIFacePtrList::iterator last = face_list.end();
    OSSIFacePtr faceptr = NULL;

    if (face_index > face_list.size())
      return;
    while (first != last) {
      if (i == face_index) {
                faceptr = (*first);
                break;
      }
      ++first;

      ++i;
    }
    faceptr->boundaryWalk();
  }

  void OSSIObject::vertexTrace(uint vertex_index) {
    //Find the Vertex with the given index in the VertexList and do a vertex trace on it
    uint i = 0;
    OSSIVertexPtrList::iterator first = vertex_list.begin();
    OSSIVertexPtrList::iterator last = vertex_list.end();
    OSSIVertexPtr vertexptr = NULL;

    if (vertex_index > vertex_list.size())
      return;
    while (first != last) {
      if (i == vertex_index) {
                vertexptr = (*first);
                break;
      }
      ++first;

      ++i;
    }
    vertexptr->vertexTrace();
  }

  OSSIVertexPtr OSSIObject::findVertex(const uint vid) {
    // Find a vertex with the given vertex id. Return NULL if none exists
    OSSIVertexPtrList::iterator first = vertex_list.begin(), last = vertex_list.end();
    OSSIVertexPtr sel = NULL;
    while ( first != last )
      {
                if ( (*first)->getID() == vid )
          {
            sel = (*first); break;
          }
                ++first;
      }
    return sel;
  }

  OSSIEdgePtr OSSIObject::findEdge(const uint eid) {
    // Find an edge with the given edge id. Return NULL if none exists

    OSSIEdgePtr sel = NULL;

        sel = (OSSIEdgePtr) edgeMap[eid];
    return sel;
  }

  OSSIFacePtr OSSIObject::findFace(const uint fid) {
    // Find a face with the given face id. Return NULL if none exists
    //OSSIFacePtrList::iterator first = face_list.begin(), last = face_list.end();
    OSSIFacePtr sel = NULL;

        sel = (OSSIFacePtr) faceMap[fid];
    return sel;
  }

  OSSIFaceVertexPtr OSSIObject::findFaceVertex(const uint fvid) {
    // Find a face vertex with the given face vertex id. Return NULL if none exists
    OSSIFacePtrList::iterator first = face_list.begin(), last = face_list.end();
    OSSIFaceVertexPtr sel,tmp = NULL;
    while ( first != last ) {
            tmp = (*first)->findFaceVertexByID(fvid);
            if( tmp != NULL ) {
                sel = tmp;

            }
            ++first;
    }
    return sel;
  }

  void OSSIObject::addVertex(const OSSIVertex& vertex)
  {
    addVertexPtr(vertex.copy());
  }

  void OSSIObject::addVertex(OSSIVertexPtr vertexptr)
  {
    addVertexPtr(vertexptr->copy());
  }

  void OSSIObject::addEdge(const OSSIEdge& edge)
  {
    addEdgePtr(edge.copy());
  }

  void OSSIObject::addEdge(OSSIEdgePtr edgeptr)
  {
    addEdgePtr(edgeptr->copy());
  }

  void OSSIObject::addFace(const OSSIFace& face)
  {
    addFacePtr(face.copy());
  }

  void OSSIObject::addFace(OSSIFacePtr faceptr)
  {
    addFacePtr(faceptr->copy());
  }

  void OSSIObject::computeNormals( ) {
    OSSIVertexPtrList::iterator first, last;

    first = vertex_list.begin(); last = vertex_list.end();
    while ( first != last ) {
      (*first)->updateNormal();
      ++first;
    }
    OSSIFacePtrList::iterator ffirst, flast;

    ffirst = face_list.begin(); flast = face_list.end();
    while ( ffirst != flast ) {
      (*ffirst)->updateNormal();
      ++ffirst;
    }
  }


} // end namespace

