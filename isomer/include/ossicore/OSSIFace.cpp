/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSIFace.cpp
 */



#include "OSSIFace.h"
#include "OSSIEdge.h"
#include "OSSIFaceVertex.h"
#include "OSSIVertex.h"


namespace OSSI {

  // Texture coordinates for the 4 corners of a unit square
  static Vector2d unittexcoords[] = { Vector2d(0,0), Vector2d(1,0), Vector2d(1,1), Vector2d(0,1) };

  // Define the static variable. Initialized to 0
  uint OSSIFace::suLastID = 0;


  // Dump contents of object
  void OSSIFace::dump(ostream& o) const {
    o << "OSSIFace" << endl
      //    << "  ID : " << uID << endl
      //    << "  MatlPtr : " << matl_ptr << endl
      //    << "  Type : " << ftType << endl
      << "  OSSIFaceVertexPtr list" << endl;

    if ( head )
      {
                OSSIFaceVertexPtr current = head;
                int i=0;

                o << "    " << i << " : " << current << endl; current->dump(o);
                ++i; current = current->next();
                while ( current != head )
          {
            o << "    " << i << " : " << current << endl; current->dump(o);
            ++i; current = current->next();
          }
      }
    o << endl;
  }

  // Constructor
  OSSIFace::OSSIFace( OSSIMaterialPtr mp )
    : head(NULL), matl_ptr(mp), ftType(FTNormal), auxcoords(), auxnormal(), centroid(), normal(), flags(0)
  {
    assignID();
    // Add this face to the face-list of any associated material
    if ( matl_ptr ) matl_ptr->addFace(this);
  }

  // Copy constructor
  OSSIFace::OSSIFace(const OSSIFace& face)
    : uID(face.uID), head(NULL), matl_ptr(face.matl_ptr), ftType(face.ftType),
      auxcoords(face.auxcoords), auxnormal(face.auxnormal), centroid(face.centroid), normal(face.normal), flags(face.flags)
  {
    copy(face.head);
    // Add this face to the face-list of any associated material
    if ( matl_ptr ) matl_ptr->addFace(this);
  }

  // Destructor
  OSSIFace::~OSSIFace() {
    // Remove this face from the face-list of the material if there is an associated material
    if ( matl_ptr ) matl_ptr->deleteFace(this);
    destroy();
  }

  // Copy face from another face. Doesn't destroy existing face. So it's more of an insert
  void OSSIFace::copy(OSSIFaceVertexPtr ptr) {
    if ( ptr ) {
      OSSIFaceVertexPtr current = ptr;
      addVertexPtr(current->copy());
      current = current->next();
      while ( current != ptr ) {
                addVertexPtr(current->copy());
                current = current->next();
      }
    }
  }

  // Destroy this face
  void OSSIFace::destroy(void)
  {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      OSSIFaceVertexPtr temp;
      temp = current;
      current = current->next();
      delete temp;
      while ( current != head ) {
                temp = current;
                current = current->next();
                delete temp;
      }
    }
    head = NULL;
  }

  // Assignment operator
  OSSIFace& OSSIFace::operator = (const OSSIFace& face) {
    destroy();
    copy(face.head);
    uID = face.uID;
    ftType = face.ftType;
    auxcoords = face.auxcoords;
    auxnormal = face.auxnormal;
    centroid = face.centroid;
    normal = face.normal;
    flags = face.flags;
    if ( matl_ptr != face.matl_ptr ) {
      // Remove this face from the face-list of the material
      // if there is an associated material
      if ( matl_ptr ) matl_ptr->deleteFace(this);

      matl_ptr = face.matl_ptr;

      // Add this face to the face-list of the material
      if ( matl_ptr ) matl_ptr->addFace(this);
    }
    return (*this);
  }

  OSSIFaceVertexPtr OSSIFace::addVertex(const OSSIFaceVertex& ofv) {
    // Insert a copy.
    OSSIFaceVertexPtr newfvp = ofv.copy();
    addVertexPtr(newfvp);
    return newfvp;
  }

  OSSIFaceVertexPtr OSSIFace::addVertex(OSSIFaceVertexPtr ofvp) {
    // Insert a copy.
    OSSIFaceVertexPtr newfvp = ofvp->copy();
    addVertexPtr(newfvp);
    return newfvp;
  }

  void OSSIFace::addVertexPtr(OSSIFaceVertexPtr ofvp) {
    // Just insert the pointer.
    // **** WARNING!!! **** The pointer will be freed when the list is deleted!
    // Set face pointer for the face-vertex
    ofvp->setFacePtr(this);
    // Adjust the previous and next fields for this and adjacent face-vertices
    if ( head ) {
      OSSIFaceVertexPtr last = head->prev();
      ofvp->next() = head; head->prev() = ofvp;
      ofvp->prev() = last; last->next() = ofvp;
    } else {
      // face is empty
      // Make the new face-vertex the head
      head = ofvp;
    }
  }

  void OSSIFace::deleteVertexPtr(OSSIFaceVertexPtr ofvp) {
    if ( ofvp->getFacePtr() != this ) return;

    // Adjust pointers of adjacent face-vertices
    OSSIFaceVertexPtr n = ofvp->next();
    OSSIFaceVertexPtr p = ofvp->prev();

    if ( n == p && n == ofvp ) {  // Lone vertex in this face
      // NOTE: If n == p, it does not mean that this is the only vertex
      // n == p will be true even when there are 2 vertices only in the face
      head = NULL;
      return;
    }

    n->prev() = p; p->next() = n;

    if ( head == ofvp ) head = n;

    // Reset ofvp's pointers to point to itself
    ofvp->next() = ofvp; ofvp->prev() = ofvp; ofvp->setFacePtr(NULL);
  }


  void OSSIFace::getVertexCoords(Vector3dArray& verts) {
    // Get the coordinates of the vertices of this face in a Vector3dArray
    verts.clear(); verts.reserve(size());
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      verts.push_back(current->getVertexCoords());
      current = current->next();
      while ( current != head ) {
                verts.push_back(current->getVertexCoords());
                current = current->next();
      }
    }
  }

  // Set the material for this face. Update face-list of material also
  void OSSIFace::setMaterial(OSSIMaterialPtr mptr) {
    if ( mptr == matl_ptr ) return;

    // Remove face from old material
    if ( matl_ptr ) matl_ptr->deleteFace(this);

    // Change material
    matl_ptr = mptr;

    // Add face to new material
    if ( matl_ptr ) matl_ptr->addFace(this);
  }

  OSSIFaceVertexPtr OSSIFace::advance(OSSIFaceVertexPtr ptr, uint num) const {
    OSSIFaceVertexPtr newptr = ptr;
    for (uint i=0; i < num; ++i)
      newptr = newptr->next();
    return newptr;
  }

  // Access the face-vertex specified by index. No range checks done
  OSSIFaceVertexPtr OSSIFace::facevertexptr(uint index) {
    return advance(head,index);
  }

  OSSIFaceVertex& OSSIFace::facevertex(uint index) {
    return *advance(head,index);
  }

  OSSIFaceVertex OSSIFace::facevertex(uint index) const {
    return *advance(head,index);
  }

  // Access the vertex specified by index. No range checks done
  OSSIVertexPtr OSSIFace::vertexptr(uint index) {
    return facevertexptr(index)->vertex;
  }

  OSSIVertex& OSSIFace::vertex(uint index) {
    return *(facevertexptr(index)->vertex);
  }

  OSSIVertex OSSIFace::vertex(uint index) const {
    return *(facevertex(index).vertex);
  }

  // Access the coordinates of the vertex specified by index. No range checks done
  Vector3d& OSSIFace::vertex_coords(uint index) {
    return vertexptr(index)->coords;
  }

  Vector3d OSSIFace::vertex_coords(uint index) const {
    return vertex(index).coords;
  }

  // Access color and normal of face vertex specified by index
  RGBColor& OSSIFace::fv_color(uint index) {
    return facevertexptr(index)->color;
  }

  RGBColor OSSIFace::fv_color(uint index) const {
    return facevertex(index).color;
  }

  Vector3d& OSSIFace::fv_normal(uint index) {
    return facevertexptr(index)->normal;
  }

  Vector3d OSSIFace::fv_normal(uint index) const {
    return facevertex(index).normal;
  }

  // Set the color of all face vertices of this face
  void OSSIFace::setColor(const RGBColor& col) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->color = col;
      current = current->next();
      while ( current != head ) {
                current->color = col;
                current = current->next();
      }
    }
  }

  void OSSIFace::setColor(double r, double g, double b) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->color.color.set(r,g,b);
      current = current->next();
      while ( current != head ) {
                current->color.color.set(r,g,b);
                current = current->next();
      }
    }
  }

  void OSSIFace::randomAssignTexCoords(void) {
    // Assumes that the face is a quad

    int start = rand() % 4; // Random no 0,1,2 or 3

    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->texcoord = unittexcoords[start];
      ++start; start %= 4;
      current = current->next();
      while ( current != head ) {
                current->texcoord = unittexcoords[start];
                ++start; start %= 4;
                current = current->next();
      }
    }
  }

  void OSSIFace::updateMaterial(void) {
    if ( matl_ptr ) matl_ptr->addFace(this);
  }

  Vector3d OSSIFace::geomSum(void) const {
    Vector3d sumg;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      sumg = current->vertex->coords;
      current = current->next();
      while ( current != head ) {
                sumg += current->vertex->coords;
                current = current->next();
      }
    }
    return sumg;
  }

  Vector2d OSSIFace::textureSum(void) const {
    Vector2d sumtc;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      sumtc = current->texcoord;
      current = current->next();
      while ( current != head ) {
                sumtc += current->texcoord;
                current = current->next();
      }
    }
    return sumtc;
  }

  RGBColor OSSIFace::colorSum(void) const {
    RGBColor sumc;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      sumc = current->color;
      current = current->next();
      while ( current != head ) {
                sumc += current->color;
                current = current->next();
      }
    }
    return sumc;
  }

  Vector3d OSSIFace::normalSum(void) const {
    Vector3d sumn;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      sumn = current->normal;
      current = current->next();
      while ( current != head ) {
                sumn += current->normal;
                current = current->next();
      }
    }
    return sumn;
  }

  void OSSIFace::getSums(Vector3d& sumg, Vector2d& sumtc, RGBColor& sumc, Vector3d& sumn) const
  {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      sumg = current->vertex->coords;
      sumtc = current->texcoord;
      sumc = current->color;
      sumn = current->normal;
      current = current->next();
      while ( current != head ) {
                sumg += current->vertex->coords;
                sumtc += current->texcoord;
                sumc += current->color;
                sumn += current->normal;
                current = current->next();
      }
    }
  }

  void OSSIFace::updateCentroid(void) {
    centroid.reset();
    if ( head ) {
      int num = 0;
      OSSIFaceVertexPtr current = head;
      centroid = current->vertex->coords; ++num;
      current = current->next();
      while ( current != head ) {
                centroid += current->vertex->coords; ++num;
                current = current->next();
      }
      centroid /= num;
    }
  }

  Vector2d OSSIFace::textureCentroid(void) const {
    Vector2d centc;
    if ( head ) {
      int num = 0;
      OSSIFaceVertexPtr current = head;
      centc = current->texcoord; ++num;
      current = current->next();
      while ( current != head ) {
                centc += current->texcoord; ++num;
                current = current->next();
      }
      centc /= num;
    }
    return centc;
  }

  RGBColor OSSIFace::colorCentroid(void) const
  {
    RGBColor cenc;
    if ( head ) {
      int num = 0;
      OSSIFaceVertexPtr current = head;
      cenc = current->color; ++num;
      current = current->next();
      while ( current != head ) {
                cenc += current->color; ++num;
                current = current->next();
      }
      cenc /= num;
    }
    return cenc;
  }

  void OSSIFace::updateNormal(void) {
    normal.reset();
    if ( head ) {
      int num = 0;
      OSSIFaceVertexPtr current = head;
      do {
                normal += current->computeNormal(); ++num;
                current = current->next();
      } while ( current != head );
      normalize(normal);
    }
  }

  Vector3d OSSIFace::computeNormal(void) {
    normal.reset();
    if ( head ) {
      int num = 0;
      OSSIFaceVertexPtr current = head;
      do {
                normal += current->computeNormal(); ++num;
                current = current->next();
      }
      while ( current != head );
      normal /= num; normalize(normal);

      current = head;
      do {
                current->setNormal(normal);
                current = current->next();
      } while ( current != head );
    }
    return normal;
  }

  void OSSIFace::storeNormals(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->computeNormal();
      current = current->next();
      while ( current != head ) {
                current->computeNormal();
                current = current->next();
      }
    }
  }

  void OSSIFace::getCentroids(Vector3d& ceng, Vector2d& centc, RGBColor& cenc, Vector3d& cenn) const {
    if ( head ) {
      int num = 0;
      OSSIFaceVertexPtr current = head;
      ceng = current->vertex->coords;
      centc = current->texcoord;
      cenc = current->color;
      cenn = current->normal;
      ++num;
      current = current->next();
      while ( current != head ) {
                ceng += current->vertex->coords;
                centc += current->texcoord;
                cenc += current->color;
                cenn += current->normal;
                ++num;
                current = current->next();
      }
      ceng /= num;
      centc /= num;
      cenc /= num;
      cenn /= num;
    }
  }

  uint OSSIFace::size(void) const {
    int num=0;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      ++num; current = current->next();
      while ( current != head ) {
                ++num; current = current->next();
      }
    }
    return num;
  }

  void OSSIFace::resetTypeDeep(void) {
    resetType();
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->resetType();
      current = current->next();
      while ( current != head ) {
                current->resetType();
                current = current->next();
      }
    }
  }

  void OSSIFace::updateFacePointers(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->setFacePtr(this);
      current = current->next();
      while ( current != head ) {
                current->setFacePtr(this);
                current = current->next();
      }
    }
  }

  void OSSIFace::resetFacePointers(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->setFacePtr(NULL);
      current = current->next();
      while ( current != head ) {
                current->setFacePtr(NULL);
                current = current->next();
      }
    }
  }

  void OSSIFace::addFaceVerticesToVertices(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->addSelfToVertex();
      current = current->next();
      while ( current != head ) {
                current->addSelfToVertex();
                current = current->next();
      }
    }
  }

  void OSSIFace::deleteFaceVerticesFromVertices(void)
  {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->deleteSelfFromVertex();
      current = current->next();
      while ( current != head ) {
                current->deleteSelfFromVertex();
                current = current->next();
      }
    }
  }

  void OSSIFace::addFaceVerticesToEdges(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->addSelfToEdge();
      current = current->next();
      while ( current != head ) {
                current->addSelfToEdge();
                current = current->next();
      }
    }
  }

  void OSSIFace::deleteFaceVerticesFromEdges(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->deleteSelfFromEdge();
      current = current->next();
      while ( current != head ) {
                current->deleteSelfFromEdge();
                current = current->next();
      }
    }
  }

  void OSSIFace::addFaceVerticesToVerticesAndEdges(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->addSelfToVertex();
      current->addSelfToEdge();
      current = current->next();
      while ( current != head ) {
                current->addSelfToVertex();
                current->addSelfToEdge();
                current = current->next();
      }
    }
  }

  void OSSIFace::deleteFaceVerticesFromVerticesAndEdges(void) {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->deleteSelfFromVertex();
      current->deleteSelfFromEdge();
      current = current->next();
      while ( current != head ) {
                current->deleteSelfFromVertex();
                current->deleteSelfFromEdge();
                current = current->next();
      }
    }
  }

  int OSSIFace::getEdges(OSSIEdge ** edges) {
    int length = size();
    if ( length < 2 ) return 0;

    // Atleast 2 vertices are there in this OSSIFace (atleast 3 should be there, but
    // that doesn't affect this function)
    *edges = new OSSIEdge[length];

    if ( head ) {
      int index = 0;
      OSSIFaceVertexPtr current = head;

      edges[0][length-1].setFaceVertexPtr2(current);  // Last Edge
      edges[0][index].setFaceVertexPtr1(current);     // First Edge
      current = current->next();
      while ( current != head ) {
                edges[0][index].setFaceVertexPtr2(current);
                index++;
                edges[0][index].setFaceVertexPtr1(current);
                current = current->next();
      }
    }
    return length;
  }

  // This function is different from the previous one in that it doesn't create any new edges
  // It simply gets the existing edges. Puts the edge pointers into an array
  void OSSIFace::getEdges(OSSIEdgePtrArray& edges) {
    edges.clear(); edges.reserve(size());
    if ( head ) {
      OSSIFaceVertexPtr current = head;

      edges.push_back(current->getEdgePtr());
      current = current->next();
      while ( current != head ) {
                edges.push_back(current->getEdgePtr());
                current = current->next();
      }
    }
  }

  // Same as above but puts edge pointers into a linked list
  void OSSIFace::getEdges(OSSIEdgePtrList& edges) {
    edges.clear();
    if ( head ) {
      OSSIFaceVertexPtr current = head;

      edges.push_back(current->getEdgePtr());
      current = current->next();
      while ( current != head ) {
                edges.push_back(current->getEdgePtr());
                current = current->next();
      }
    }
  }

  // Get the face vertex pointers in the face as an array
  void OSSIFace::getCorners(OSSIFaceVertexPtrArray& corners) {
    corners.clear(); corners.reserve(size());

    if ( head )
      {
                OSSIFaceVertexPtr current = head;

                corners.push_back(current);
                current = current->next();
                while ( current != head )
          {
            corners.push_back(current);
            current = current->next();
          }
      }
  }

    OSSIFaceVertexPtr OSSIFace::findFaceVertex( uint vertexId ) const {
        if(head) {
            OSSIFaceVertexPtr current = head;
            do {
                uint id = current->getVertexID();
                if( vertexId == id )
                    return current;
                current = current->next();
            } while( current != head );
        }
        return NULL;
    }

    OSSIFaceVertexPtr OSSIFace::findFaceVertexByID( uint faceVertexId ) const {
        if(head) {
            OSSIFaceVertexPtr current = head;
            do {
                uint id = current->getID();
                if( faceVertexId == id )
                    return current;
                current = current->next();
            } while( current != head );
        }
        return NULL;
    }

  // Get the corners and the coords
  void OSSIFace::getCornersAndCoords(OSSIFaceVertexPtrArray& corners, Vector3dArray& coords) {
    int n = size();
    corners.clear(); corners.reserve(n);
    coords.clear(); coords.reserve(n);

    if ( head ) {
      OSSIFaceVertexPtr current = head;

      corners.push_back(current);
      coords.push_back(current->getVertexCoords());
      current = current->next();
      while ( current != head ) {
                corners.push_back(current);
                coords.push_back(current->getVertexCoords());
                current = current->next();
      }
    }
  }

  // Does this face contain the given face-vertex?
  bool OSSIFace::contains(OSSIFaceVertexPtr fvptr) {
        if (fvptr){
        if ( head ) {
          OSSIFaceVertexPtr current = head;
          if ( current == fvptr ) return true;
          current = current->next();
          while ( current != head ) {
                    if ( current == fvptr ) return true;
                    current = current->next();
          }
        }
        }
    return false;
  }

  bool OSSIFace::sharesOneVertex( OSSIFacePtr ofp ){
    if (head) {
      int numSharedVerts = 0;
      OSSIFaceVertexPtr current = head;
      OSSIFaceVertexPtrArray ofvparray;
      ofp->getCorners(ofvparray);
      vector<OSSIFaceVertexPtr>::iterator it;
      for ( it = ofvparray.begin(); it != ofvparray.end(); it++){
                current = head;
                if (current->getVertexPtr() == (*it)->getVertexPtr())
                    numSharedVerts++;
                current = current->next();
                while (current != head){
                    if (current->getVertexPtr() == (*it)->getVertexPtr())
                        numSharedVerts++;
                    current = current->next();
                }

      }//end for it loop
      if (numSharedVerts == 1) return true;
      else return false;
    }
    return false;
  }

  //store all neighboring faces in fparray, passed by reference
  void OSSIFace::getNeighboringFaces(OSSIFacePtrArray& fparray) {
    fparray.clear();
    OSSIFaceVertexPtr current = head;
    OSSIFacePtrArray fparr;
    vector<OSSIFacePtr>::iterator it;
    current->getVertexPtr()->getFaces(fparr);
    for ( it = fparr.begin(); it != fparr.end(); it++){
      if ((*it) != this)
                fparray.push_back((*it));
    }
    current = current->next();
    while (current != head){
      current->getVertexPtr()->getFaces(fparr);
      for ( it = fparr.begin(); it != fparr.end(); it++){
                if ((*it) != this)
                    fparray.push_back((*it));
      }
      current = current->next();
    }
  }

  // Find next OSSIFaceVertex - just return the next() for given face-vertex
  OSSIFaceVertexPtr OSSIFace::nextFaceVertex(OSSIFaceVertexPtr fvptr) {
    return fvptr->next();
  }

  // Find previous OSSIFaceVertex - just return the prev() for given face-vertex
  OSSIFaceVertexPtr OSSIFace::prevFaceVertex(OSSIFaceVertexPtr fvptr) {
    return fvptr->prev();
  }

  // Find the OSSIFaceVertex closest to the given point
  OSSIFaceVertexPtr OSSIFace::findClosest(const Vector3d& p) {
    OSSIFaceVertexPtr closest = NULL;

    if ( head ) {
      OSSIFaceVertexPtr current = head;
      double dist, mindist;
      mindist = dist = normsqr(p - current->vertex->coords);
      closest = current;
      current = current->next();
      while ( current != head ) {
                dist = normsqr(p - current->vertex->coords);
                if ( dist < mindist ) {
                    mindist = dist; closest = current;
                }
                current = current->next();
      }
    }
    return closest;
  }

  // Find the two corners which are closest to each other in the two faces
  // The passed OSSIFaceVertex pointers will be set to the closest corners,
  // the first one from the first face and the second one from the second face
  void OSSIFace::findClosestCorners(OSSIFacePtr fp1, OSSIFacePtr fp2,
                                    OSSIFaceVertexPtr& fvp1, OSSIFaceVertexPtr& fvp2) {
    // Traverse face 1 and find closest point in face 2 for each point in face 1
    // using findClosest member function in OSSIFace
    fvp1 = NULL; fvp2 = NULL;

    if ( fp1->head ) {
      double dist, mindist;
      OSSIFaceVertexPtr cur1, cur2;
      fvp1 = cur1 = fp1->head;
      fvp2 = cur2 = fp2->findClosest(cur1->vertex->coords);
      mindist = dist = normsqr(cur1->vertex->coords - cur2->vertex->coords);
      cur1 = cur1->next();
      while ( cur1 != fp1->head ) {
                cur2 = fp2->findClosest(cur1->vertex->coords);
                dist = normsqr(cur1->vertex->coords - cur2->vertex->coords);
                if ( dist < mindist ) {
                    mindist = dist; fvp1 = cur1; fvp2 = cur2;
                }
                cur1 = cur1->next();
      }
    }
  }

  // Insert a new OSSIFaceVertex after a given OSSIFaceVertex
  OSSIFaceVertexPtr OSSIFace::insertAfter(OSSIFaceVertexPtr fvptr, OSSIFaceVertexPtr new_fvp, bool copy) {
    OSSIFaceVertexPtr fvp;

    if ( copy ) fvp = new_fvp->copy();
    else fvp = new_fvp;

    fvp->setFacePtr(this);

    fvp->next() = fvptr->next();
    fvp->next()->prev() = fvp;
    fvp->prev() = fvptr;
    fvptr->next() = fvp;

    return fvp;
  }

  // Insert a new OSSIFaceVertex before a given OSSIFaceVertex
  OSSIFaceVertexPtr OSSIFace::insertBefore(OSSIFaceVertexPtr fvptr, OSSIFaceVertexPtr new_fvp, bool copy) {
    // Find the previous face vertex to the given face vertex
    // and insert the new one after that
    OSSIFaceVertexPtr prev = fvptr->prev();
    return insertAfter(prev,new_fvp,copy);
  }

  void OSSIFace::reorder(OSSIFaceVertexPtr fvptr) {
    // Reorder the face-vertices so that the given face-vertex is the first one
    // Simply have to change the head pointer to point to the new face vertex
    // Check if fvptr belongs to this face. If yes, simply reset head to be fvptr
    // Otherwise don't change anything
    if ( fvptr && this->contains(fvptr) ) head = fvptr;
  }

  void OSSIFace::reverse(void) {
    // Reverse the order of face-vertices in the face
    // For every face-vertex, swap the previous and next fields
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      current->reverse();
      current = current->next();
      while ( current != head ) {
                current->reverse();
                current = current->next();
      }
    }
  }

  vector<int> OSSIFace::vertexWalk( ) const {
    vector<int> verts;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      if ( current->getFacePtr() != this ) {
                cerr << "Data-structure consistency error!" << endl;
                cerr << "Vertex : " << current->getVertexID() << endl
                         << "Face : " << current->getFacePtr()->getID() << endl
                         << "Edge : " << current->getEdgePtr()->getID() << endl;
                return verts;
      }

      if ( current->getVertexPtr() != NULL )
                verts.push_back( current->getVertexID() );
      current = current->next();
      while ( current != head ) {
                if ( current->getVertexPtr() != NULL )
                    verts.push_back( current->getVertexID() );
                current = current->next();
      }
    }
    return verts;
  }

  vector<int> OSSIFace::edgeWalk( ) const {
    vector<int> edges;
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      if ( current->getFacePtr() != this ) {
                cerr << "Data-structure consistency error!" << endl;
                cerr << "Vertex : " << current->getVertexID() << endl
                         << "Face : " << current->getFacePtr()->getID() << endl
                         << "Edge : " << current->getEdgePtr()->getID() << endl;
                return edges;
      }

      if ( current->getEdgePtr() != NULL )
                edges.push_back( current->getEdgeID() );
      current = current->next();
      while ( current != head ) {
                if ( current->getEdgePtr() != NULL )
                    edges.push_back( current->getEdgeID() );
                current = current->next();
      }
    }

    return edges;
  }

  // Boundary walk of OSSIFace
  void OSSIFace::boundaryWalk(void) const {
    cout << "OSSIFace " << uID << " (" << size() << ") : ";
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      if ( current->getFacePtr() != this ) {
                cerr << "Data-structure consistency error!" << endl;
                cerr << "Vertex : " << current->getVertexID() << endl
                         << "Face : " << current->getFacePtr()->getID() << endl
                         << "Edge : " << current->getEdgePtr()->getID() << endl;
                return;
      }
      cout << current->getVertexID();
      cout << '[' << current->backface << ']';
      if ( current->getEdgePtr() != NULL )
                cout << "--(" << current->getEdgeID() << ")-->";
      current = current->next();
      while ( current != head ) {
                cout << current->getVertexID();
                cout << '[' << current->backface << ']';
                if ( current->getEdgePtr() != NULL )
                    cout << "--(" << current->getEdgeID() << ")-->";
                current = current->next();
      }
      cout << current->getVertexID() << '[' << current->backface << ']' << endl;
    }
  }

  void OSSIFace::printPointers(void) const {
    cout << "OSSIFace " << uID << " (" << size() << ") : ";
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      cout << current;
      cout << "--(" << current->getEdgePtr() << ")-->";
      current = current->next();
      while ( current != head ) {
                cout << current;
                cout << "--(" << current->getEdgePtr() << ")-->";
                current = current->next();
      }
      cout << current << endl;
    }
  }

  // Write out OSSIFace in OBJ format
  void OSSIFace::objWrite(ostream& o, uint min_id) const {
    uint index;
    if ( head ) {
      o << 'f';
      OSSIFaceVertexPtr current = head;
      index = current->getVertexID() - min_id;
      o << ' ' << index;
      current = current->next();
      while ( current != head ) {
                index = current->getVertexID() - min_id;
                o << ' ' << index;
                current = current->next();
      }
      o << endl;
    }
  }

  void OSSIFace::objWriteWithNormals(ostream& o, uint min_id, uint& normal_id_start) const {
    uint index;
    if ( head ) {
      o << 'f';
      OSSIFaceVertexPtr current = head;
      index = current->getVertexID() - min_id;
      o << ' ' << index << "//" << normal_id_start;
      ++normal_id_start;
      current = current->next();
      while ( current != head ) {
                index = current->getVertexID() - min_id;
                o << ' ' << index << "//" << normal_id_start;
                ++normal_id_start;
                current = current->next();
      }
      o << endl;
    }
  }

  void OSSIFace::objWriteWithTexCoords(ostream& o, uint min_id, uint& tex_id_start) const {
    uint index;
    if ( head ) {
      o << 'f';
      OSSIFaceVertexPtr current = head;
      index = current->getVertexID() - min_id;
      o << ' ' << index << '/' << tex_id_start;
      ++tex_id_start;
      current = current->next();
      while ( current != head ) {
                index = current->getVertexID() - min_id;
                o << ' ' << index << '/' << tex_id_start;
                ++tex_id_start;
                current = current->next();
      }
      o << endl;
    }
  }

  void OSSIFace::objWriteWithNormalsAndTexCoords(ostream& o, uint min_id, uint& normal_id_start, uint& tex_id_start) const {
    uint index;
    if ( head ) {
      o << 'f';
      OSSIFaceVertexPtr current = head;
      index = current->getVertexID() - min_id;
      o << ' ' << index << '/' << tex_id_start << '/' << normal_id_start;
      ++tex_id_start; ++normal_id_start;
      current = current->next();
      while ( current != head ) {
                index = current->getVertexID() - min_id;
                o << ' ' << index << '/' << tex_id_start << '/' << normal_id_start;
                ++tex_id_start; ++normal_id_start;
                current = current->next();
      }
      o << endl;
    }
  }

  // Write out normals at each vertex in OBJ format
  void OSSIFace::objWriteNormals(ostream& o) const {
    if ( head ) {
      Vector3d n;
      OSSIFaceVertexPtr current = head;
      n = current->getNormal();
      o << "vn " << n[0] << ' ' << n[1] << ' ' << n[2] << endl;
      current = current->next();
      while ( current != head ) {
                n = current->getNormal();
                o << "vn " << n[0] << ' ' << n[1] << ' ' << n[2] << endl;
                current = current->next();
      }
    }
  }

  // Write out texture coordinates at each vertex in OBJ format
  void OSSIFace::objWriteTexCoords(ostream& o) const {
    if ( head ) {
      Vector2d t;
      OSSIFaceVertexPtr current = head;
      t = current->getTexCoords();
      o << "vt " << t[0] << ' ' << t[1] << endl;
      current = current->next();
      while ( current != head ) {
                t = current->getTexCoords();
                o << "vt " << t[0] << ' ' << t[1] << endl;
                current = current->next();
      }
    }
  }

  void OSSIFace::writeOSSI(ostream& o) const {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      o << 'f';
      //o << " {" << getID() << "}";
      o << ' ' << current->getIndex();
      current = current->next();
      while ( current != head ) {
                o << ' ' << current->getIndex();
                current = current->next();
      }
      o << endl;
    }
  }

  void OSSIFace::writeOSSIReverse(ostream& o) const {
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      o << 'f';
      //o << " {" << getID() << "}";
      o << ' ' << current->getIndex();
      current = current->prev();
      while ( current != head ) {
                o << ' ' << current->getIndex();
                current = current->prev();
      }
      o << endl;
    }
  }

  void OSSIFace::for_each(void (*func)(OSSIFaceVertexPtr)) const {
    // Apply given function to every face-vertex in the face starting at head
    if ( head ) {
      OSSIFaceVertexPtr current = head;
      func(current);
      current = current->next();
      while ( current != head ) {
                func(current);
                current = current->next();
      }
    }
  }

  ostream& operator << (ostream& o, const OSSIFace& face) {
    o << "Face (" << face.size() << ") : ";
    if ( face.head ) {
      OSSIFaceVertexPtr current = face.head;
      o << current->vertex->coords;
      current = current->next();
      while ( current != face.head ) {
                o << ", " << current->vertex->coords;
                current = current->next();
      }
    }
    return o;
  }

    float OSSIFace::getArea(){
        updateCentroid();

        vector<OSSIEdgePtr>::iterator eit;
        OSSIEdgePtrArray eptrarray;

        float area = 0;
        //get the edges for the current face
        getEdges(eptrarray);
        for(eit = eptrarray.begin(); eit != eptrarray.end(); eit++){
            //get the 2 faces for the current edge, select them if they aren't already selected
            OSSIVertexPtr vp1, vp2;
            //get the two vertices for each edge, select them if they aren't already selected
            (*eit)->getVertexPointers(vp1,vp2);
            //find the area of this triangle
            area += 0.5 * (vp1->coords-centroid)*(vp1->coords-vp2->coords);
        }
        return area;
    }

  void boundaryWalk(OSSIFacePtr faceptr)
  {
    faceptr->boundaryWalk();
  }

  void boundaryWalk(const OSSIFace& face)
  {
    face.boundaryWalk();
  }


} // end namespace

