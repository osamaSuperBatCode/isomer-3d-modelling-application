/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSICommon.h
 */

#ifndef _OSSI_COMMON_H_
#define _OSSI_COMMON_H_

// Common includes, definitions and declarations for OSSI

#include <vector>
#include <list>
#include <ext/hash_map>
#include <algorithm>
#include <iterator>
#include <Vector2d.h>
#include <Vector3d.h>
#include <Matrix4x4.h>
#include <Base/Inlines.h>
#include <Base/StreamIO.h>
#include <Graphics/Color.h>
#include <cstring>
#include <cstdlib>

// strstream is deprecated in ..but what gives as long as GNU and MinGW Compile it :D
#ifdef __GNUG__
#include <sstream>
typedef stringstream StringStream;
#else
#include <strstream.h>
typedef strstream StringStream;
#endif

#ifdef __GNUG__
#include <fstream>
#else
#include <fstream.h>
#endif

// This is required if the standard versions of the STL header files are included
using namespace std;

// Forward declare all the classes and define typedefs for simplicity

namespace OSSI {

  class OSSIVertex;
  class OSSIFaceVertex;
  class OSSIEdge;
  class OSSIFace;
  class OSSIObject;
  class OSSIMaterial;


  typedef OSSIVertex * OSSIVertexPtr;
  typedef OSSIFaceVertex * OSSIFaceVertexPtr;
  typedef OSSIEdge * OSSIEdgePtr;
  typedef OSSIFace * OSSIFacePtr;
  typedef OSSIObject * OSSIObjectPtr;
  typedef OSSIMaterial * OSSIMaterialPtr;


  typedef vector<OSSIVertex> OSSIVertexArray;
  typedef vector<OSSIVertexPtr> OSSIVertexPtrArray;
  typedef list<OSSIVertex> OSSIVertexList;
  typedef list<OSSIVertexPtr> OSSIVertexPtrList;

  typedef vector<OSSIFaceVertex> OSSIFaceVertexArray;
  typedef vector<OSSIFaceVertexPtr> OSSIFaceVertexPtrArray;
  typedef list<OSSIFaceVertex> OSSIFaceVertexList;
  typedef list<OSSIFaceVertexPtr> OSSIFaceVertexPtrList;

  typedef vector<OSSIEdge> OSSIEdgeArray;
  typedef vector<OSSIEdgePtr> OSSIEdgePtrArray;
  typedef list<OSSIEdge> OSSIEdgeList;
  typedef list<OSSIEdgePtr> OSSIEdgePtrList;

  typedef vector<OSSIFace> OSSIFaceArray;
  typedef vector<OSSIFacePtr> OSSIFacePtrArray;
  typedef list<OSSIFace> OSSIFaceList;
  typedef list<OSSIFacePtr> OSSIFacePtrList;

  typedef vector<OSSIObject> OSSIObjectArray;
  typedef vector<OSSIObjectPtr> OSSIObjectPtrArray;
  typedef list<OSSIObject> OSSIObjectList;
  typedef list<OSSIObjectPtr> OSSIObjectPtrList;

  typedef vector<OSSIMaterial> OSSIMaterialArray;
  typedef vector<OSSIMaterialPtr> OSSIMaterialPtrArray;
  typedef list<OSSIMaterial> OSSIMaterialList;
  typedef list<OSSIMaterialPtr> OSSIMaterialPtrList;

  typedef vector<Vector4d> Vector4dArray;
  typedef vector<Vector4dPtr> Vector4dPtrArray;
  typedef list<Vector4d> Vector4dList;
  typedef list<Vector4dPtr> Vector4dPtrList;

  typedef vector<Vector4dArray> Vector4dGrid;
  typedef vector<Vector4dPtrArray> Vector4dPtrGrid;

  typedef vector<Vector3d> Vector3dArray;
  typedef vector<Vector3dPtr> Vector3dPtrArray;
  typedef list<Vector3d> Vector3dList;
  typedef list<Vector3dPtr> Vector3dPtrList;

  typedef vector<Vector3dArray> Vector3dGrid;
  typedef vector<Vector3dPtrArray> Vector3dPtrGrid;

  typedef vector<Vector2d> Vector2dArray;
  typedef vector<Vector2dPtr> Vector2dPtrArray;
  typedef list<Vector2d> Vector2dList;
  typedef list<Vector2dPtr> Vector2dPtrList;

  typedef vector<Vector2dArray> Vector2dGrid;
  typedef vector<Vector2dPtrArray> Vector2dPtrGrid;

  typedef vector<double> DoubleArray;
  typedef list<double> DoubleList;

  typedef vector<DoubleArray> DoubleGrid;

  typedef vector<int> IntArray;
  typedef list<int> IntList;

  typedef vector<IntArray> IntGrid;

  // Define some types/flags needed for subdivision surfaces and other operations
  enum OSSIVertexType {
    VTNormal=0,
    VTNewFacePoint=1,
    VTNewEdgePoint=2,
    VTNewPoint=3,
    VTOld=4,
    VTNewSub=5,
    VTWireInside=6,
    VTWire=7,
    VTWireSubD=8,
    VTWireSubDInside=9 };

  enum OSSIFaceVertexType {
    FVTNormal=0,
    FVTNew=1,
    FVTWire=2 };

  enum OSSIEdgeType {
    ETNormal=0,
    ETNew=1,
    ETCollapse=2,
    ETCollapseAux=3,
    ETOriginal=4,
    ETChull=5,
    ETdoNotDelete=6,
    ETdoDelete=7 };

  enum OSSIFaceType {
    FTNormal=0,
    FTNew=1,
    FTHole=2,
    FTHoleInside=3,
    FTWire=4 };


  // Common utility functions

  /*
    Remove node specified by index from vector specified by array
    It is assumed that order of elements within the array is not important.
    Copy the last element to the specified index and delete the last element.
    Iterators pointing to the deleted index and to the last element are invalidated.
    Relative ordering of iterators is also changed for index, last element and adjacent nodes.
  */

  template <class T, class Alloc>
  inline void remove(vector<T,Alloc>& array, typename vector<T,Alloc>::size_type index) {
    array[index] = array.back();
    array.pop_back();
  }

  // Functions to clear lists/arrays of pointers, where the lists/arrays own
  // the objects pointed to.
  // These are needed since the STL functions merely erase the node

  void erase_ovp(OSSIVertexPtr vp);
  void erase_ofvp(OSSIFaceVertexPtr fvp);
  void erase_oep(OSSIEdgePtr ep);
  void erase_ofp(OSSIFacePtr fp);
  void erase_oop(OSSIObjectPtr op);
  void erase_omp(OSSIMaterialPtr mp);

  void clear(OSSIVertexPtrArray& vparray);
  void clear(OSSIFaceVertexPtrArray& fvparray);
  void clear(OSSIEdgePtrArray& eparray);
  void clear(OSSIFacePtrArray& fparray);
  void clear(OSSIObjectPtrArray& oparray);
  void clear(OSSIMaterialPtrArray& mparray);

  void clear(OSSIVertexPtrList& vplist);
  void clear(OSSIFaceVertexPtrList& fvplist);
  void clear(OSSIEdgePtrList& eplist);
  void clear(OSSIFacePtrList& fplist);
  void clear(OSSIObjectPtrList& oplist);
  void clear(OSSIMaterialPtrList& mplist);

  struct eqstr {
    bool operator() ( int a, int b ) const {
      return a == b;
    }
  };

  typedef __gnu_cxx::hash<unsigned int> Hash;
  typedef __gnu_cxx::hash_map<unsigned int, unsigned int, Hash, eqstr> HashMap;

} // end namespace

#endif /* #ifndef _OSSI_COMMON_H_ */
