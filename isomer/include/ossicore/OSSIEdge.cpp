/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 *  OSSIEdge.cpp
 */

#include "OSSIEdge.h"
#include "OSSIFaceVertex.h"
#include "OSSIVertex.h"

namespace OSSI {

  // Define the static variable. Initialized to 0

  uint OSSIEdge::suLastID = 0;

  void OSSIEdge::dump(ostream& o) const
  {
    o << "OSSIEdge" << endl

      << "  fvpV1 : " << fvpV1 << endl
      << "  fvpV2 : " << fvpV2 << endl

      << endl;
  }

  // Update the mid point for this edge
  void OSSIEdge::updateMidPoint(void)
  {
    if ( fvpV1 != NULL && fvpV2 != NULL ) midpoint = 0.5 * (fvpV1->getVertexCoords() + fvpV2->getVertexCoords());
  }

  // Update the edge normal - average of normals at the 4 corners adjacent to this edge
  void OSSIEdge::updateNormal(void) {
    if ( fvpV1 != NULL && fvpV2 != NULL ) {
      normal = fvpV1->computeNormal() + fvpV1->next()->computeNormal() +
    fvpV2->computeNormal() + fvpV2->next()->computeNormal();
      normalize(normal);
    }
  }

  // Reverse the OSSI edge. Update face-vertex pointers appropriately
  // See comment in header file before using this function
  void OSSIEdge::reverse(void) // PRIVATE
  {
    // Clear edge pointer fields of original face-vertices
    // If edge is reversed then the next face-vertices will be the starting
    // points for this edge in the corresponding faces.
    // Update the edge pointer fields of the new face-vertices.
    //fvpV1->setEdgePtr(NULL);
    fvpV1 = fvpV1->next(); fvpV1->setEdgePtr(this);
    //fvpV2->setEdgePtr(NULL);
    fvpV2 = fvpV2->next(); fvpV2->setEdgePtr(this);
  }

  void OSSIEdge::updateFaceVertices(void)
  {
    fvpV1->setEdgePtr(this);
    fvpV2->setEdgePtr(this);
  }

  // Get face-vertex pointer belonging to give face. Returns NULL if not found
  OSSIFaceVertexPtr OSSIEdge::getFaceVertexPtr(OSSIFacePtr fptr)
  {
    OSSIFacePtr fp1, fp2;
    OSSIFaceVertexPtr fvp = NULL;
    fp1 = fvpV1->getFacePtr(); fp2 = fvpV2->getFacePtr();
    if ( fp1 == fptr ) fvp = fvpV1;
    else if ( fp2 == fptr ) fvp = fvpV2;
    return fvp;
  }

  // Check if two given edges are cofacial
  bool coFacial(OSSIEdgePtr ep1, OSSIEdgePtr ep2)
  {
    OSSIFacePtr fp11,fp12,fp21,fp22;
    ep1->getFacePointers(fp11,fp12);
    ep2->getFacePointers(fp21,fp22);
    bool cofacial = false;
    if ( fp11 == fp21 || fp11 == fp22 ||
     fp12 == fp21 || fp12 == fp22 ) cofacial = true;
    return cofacial;
  }

  void OSSIEdge::getEndPoints(Vector3d& p1, Vector3d& p2) const
  {
    p1 = fvpV1->getVertexCoords(); p2 = fvpV2->getVertexCoords();
  }

  double OSSIEdge::length(void) const
  {
    return norm( fvpV1->getVertexCoords() - fvpV2->getVertexCoords() );
  }

  Vector3d OSSIEdge::getEdgeVector(void) const
  {
    return Vector3d( fvpV2->getVertexCoords() - fvpV1->getVertexCoords() );
  }

  void OSSIEdge::getFacePointers(OSSIFacePtr& fptr1, OSSIFacePtr& fptr2)
  {
    fptr1 = fvpV1->getFacePtr(); fptr2 = fvpV2->getFacePtr();
  }

  OSSIFacePtr OSSIEdge::getOtherFacePointer(OSSIFacePtr fptr)
  {
    // Each edge is adjacent to 2 faces (both can be same)
    // Return the pointer to the face which is not the given face pointer
    // NOTE: If both sides of the Edge are the same face, then
    // other face pointer will be the same.
    // If given face pointer is not adjacent to this edge, returns NULL
    if ( fvpV1->getFacePtr() == fptr ) return fvpV2->getFacePtr();
    else if ( fvpV2->getFacePtr() == fptr ) return fvpV1->getFacePtr();
    return NULL;
  }

  void OSSIEdge::getVertexPointers(OSSIVertexPtr& vp1, OSSIVertexPtr& vp2)
  {
    vp1 = fvpV1->getVertexPtr(); vp2 = fvpV2->getVertexPtr();
  }

  OSSIVertexPtr OSSIEdge::getOtherVertexPointer(OSSIVertexPtr vptr)
  {
    if ( fvpV1->getVertexPtr() == vptr ) return fvpV2->getVertexPtr();
    else if ( fvpV2->getVertexPtr() == vptr ) return fvpV1->getVertexPtr();
    return NULL;
  }

  void OSSIEdge::getEFCorners(OSSIFaceVertexPtrArray& corners)
  {
    corners.resize(4);
    corners[0] = fvpV1->next();
    corners[1] = fvpV1;
    corners[2] = fvpV2->next();
    corners[3] = fvpV2;
  }

  void OSSIEdge::getEFCornersAuxCoords(Vector3dArray& coords)
  {
    coords.resize(4);
    coords[0] = fvpV1->next()->getAuxCoords();
    coords[1] = fvpV1->getAuxCoords();
    coords[2] = fvpV2->next()->getAuxCoords();
    coords[3] = fvpV2->getAuxCoords();
  }

  // Equality operator - two edges are equal if they have the same 2 end-points
  bool operator == (const OSSIEdge& e1, const OSSIEdge& e2)
  {
    bool same = false;
    uint e11,e12,e21,e22;

    e11 = e1.fvpV1->getVertexID();
    e12 = e1.fvpV2->getVertexID();
    e21 = e2.fvpV1->getVertexID();
    e22 = e2.fvpV2->getVertexID();

    if ( (e11 == e21) && (e12 == e22) ) same = true;
    else if ( (e11 == e22) && (e12 == e21) ) same = true;
    return same;
  }

  bool OSSIEdge::isValid(void) const
  {
    if ( fvpV1->getFacePtr() == fvpV2->getFacePtr() ) return false;
    return true;
  }

  bool OSSIEdge::isSelfLoop(void) const
  {
    if ( fvpV1->getVertexPtr() == fvpV2->getVertexPtr() ) return true;
    return false;
  }

  bool checkIntersection(OSSIEdgePtr ep1, OSSIEdgePtr ep2) // friend function
  {
    // Check if the 2 given edges intersect
    // We will check only for in-plane intersections
    // So if the 4 end points do not lie on a plane, function will return false
    // If one end point of one edge lies on the other edge, it IS NOT considered an intersection
    // If the edges are co-linear, it IS NOT considered an intersection
    // If either of the edges are self loops, there is NO intersection
    if ( ep1->isSelfLoop() || ep2->isSelfLoop() )
      {
    cout << "Self loop" << endl;
    return false;
      }
    bool intersects = false;
    Vector3d ep1v1,ep1v2,ep2v1,ep2v2;
    ep1->getEndPoints(ep1v1,ep1v2);
    ep2->getEndPoints(ep2v1,ep2v2);

    // Find normals of triangle formed by ep1 and each end point of ep2
    // If angle between the normals is not 0 the 4 end points are not co-planar
    Vector3d n1 = (ep1v2-ep1v1) % (ep2v1-ep1v1); normalize(n1);
    Vector3d n2 = (ep1v2-ep1v1) % (ep2v2-ep1v1); normalize(n2);

    if ( normsqr(n1) < 1.0e-5 && normsqr(n2) < 1.0e-5 )
      {
    return false; // Co-linear edges
      }

    // Check the sum between n1 and n2
    // If difference is 0, the points are co-planar but ep2 lies
    // entirely on one side of ep1, so there is no possibility of intersection
    // If sum is 0, the points are co-planar and end points of ep2 lie
    // on either side of ep1. Then check if point of intersection is within the edge
    if ( normsqr(n1+n2) < 1.0e-4 )
      {
    // Points are co-planar, and there is possibility of intersection
    // Check if point of intersection is within the edge
    // We can simply check if ep1 is entirely on one side of ep2
    // If it is not, then there is an intersection
    n1 = (ep2v2-ep2v1) % (ep1v1-ep2v1); normalize(n1);
    n2 = (ep2v2-ep2v1) % (ep1v2-ep2v1); normalize(n2);
    if ( normsqr(n1+n2) < 1.0e-4 )
          {
            intersects = true;
          }
      }
    return intersects;
  }

  ostream& operator << (ostream& o, const OSSIEdge& e)
  {
    o << "Edge " << e.getID() << " : "
      << e.fvpV1->getVertexID() << "<-->"
      << e.fvpV2->getVertexID();
    return o;
  }

  void OSSIEdge::printFaceIDs(void) const
  {
    cout << "Edge " << uID << " : "
     << fvpV1->getFaceID() << " (" << fvpV1->getVertexID() << ") " << "<--> "
     << fvpV2->getFaceID() << " (" << fvpV2->getVertexID() << ") " << endl;
  }

  void OSSIEdge::printPointers(void) const
  {
    cout << fvpV1 << " <-> " << fvpV2;
  }

  // Write out the edge in OSSI format
  void OSSIEdge::writeOSSI(ostream& o) const
  {

    o << "e " << fvpV1->getIndex() << ' ' << fvpV2->getIndex() << endl;
  }

  // Write out the edge in OSSI format in reverse order
  // Reverse of edge will point to corners following the current corners
  void OSSIEdge::writeOSSIReverse(ostream& o) const
  {

    o << "e " << (fvpV1->next())->getIndex() << ' ' << (fvpV2->next())->getIndex() << endl;
  }

  // Distance of a point from the OSSIEdge in 3D
  double OSSIEdge::distFrom(double x, double y, double z) const
  {
    Vector3d p1,p2;                                      // 2 ends of the Edges
    Vector3d p(x,y,z);

    p1 = fvpV1->vertex->coords;
    p2 = fvpV2->vertex->coords;

    double len = norm(p1-p2);                            // Length of line segment
    double dist;
    if ( len < ZERO )
      {
    // Find distance from one of the 2 end points and return that
    dist = norm(p-p1);
      }
    else
      {
    double u;                                       // Parametric coordinate along edge
    // from p1 to p2
    u = ( (p-p1) * (p2-p1) ) / sqr(len);
    if ( u < 0.0 )
          {
            dist = norm(p-p1);
          }
    else if ( u > 1.0 )
          {
            dist = norm(p-p2);
          }
    else
          {
            Vector3d p3 = p1 + u * (p2-p1);
            dist = norm(p-p3);
          }
      }
    return dist;
  }

  // For hit calculations with projections
  double OSSIEdge::distFromXY(double x, double y) const
  {
    Vector3d p1temp,p2temp;
    Vector2d p1,p2;
    Vector2d p(x,y);

    p1temp = fvpV1->vertex->coords;
    p2temp = fvpV2->vertex->coords;

    // Convert to 2D coords in XY plane
    p1.set(p1temp[0],p1temp[1]); p2.set(p2temp[0],p2temp[1]);

    double len = norm(p1-p2);                            // Length of line segment in XY plane
    double dist;
    if ( len < ZERO )
      {
    // Find distance from one of the 2 end points and return that
    dist = norm(p-p1);
      }
    else
      {
    double u;                                       // Parametric coordinate along edge
    // from p1 to p2
    u = ( (p-p1) * (p2-p1) ) / sqr(len);
    if ( u < 0.0 )
          {
            dist = norm(p-p1);
          }
    else if ( u > 1.0 )
          {
            dist = norm(p-p2);
          }
    else
          {
            Vector2d p3 = p1 + u * (p2-p1);
            dist = norm(p-p3);
          }
      }
    return dist;
  }

  double OSSIEdge::distFromYZ(double y, double z) const
  {
    Vector3d p1temp,p2temp;
    Vector2d p1,p2;
    Vector2d p(y,z);

    p1temp = fvpV1->vertex->coords;
    p2temp = fvpV2->vertex->coords;

    // Convert to 2D coords in YZ plane
    p1.set(p1temp[1],p1temp[2]); p2.set(p2temp[1],p2temp[2]);

    double len = norm(p1-p2);                            // Length of line segment in YZ plane
    double dist;
    if ( len < ZERO )
      {
    // Find distance from one of the 2 end points and return that
    dist = norm(p-p1);
      }
    else
      {
    double u;                                       // Parametric coordinate along edge
    // from p1 to p2
    u = ( (p-p1) * (p2-p1) ) / sqr(len);
    if ( u < 0.0 )
          {
            dist = norm(p-p1);
          }
    else if ( u > 1.0 )
          {
            dist = norm(p-p2);
          }
    else
          {
            Vector2d p3 = p1 + u * (p2-p1);
            dist = norm(p-p3);
          }
      }
    return dist;
  }

  double OSSIEdge::distFromZX(double z, double x) const
  {
    Vector3d p1temp,p2temp;
    Vector2d p1,p2;
    Vector2d p(z,x);

    p1temp = fvpV1->vertex->coords;
    p2temp = fvpV2->vertex->coords;

    // Convert to 2D coords in ZX plane
    p1.set(p1temp[2],p1temp[0]); p2.set(p2temp[2],p2temp[0]);

    double len = norm(p1-p2);                            // Length of line segment in ZX plane
    double dist;
    if ( len < ZERO )
      {
    // Find distance from one of the 2 end points and return that
    dist = norm(p-p1);
      }
    else
      {
    double u;                                       // Parametric coordinate along edge
    // from p1 to p2
    u = ( (p-p1) * (p2-p1) ) / sqr(len);
    if ( u < 0.0 )
          {
            dist = norm(p-p1);
          }
    else if ( u > 1.0 )
          {
            dist = norm(p-p2);
          }
    else
          {
            Vector2d p3 = p1 + u * (p2-p1);
            dist = norm(p-p3);
          }
      }
    return dist;
  }

  // Distance betwen midpoint of two edges - friend function
  double distBetween(OSSIEdgePtr ep1, OSSIEdgePtr ep2)
  {
    return norm( ep1->getMidPoint() - ep2->getMidPoint() );
  }


  void resetEdgeType(OSSIEdgePtr oep)
  {
    oep->resetType();
  }

  void makeEdgeUnique(OSSIEdgePtr oep)
  {
    oep->makeUnique();
  }



} // end namespace

