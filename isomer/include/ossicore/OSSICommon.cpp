/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSICommon.h
 */

#include "OSSICommon.h"
#include "OSSIVertex.h"
#include "OSSIFaceVertex.h"
#include "OSSIEdge.h"
#include "OSSIFace.h"
#include "OSSIObject.h"

namespace OSSI {

  void erase_ovp(OSSIVertexPtr vp) { delete vp; }

  void erase_ofvp(OSSIFaceVertexPtr fvp) { delete fvp; }

  void erase_oep(OSSIEdgePtr ep) { delete ep; }

  void erase_ofp(OSSIFacePtr fp) { delete fp; }

  void erase_oop(OSSIObjectPtr op) { delete op; }

  void erase_omp(OSSIMaterialPtr mp) { delete mp; }

  void clear(OSSIVertexPtrArray& vparray) {
    for_each(vparray.begin(),vparray.end(),erase_ovp);
    vparray.clear();
  }

  void clear(OSSIFaceVertexPtrArray& fvparray) {
    for_each(fvparray.begin(),fvparray.end(),erase_ofvp);
    fvparray.clear();
  }

  void clear(OSSIEdgePtrArray& eparray) {
    for_each(eparray.begin(),eparray.end(),erase_oep);
    eparray.clear();
  }

  void clear(OSSIFacePtrArray& fparray) {
    for_each(fparray.begin(),fparray.end(),erase_ofp);
    fparray.clear();
  }

  void clear(OSSIObjectPtrArray& oparray) {
    for_each(oparray.begin(),oparray.end(),erase_oop);
    oparray.clear();
  }

  void clear(OSSIMaterialPtrArray& mparray) {
    for_each(mparray.begin(),mparray.end(),erase_omp);
    mparray.clear();
  }


  void clear(OSSIVertexPtrList& vplist) {
    for_each(vplist.begin(),vplist.end(),erase_ovp);
    vplist.clear();
  }

  void clear(OSSIFaceVertexPtrList& fvplist) {
    for_each(fvplist.begin(),fvplist.end(),erase_ofvp);
    fvplist.clear();
  }

  void clear(OSSIEdgePtrList& eplist) {
    for_each(eplist.begin(),eplist.end(),erase_oep);
    eplist.clear();
  }

  void clear(OSSIFacePtrList& fplist) {
    for_each(fplist.begin(),fplist.end(),erase_ofp);
    fplist.clear();
  }

  void clear(OSSIObjectPtrList& oplist) {
    for_each(oplist.begin(),oplist.end(),erase_oop);
    oplist.clear();
  }

  void clear(OSSIMaterialPtrList& mplist) {
    for_each(mplist.begin(),mplist.end(),erase_omp);
    mplist.clear();
  }

} // end namespace

