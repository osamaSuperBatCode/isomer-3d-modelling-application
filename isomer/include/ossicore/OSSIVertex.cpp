/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 * OSSIVertex.cpp
 */

// Non-inline function definitions for OSSIVertex class

#include "OSSIVertex.h"
#include "OSSIFace.h"
#include "OSSIEdge.h"

namespace OSSI {
  uint OSSIVertex::suLastID = 0;

  // Dump contents of this object to an output stream
  void OSSIVertex::dump(ostream& o) const {
    o << "OSSIVertex" << endl

      << "  fvpList" << endl;

    OSSIFaceVertexPtrList::const_iterator first, last;
    int i=0;

    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      o << "    " << i++ << " : " << (*first) << endl;
      ++first;
    }
    o << endl;
  }

  // Reset type of vertex, face-vertices and edges associated with this vertex
  void OSSIVertex::resetTypeDeep(void) {
    // Reset vertex type
    resetType();

    // Go through face-vertex list and reset type of each face-vertex
    // and the edge starting at that face-vertex
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      fvp->resetType();
      fvp->getEdgePtr()->resetType();
    }
  }

  // Set the texture coordinates for all FaceVertexes referring to this vertex
  void OSSIVertex::setTexCoords(const Vector2d& texcoord) {
    OSSIFaceVertexPtrList::iterator first, last;

    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      (*first)->texcoord = texcoord;
      ++first;
    }
  }

  // Set the color values for all FaceVertexes referring to this vertex
  void OSSIVertex::setColor(const RGBColor& color) {
    OSSIFaceVertexPtrList::iterator first, last;

    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      (*first)->color = color;
      ++first;
    }
  }

  // Set the normals for all FaceVertexes referring to this vertex
  Vector3d OSSIVertex::computeNormal( bool set ) {
    OSSIFaceVertexPtrList::iterator first, last;
    Vector3d normal;
    int i=0;

    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      normal += (*first)->computeNormal();
      ++first; ++i;
    }
    normal /= i;
    if ( set ) {
      first = fvpList.begin();
      while ( first != last ) {
    (*first)->normal = normal;
    ++first;
      }
    }
    return normal;
  }

  // Compute the normals for all FaceVertexes referring to this vertex
  // Update the vertex normal and return it
  Vector3d OSSIVertex::updateNormal(bool recompute) {
    OSSIFaceVertexPtrList::iterator first, last;

    first = fvpList.begin(); last = fvpList.end();
    normal.reset();
    if ( recompute )
      while ( first != last ) {
    normal += (*first)->computeNormal();
    ++first;
      }
    else
      while ( first != last ) {
    normal += (*first)->normal;
    ++first;
      }

    normalize(normal);
    return normal;
  }

  Vector3d OSSIVertex::getNormals(Vector3dArray& normals) {
    // Return normals at all corners in an Vector3dArray
    Vector3d avenormal;
    int numnormals = fvpList.size();
    if ( numnormals > 0 ) {
      normals.clear(); normals.reserve(numnormals);

      OSSIFaceVertexPtrList::const_iterator first, last;
      OSSIFaceVertexPtr fvp = NULL;
      first = fvpList.begin(); last = fvpList.end();
      while ( first != last ) {
    fvp = (*first); ++first;
    normals.push_back(fvp->normal); avenormal += fvp->normal;
      }
      avenormal /= numnormals;
    }
    return avenormal;
  }

  // Set tex coordinates, color and normal info for all FaceVertexes referring to this vertex
  void OSSIVertex::setFaceVertexProps(const Vector2d& texcoord, const RGBColor& color, const Vector3d& n) {
    OSSIFaceVertexPtrList::iterator first, last;

    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      (*first)->texcoord = texcoord;
      (*first)->color = color;
      (*first)->normal = n;
      ++first;
    }
  }

  void OSSIVertex::vertexTrace(void) const {
    // Output all edges incident on this OSSIVertex in the specific rotation order

    // Pick a OSSIFaceVertex from the list - first one
    OSSIFaceVertexPtr fvptr1 = fvpList.front();

    if ( fvptr1 == NULL ) return;

    OSSIEdgePtr e1,e2;
    OSSIFaceVertexPtr fvptr2;
    OSSIFacePtr f;

    // Find Edge starting at this OSSIFaceVertex
    e1 = fvptr1->getEdgePtr();
    e2 = e1;

    do {
      cout << (*e2) << " ";

      // Find the other OSSIFaceVertex for this OSSIEdge
      fvptr2 = e2->getOtherFaceVertexPtr(fvptr1);
      if ( fvptr2 == NULL ) return;

      // Find the Face to which the second OSSIFaceVertex belongs
      f = fvptr2->getFacePtr();

      // Find the OSSIFaceVertex following the second OSSIFaceVertex in this Face
      fvptr1 = f->nextFaceVertex(fvptr2);

      // Find Edge starting at this OSSIFaceVertex
      e2 = fvptr1->getEdgePtr();
    }
    while ( e2 != e1 );

    cout << endl;
  }

  int OSSIVertex::getEdges(OSSIEdgePtr ** edges) const {
    // Create an array of OSSIEdge's incident on this OSSIVertex. The number of OSSIEdges
    // is returned. Memory will be allocated inside this function, which should
    // be freed by the caller. Pass a pointer to the array (OSSIEdge ** or OSSIEdgePtr *)
    // Equivalent to a vertex trace
    // In case of an error -1 is returned, but memory is not freed

    int num_edges = fvpList.size();
    int i = 0;

    if ( num_edges <= 0 ) return -1;

    // Allocate memory for the OSSIEdgePtr array
    *edges = new OSSIEdgePtr[num_edges];

    OSSIFaceVertexPtrList::const_iterator first, last;
    OSSIFaceVertexPtr fvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      edges[0][i++] = fvp->getEdgePtr();
    }
    return num_edges;
  }

  void OSSIVertex::getEdges(OSSIEdgePtrArray& edges) const {
    // Return edges in an OSSIEdgePtrArray


        if (fvpList.size() > 0){
        edges.clear(); edges.reserve(fvpList.size());

        OSSIFaceVertexPtrList::const_iterator first, last;
        OSSIFaceVertexPtr fvp = NULL;
        first = fvpList.begin(); last = fvpList.end();
        while ( first != last ) {
          fvp = (*first); ++first;
          edges.push_back(fvp->getEdgePtr());
        }
        }
  }

  OSSIEdgePtr OSSIVertex::getEdgeTo(OSSIVertexPtr vp) {
    // Get the Edge incident on this Vertex which connects to given Vertex
    // If no such edge exists, returns NULL
    OSSIEdgePtr ep, retep = NULL;
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp = NULL, ofvp;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      ep = fvp->getEdgePtr();
      if ( ep != NULL ) {
    ofvp = ep->getOtherFaceVertexPtr(fvp);
    if ( ofvp && ofvp->vertex == vp ) {
      retep = ep; break;
    }
      }
    }
    return retep;
  }

  void OSSIVertex::getFaceVertices(OSSIFaceVertexPtrArray& fvparray) {
    // Go through the face-vertex-pointer list and add each
    // face vertex pointer to the array
    fvparray.clear(); fvparray.reserve(fvpList.size());
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      fvparray.push_back(fvp);
    }
  }

  void OSSIVertex::getOrderedFaceVertices(OSSIFaceVertexPtrArray& fvparray) {
    // Get the face vertices associated with this vertex in the clockwise rotation order
    fvparray.clear();

    OSSIFaceVertexPtr fvpstart = fvpList.front();
    if ( fvpstart == NULL ) return;

    fvparray.reserve(fvpList.size());
    OSSIFaceVertexPtr fvp = fvpstart;

    do {
      fvparray.push_back(fvp);
      fvp = fvp->vnext();
    } while ( fvp != fvpstart );
  }


  void OSSIVertex::getCornerAuxCoords(Vector3dArray& coords) const {
    coords.clear(); coords.reserve(fvpList.size());
    OSSIFaceVertexPtrList::const_iterator first, last;
    OSSIFaceVertexPtr fvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      coords.push_back(fvp->getAuxCoords());
    }
  }

  void OSSIVertex::getOrderedCornerAuxCoords(Vector3dArray& coords) const {
    // Get the aux coords of face vertices associated with this vertex in the clockwise rotation order
    coords.clear();

    OSSIFaceVertexPtr fvpstart = fvpList.front();
    if ( fvpstart == NULL ) return;

    coords.reserve(fvpList.size());
    OSSIFaceVertexPtr fvp = fvpstart;

    do {
      coords.push_back(fvp->getAuxCoords());
      fvp = fvp->vnext();
    } while ( fvp != fvpstart );
  }


  void OSSIVertex::getFaces(OSSIFacePtrArray& fparray) {
    // Go through the face-vertex-pointer list and add
    // face pointer of each face vertex pointer to the array
    fparray.clear(); fparray.reserve(fvpList.size());
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      fparray.push_back(fvp->getFacePtr());
    }
  }

  OSSIFaceVertexPtr OSSIVertex::getFaceVertexInFace(OSSIFacePtr fp) {
    // Get the FaceVertex belonging to the given face. If only 1 face-vertex
    // is there in the list, return that. If more than 1 exist but nothing
    // belongs to given face, return NULL
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp, retfvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    if ( fvpList.size() == 1 )
      retfvp = (*first);
    else {
      while ( first != last ) {
    fvp = (*first); ++first;
    if ( fvp->getFacePtr() == fp ) {
      retfvp = fvp;
      break;
    }
      }
    }
    return retfvp;
  }

  OSSIFaceVertexPtr OSSIVertex::getFaceVertexWithPrev(OSSIVertexPtr vp) {
    // Get the FaceVertex which has the given Vertex before it in it's Face
    // If only 1 FaceVertex refers to this Vertex, will return that
    // If there are more than 1 and none of them satisfies the condition, returns NULL
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp, retfvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    if ( fvpList.size() == 1 )
      retfvp = (*first);
    else {
      while ( first != last ) {
    fvp = (*first); ++first;
    if ( fvp->prev()->vertex == vp ) {
      retfvp = fvp;
      break;
    }
      }
    }
    return retfvp;
  }

  OSSIFaceVertexPtr OSSIVertex::getFaceVertexWithNext(OSSIVertexPtr vp) {
    // Get the FaceVertex which has the given Vertex after it in it's Face
    // If only 1 FaceVertex refers to this Vertex, will return that
    // If there are more than 1 and none of them satisfies the condition, returns NULL
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp, retfvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    if ( fvpList.size() == 1 )
      retfvp = (*first);
    else {
      while ( first != last ) {
    fvp = (*first); ++first;
    if ( fvp->next()->vertex == vp ) {
      retfvp = fvp;
      break;
    }
      }
    }
    return retfvp;
  }

  OSSIFaceVertexPtr OSSIVertex::getBackFaceVertex(void) {
    // Get the FaceVertex which has the 'backface' flag set
    // If no such FaceVertex is found, returns NULL
    OSSIFaceVertexPtrList::iterator first, last;
    OSSIFaceVertexPtr fvp, retfvp = NULL;
    first = fvpList.begin(); last = fvpList.end();
    while ( first != last ) {
      fvp = (*first); ++first;
      if ( fvp->backface == true ) {
    retfvp = fvp; break;
      }
    }
    return retfvp;
  }

  void makeVertexUnique(OSSIVertexPtr dvp)
  {
    dvp->assignID();
  }

  void resetVertexType(OSSIVertexPtr dvp)
  {
    dvp->resetType();
  }

  void vertexTrace(OSSIVertexPtr vertexptr)
  {
    vertexptr->vertexTrace();
  }

  void vertexTrace(const OSSIVertex& vertex)
  {
    vertex.vertexTrace();
  }

  istream& operator >> (istream& i, OSSIVertex& dv)
  {
    // Read x,y,z coordinates.
    double x,y,z;
    i >> x >> y >> z;
    dv.coords.set(x,y,z);
    return i;
  }

  ostream& operator << (ostream& o, const OSSIVertex& dv)
  {
    // Only coordinates are written.
    double x,y,z;
    dv.coords.get(x,y,z);
    o << "v " << x << ' ' << y << ' ' << z << endl;
    return o;
  }

} // end namespace

