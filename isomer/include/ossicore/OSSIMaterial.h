/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

#ifndef _OSSI_MATERIAL_H_
#define _OSSI_MATERIAL_H_

// class for a material for use in the OSSIObject class
// Contains an array/list of faces which use this material

#include "OSSICommon.h"
#include "OSSIFace.h"
#include <Graphics/Color.h>

namespace OSSI {

class OSSIMaterial {
public :

  char *          name;                     // Name of material
  RGBColor        color;                    // Material diffuse color
  OSSIFacePtrList faces;                    // Pointers to faces using this material
  double          Ka;                       // Ambient coefficient
  double          Kd;                       // Diffuse coefficient
  double          Ks;                       // Specular coefficient

public :

  // Default, 1 and 2 arg constructors
  OSSIMaterial(const char * n, const RGBColor& c = RGBColor(0))
    : name(NULL), color(c), faces(), Ka(0.1), Kd(0.3), Ks(0.8) {
    if ( n ) {
      name = new char[strlen(n)+1]; strcpy(name,n);
    } else {
      name = new char[8]; strcpy(name,"default");
    }
  }

  OSSIMaterial(const char * n, double r, double g, double b)
    : name(NULL), color(r,g,b), faces(), Ka(0.1), Kd(0.3), Ks(0.8) {
    if ( n ) {
      name = new char[strlen(n)+1]; strcpy(name,n);
    } else {
      name = new char[8]; strcpy(name,"default");
    }
  }

  OSSIMaterial(const OSSIMaterial& mat)
    : name(NULL), color(mat.color), faces(mat.faces), Ka(mat.Ka), Kd(mat.Kd), Ks(mat.Ks) {
    name = new char[strlen(mat.name)+1]; strcpy(name,mat.name);
  }

  ~OSSIMaterial() {
    faces.clear();
    delete [] name;
  }

  OSSIMaterial& operator = (const OSSIMaterial& mat) {
    delete [] name; name = NULL;
    name = new char[strlen(mat.name)+1]; strcpy(name,mat.name);
    color = mat.color; faces = mat.faces; Ka = mat.Ka; Kd = mat.Kd; Ks = mat.Ks;
    return (*this);
  }

  OSSIMaterialPtr copy(void) const
  {
    OSSIMaterialPtr mpcopy = new OSSIMaterial(*this);
    return mpcopy;
  }

  //--- Member functions ---//

  void setColor(const RGBColor& c) {
    color = c;
  }

  void setColor(double r, double g, double b) {
    color.set(r,g,b);
  }

  void setName(const char * n) {
    if ( n ) {
      delete [] name; name = NULL;
      name = new char[strlen(n)]; strcpy(name,n);
    }
  }

  void addFace(OSSIFacePtr faceptr) {
    faces.push_back(faceptr);
  }

  void deleteFace(OSSIFacePtr faceptr) {
    if( faces.size() > 0 )
      faces.remove(faceptr);
  }

  uint numFaces(void) const
  {
    return faces.size();
  }

  // Check if material color is same as given color
  bool operator == (const RGBColor& c) const
  {
    return (color == c);
  }

  // Same as above, but function form
  bool equals(const RGBColor& c) const
  {
    return (color == c);
  }

  // Check if material name is same as given string
  bool operator == (const char * n) const
  {
    if ( n && !strcasecmp(name,n) ) return true;
    return false;
  }

  // Same as above but function form
  bool equals(const char * n) const
  {
    if ( n && !strcasecmp(name,n) ) return true;
    return false;
  }

  // Compare 2 materials - check both name and color
  bool operator == (const OSSIMaterial& mat) const
  {
    if ( (color == mat.color) &&
     !strcasecmp(name,mat.name) ) return true;
    return false;
  }

  // Same as above but function form
  bool equals(const OSSIMaterial& mat) const
  {
    if ( (color == mat.color) &&
     !strcasecmp(name,mat.name) ) return true;
    return false;
  }


};

} // end namespace

#endif /* _OSSI_MATERIAL_H_ */
