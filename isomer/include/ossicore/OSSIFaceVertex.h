/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
*/

/*
 *  OSSIFaceVertex.h
 */

#ifndef _OSSI_FACE_VERTEX_H_
#define _OSSI_FACE_VERTEX_H_

// Class for a face vertex (corner) for the OSSIObject class
// Based on OBJFaceVertex class

// Contains a pointer to a Vertex and a pointer to the Edge which "starts" at this Face Vertex
// NOTE : The same physical point may be the start of many edges, but for a given face
// only 1 Edge can start at a physical point
// "start" implies the rotation order for the face
// Each FaceVertex also contains a pointer to the Face to which it belongs

// The FaceVertex class can now also be used directly as a node in a list
// It contains pointers to the next and previous nodes of the list

#include "OSSICommon.h"
#include "OSSICoreExt.h"
#include "OSSIEdge.h"
#include "OSSIVertex.h"

namespace OSSI {

  class OSSIFaceVertex {
  public:
    static void setLastID( uint id ) {
      if( id > suLastID )
                suLastID = id;
    }
  protected:
    static uint suLastID;

    static uint newID( ) {
      uint temp = suLastID;
      suLastID++;
      return temp;
    }

  public :
    OSSIVertexPtr      vertex;                        // Associated vertex pointer
    Vector3d           normal;                        // Normal
    RGBColor           color;                         // Color
    Vector2d           texcoord;                      // Texture coordinate
    bool               backface;                      // Flag indicating this is part of a "back" face
                                                      // Used in reading OBJ files
                                                      // Default value is 'false'
  protected :
    uint               uID;                            // Id for face vertex
    uint               index;                         // Index for use in file output
    OSSIEdgePtr        epEPtr;                        // Pointer to the Edge
    OSSIFacePtr        fpFPtr;                        // Pointer to the Face

    // List node data
    OSSIFaceVertexPtr  fvpNext;                       // Pointer to next FaceVertex
    OSSIFaceVertexPtr  fvpPrev;                       // Pointer to previous FaceVertex

    OSSIFaceVertexType fvtType;                       // For use in subdivision surfaces
    Vector3d           auxcoords;                     // Coords for use during subdivs, extrude, etc.
    Vector3d           auxnormal;                     // Additional storage for normal
    Vector3d           ds2coords[4];                  // Level-2 Doo Sabin coordinates


    void assignID( ) {

            uID = (unsigned int) this;

    }

  public :
    // Default constructor
    OSSIFaceVertex(bool bf=false);
    // 2 arg-constructor - copy the pointers
    OSSIFaceVertex(OSSIVertexPtr vptr, OSSIEdgePtr eptr, bool bf=false);
    // Copy constructor
    OSSIFaceVertex(const OSSIFaceVertex& dfv);
    // Destructor
    ~OSSIFaceVertex();

    // Assignment operator
    OSSIFaceVertex& operator=(const OSSIFaceVertex& dfv);
        bool operator==(const OSSIFaceVertex &other) const;

    OSSIFaceVertexPtr copy( ) const;

    // Dump contents of this object
    void dump(ostream& o) const;

    // Average 2 facevertexes
    // Average the coordinates, normals, color and texture coordinates
    // Assumes that all 3 have valid vertex pointers
    friend void average(const OSSIFaceVertex& dfv1, const OSSIFaceVertex& dfv2, OSSIFaceVertex& ave);
    friend void average(OSSIFaceVertexPtr dfvp1, OSSIFaceVertexPtr dfvp2, OSSIFaceVertexPtr avep);

    // Query Functions
    uint getIndex( ) const { return index; }
    uint getID( ) { assignID(); return uID; }
    OSSIFaceVertexType getType( ) const { return fvtType; }
    OSSIVertexType getVertexType( ) const { return vertex->getType(); }
    OSSIVertexPtr getVertexPtr( ) const { return vertex; }
    OSSIEdgePtr getEdgePtr( ) const { return epEPtr; }
    OSSIFaceVertexPtr getOppositeCorner( ) { return epEPtr->getOtherFaceVertexPtr(this); }
        OSSIFacePtr getFacePtr( ) const { if (fpFPtr) return fpFPtr; else return NULL; }
    uint getVertexID( ) const { return vertex->getID(); }
    uint getFaceID( ) const;
    uint getEdgeID( ) const { return epEPtr->getID(); }
    Vector3d getNormal( ) const { return normal; }
    Vector3d getVertexCoords( ) const { return vertex->coords; }
    Vector2d getTexCoords( ) const { return texcoord; }
    Vector3d getAuxCoords( ) const { return auxcoords; }
    Vector3d getAuxNormal( ) const { return auxnormal; }
    void getDS2Coords(Vector3d& dsc0, Vector3d& dsc1, Vector3d& dsc2, Vector3d& dsc3) const {
      dsc0 = ds2coords[0]; dsc1 = ds2coords[1]; dsc2 = ds2coords[2]; dsc3 = ds2coords[3];
    }
    Vector3d getDS2Coord(uint index) const {
      // Assumes index is within range (0 to 3)
      return ds2coords[index];
    }

    //--- Mutative Functions ---//

    void setType(OSSIFaceVertexType type) { fvtType = type; }
    void resetType( ) { fvtType = FVTNormal; }

    // Reset to original state
    void reset( ) {
      vertex = NULL; normal.reset(); //color = 1.0; texcoord.reset();
      epEPtr = NULL; fpFPtr = NULL;
      fvpNext = this; fvpPrev = this;
      fvtType = FVTNormal;
    }

    void setVertexPtr(OSSIVertexPtr vptr) { vertex = vptr; }
    void setEdgePtr(OSSIEdgePtr eptr) { epEPtr = eptr; }
    void setFacePtr(OSSIFacePtr fptr) { fpFPtr = fptr; }
    void setNormal(const Vector3d& n) { normal = normalized(n); }

    // Check if this corner is a concave corner or not
    // Will not work properly for non-planar faces.
    // Will not work properly for faces which have edges which belong entirely to that face.
    // should ideally not work properly for highly skewed geometry (theorotically, though never tested)
    bool isConcaveCorner( ) const;

    // Check if this corner is a winged corner or not
    // A winged corner is a degenerate corner
    // i.e. the angle at the corner is 180 degrees
    bool isWingedCorner( ) const;

    //Find the next non-winged corner starting from this corner
    // If none can be found, returns NULL
    OSSIFaceVertexPtr nextNonWingedCorner( ) const;

    // Find the CLOSEST non-winged corner starting from this corner
    // If none can be found, returns NULL
    // Search both forward and backward till we find one.
    OSSIFaceVertexPtr closestNonWingedCorner( ) const;

    // Get a vector along the edge starting at this corner
    Vector3d getEdgeVector( ) const;

    // Get 2 vectors along the 2 edges which form this corner
     // Both vectors will originate at this corner
    void getEdgeVectors(Vector3d& pvec, Vector3d& nvec);

    // Compute the normal using adjacent vertices in this face
     // Normal will be adjusted to account for concave corners
    void updateNormal( );

    Vector3d computeNormal( ) {
      updateNormal();
      return normal;
    }

    void setVertexCoords(const Vector3d& vec) {
      if ( vertex ) vertex->coords = vec;
    }
    void setVertexCoords(double x, double y, double z) {
      if ( vertex ) vertex->coords.set(x,y,z);
    }

    void setTexCoords(const Vector2d& tc) { texcoord = tc; }
    void setAuxCoords(const Vector3d& p) { auxcoords = p; }
    void setAuxNormal(const Vector3d& n) { auxnormal = n; }

    void setDS2Coords(const Vector3d& dsc0, const Vector3d& dsc1, const Vector3d& dsc2, const Vector3d& dsc3) {
      ds2coords[0] = dsc0; ds2coords[1] = dsc1; ds2coords[2] = dsc2; ds2coords[3] = dsc3;
    }

    void setDS2Coord0(const Vector3d& dsc0) { ds2coords[0] = dsc0; }
    void setDS2Coord1(const Vector3d& dsc1) { ds2coords[1] = dsc1; }
    void setDS2Coord2(const Vector3d& dsc2) { ds2coords[2] = dsc2; }
    void setDS2Coord3(const Vector3d& dsc3) { ds2coords[3] = dsc3; }

    // Update the OSSIFaceVertexList of the OSSIVertex referred to by this OSSIFaceVertex
    void addSelfToVertex( );
    void deleteSelfFromVertex( );

    // Update the OSSIFaceVertexPtr of the OSSIEdge starting at this OSSIFaceVertex
    void addSelfToEdge( );
    void deleteSelfFromEdge( );

    // Remove the OSSIFaceVertexPtr from the OSSIFace to which it belongs
    void deleteSelfFromFace( );

    // List related functions

  private:

    // This function by itself can leave the OSSI object in an invalid state
    // It should be used with extreme care, ideally as part of a face reversal only.
    // To prevent unintended misuse, this is made private and classes which need
    // access to this function are made friends

    void reverse( ) {
      // Swap the previous and next pointers
      OSSIFaceVertexPtr temp = fvpNext;
      fvpNext = fvpPrev; fvpPrev = temp;
    }

    friend class OSSIFace;

  public :

    void setNext(OSSIFaceVertexPtr fvptr) { fvpNext = fvptr; }
    void setPrev(OSSIFaceVertexPtr fvptr) { fvpPrev = fvptr; }

    OSSIFaceVertexPtr next( ) const { return fvpNext; }
    OSSIFaceVertexPtr& next( ) { return fvpNext; }

    OSSIFaceVertexPtr prev( ) const { return fvpPrev; }
    OSSIFaceVertexPtr& prev( ) { return fvpPrev; }

    /* Next/prev facevertex around the vertex pointed to by this facevertex
     * Use the EdgePtr to do a vertex-trace
     * Returns this facevertex if EdgePtr is NULL */
    OSSIFaceVertexPtr vnext( );
    OSSIFaceVertexPtr vprev( );

    /* Make given OSSIFaceVertexPtr point to the next OSSIFaceVertexPtr
     * Used in list traversal */
    friend void advance(OSSIFaceVertexPtr& fvp);

    /* Make given OSSIFaceVertexPtr point to the previous OSSIFaceVertexPtr
     * Used in list traversal */
    friend void backward(OSSIFaceVertexPtr& fvp);


    void print(bool printadjacent = false) const;

    void printPointers( ) const {
      cout << "OSSIFaceVertex : " << vertex << ", " << epEPtr << ", " << fpFPtr << ";"
       << "< " << fvpPrev << ", " << fvpNext << " >" << endl;
    }

    // Write this face vertex in OSSI format and set it's index value
    void writeOSSI(ostream& o, uint newindex);

    /* Read normal, texcoord and color info for this face vertex from an input stream
     * in OSSI format. Returns the vertex index */
    uint readOSSI(istream& i);


  }; // end class OSSIFaceVertex

  bool coFacial(OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2);
  bool coFacial(const OSSIFaceVertex& fv1, const OSSIFaceVertex& fv2);

  bool areEqual(OSSIFaceVertexPtr fvptr1, OSSIFaceVertexPtr fvptr2);
  bool areEqual(const OSSIFaceVertex& fv1, const OSSIFaceVertex& fv2);

  void advance(OSSIFaceVertexPtr& fvp);
  void backward(OSSIFaceVertexPtr& fvp);



} // end namespace

#endif /* _OSSI_FACE_VERTEX_HH_ */

