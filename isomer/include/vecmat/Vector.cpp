/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
* Basic Vector
*
* Vector.h
*
*/



#include "Vector.h"
#include "Vector2d.h"
#include "Vector3d.h"
#include "Vector4d.h"

   // Cross product between 2D vectors
Vector3d operator % (const Vector2d& vec1, const Vector2d& vec2)
{
  Vector3d crossp, v1(vec1), v2(vec2);

  crossp = v1 % v2;
  return crossp;
}

   // Cross product between 3D and 2D vectors
Vector3d operator % (const Vector3d& vec1, const Vector2d& vec2)
{
  Vector3d crossp, v2(vec2);

  crossp = vec1 % v2;
  return crossp;
}

   // Cross product between 2D and 3D vectors
Vector3d operator % (const Vector2d& vec1, const Vector3d& vec2)
{
  Vector3d crossp, v1(vec1);

  crossp = v1 % vec2;
  return crossp;
}

