/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/



#ifndef _VECTOR_H_

#define _VECTOR_H_

// Common stuff for all the static Vector classes
// Forward declarations for all the classes is included here, along with
// common include files and prototypes for functions that use different kinds
// of Vectors

// Common include files
#include "../Base/BaseObject.h"
#include "../Base/StreamIO.h"
#include "../Base/Constants.h"
#include "../Base/Inlines.h"

#include <cstdlib>
#include <cmath>

// Forward declarations
class Vector2d;
class Vector3d;
class Vector4d;

// Function prototypes

   // Cross products for 2d and 3d Vectors
Vector3d operator % (const Vector2d& vec1, const Vector2d& vec2);
Vector3d operator % (const Vector3d& vec1, const Vector2d& vec2);
Vector3d operator % (const Vector2d& vec1, const Vector3d& vec2);

#endif /* #ifndef _VECTOR_H_ */
