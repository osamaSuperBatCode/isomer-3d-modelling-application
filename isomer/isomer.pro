# version info : code from -- http://wiki.qtcentre.org/index.php?title=Version_numbering_using_QMake
VERSION = $$system(svn info -r HEAD . | grep 'Changed\ Rev' | cut -b 19-)
#!isEmpty(VERSION){
        VERSION = 1.$${VERSION}
        VERSTR = '\\"$${VERSION}\\"'  # place quotes around the version string
        DEFINES += VER=\"$${VERSTR}\" # create a VER macro containing the version string
#}

#qt version
QTVERSION = $$[QT_VERSION]
QT_VERSTR = '\\"$${QTVERSION}\\"'  # place quotes around the version string
DEFINES += QT_VER=\"$${QT_VERSTR}\" # create a QT_VER macro containing the version string

# main stuff
QT += opengl xml
CONFIG += qt debug warn_off link_prl

QMAKE_CXXFLAGS_DEBUG += -pg
QMAKE_LFLAGS_DEBUG += -pg

# to include the popup command line interface leave the following line uncommented
DEFINES *= QCOMPLETER

# DEFINES += ISOMER_VERSION
DEFINES -= GPU_OK

TEMPLATE = app

SUBDIRS = include

MOC_DIR = tmp
OBJECTS_DIR = tmp
# UI_DIR = tmp

# Isomer will be the name for the debug version,
# and Isomer will be the release version
CONFIG(debug, debug|release) {
 TARGET = Isomer# -$${VERSION}
} else {
 TARGET = Isomer# -$${VERSION}
}


DEPENDPATH += \
           lang \
           include \
           include/Graphics \
           include/Light \
           include/vecmat \
           include/ossicore \
           include/ossiaux \


INCLUDEPATH += \
            include \
            include/Graphics \
            include/Light \
            include/vecmat \
            include/ossicore \
            include/ossiaux \

unix {

        QMAKE_LFLAGS += -L./lib
        LIBS += -lvecmat -lossicore -lossiaux
        DEFINES *= LINUX
}

else:win32 {

        # Isomer will be the name for the debug version,
        # and Isomer will be the release version
        CONFIG(debug, debug|release) {
         TARGET = Isomer# -$${VERSION}
        } else {
         TARGET = Isomer# -$${VERSION}
        }

        #application icon windows
        RC_FILE = isomer.rc



        INCLUDEPATH += ./lib
        QMAKE_LFLAGS += -L./lib
         INCLUDEPATH += C:/isomer/isomert/isomer/lib
         QMAKE_LFLAGS += -LC:/isomer/isomert/isomer/lib
         #INCLUDEPATH += C:/isomer/isomert/isomer/lib
         #QMAKE_LFLAGS += -LC:/isomert/isomert/isomer/lib
        LIBS += -lvecmat -lossicore -lossiaux

}


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = isomer

FORMS += shortcutdialog.ui stylesheeteditor.ui

SOURCES += main.cpp\
        MainWindow.cpp \
    BasicsMode.cpp \
    ConicalMode.cpp \
    PainterMode.cpp \
    ExtrusionsMode.cpp \
    GeometryRenderer.cpp \
    GLWidget.cpp \
    HandleMode.cpp \
    MainWindowCallbacks.cpp \
    MainWindowRemeshingCallbacks.cpp \
    qshortcutdialog.cpp \
    qshortcutmanager.cpp \
    RemeshingMode.cpp \
    stylesheeteditor.cpp \
    ISOPatchFace.cpp \
    ISOPatchObject.cpp \
    Isomer.cpp \
    IsomerPreferences.cpp \
    OSSILighting.cpp \
    OSSILocator.cpp \
    OSSIRenderer.cpp \
    OSSIScriptEditor.cpp \
    OSSISelection.cpp \
    OSSIUndo.cpp \
    CommandCompleter.cpp \
    CgCompute.cpp \
    include/Camera3.cpp \
    include/Camera2.cpp

HEADERS  += MainWindow.h \
    BasicsMode.h \
    CommandCompleter.h \
    ConicalMode.h \
    PainterMode.h \
    ExtrusionsMode.h \
    GeometryRenderer.h \
    GLWidget.h \
    HandleMode.h \
    qcumber.h \
    qshortcutdialog.h \
    RemeshingMode.h \
    stylesheeteditor.h \
    ISOPatch.h \
    ISOPatchFace.h \
    ISOPatchObject.h \
    Isomer.h \
    IsomerPreferences.h \
    Viewport.h \
    qshortcutmanager.h \
    include/Camera3.h \
    include/Base/BaseObject.h \
    include/Base/Constants.h \
    include/Base/Inlines.h \
    include/Base/StreamIO.h \
    include/Graphics/Color.h \
    include/Graphics/Grid.h \
    include/Graphics/Texture.h \
    include/Graphics/Transform.h \
    include/Light/AmbientLight.h \
    include/Light/Light.h \
    include/Light/PointLight.h \
    include/Light/SpotLight.h \
    include/ossicore/OSSIObject.h \
    OSSILighting.h \
    OSSILocator.h \
    OSSIRenderer.h \
    OSSISelection.h \
    CgCompute.h \
    include/WireframeRenderer.h \
    include/TexturedRenderer.h \
    include/TexturedLitRenderer.h \
    include/ShadedRenderer.h \
    include/PatchRenderer.h \
    include/NormalRenderer.h \
    include/LitRenderer.h \
    include/ColorableRenderer.h \
    include/Camera2.h

RESOURCES += application.qrc
