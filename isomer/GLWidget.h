/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/
//Basically the preferences window
#ifndef _GL_WIDGET_H
#define _GL_WIDGET_H


#include <QtOpenGL/QtOpenGL>
#include <QtOpenGL/QGLContext>
#include <QCursor>

#include <QBrush>
#include <QImage>
#include <QPen>

#include <QApplication>
#include <QKeyEvent>
#include <QFont>
#include <QPushButton>

#include <include/ossicore/OSSIObject.h>
#include "OSSIRenderer.h"
#include "ISOPatchObject.h"

#include "OSSILighting.h"
#include <Light.h>
#include <SpotLight.h>
#include <PointLight.h>
#include <AmbientLight.h>

#include "include/Camera3.h"
#include "CgCompute.h"

#ifdef GPU_OK
using namespace Cg;
#endif // GPU_OK

using namespace OSSI;


class QCursor;
class QImage;

#include "OSSILocator.h"


class GLWidget : public QGLWidget {
    Q_OBJECT        // must include this if you use Qt signals/slots

public :

    GLWidget(	int w, int h , OSSIRendererPtr rp, QColor color, QColor vcolor, OSSIObjectPtr op, const QGLFormat & format, QWidget * parent = 0 ); //se docs for QGLFOrmat :P

    ~GLWidget( );

    public slots :


    void update() { repaint(); } //qts repaint


        //set lighting colors... warm and cool...like deep purple, smoke on water, fire in the sky! :D
    void setWarmLightColor(QColor c); //setting warm colors (aka the color of light for rendering)
    void setCoolLightColor(QColor c); //setting cool colors (aka the color of light for rendering)
    void setLightIntensity(double i); //set the intensity of light, ie, harshness.

    //setters for properties inside OSSIRenderer
    void setRenderColor(QColor c); //set the objects render color

    void setViewportColor(QColor c){ mViewportColor = c; redraw(); } //setting viewport color
    void setWireframeColor(QColor c){ if(renderer) renderer->setWireframeColor(c); redraw(); } //setting the wireframe color
    void setSilhouetteColor(QColor c){ if( renderer ) renderer->setSilhouetteColor(c); redraw(); } //setting the silhouette color
    void setWireframeThickness(double t){	if( renderer ) renderer->setWireframeThickness(t); redraw(); } //setting wireframe thickness
    void setVertexThickness(double t){ if( renderer ) renderer->setVertexThickness(t); redraw(); } //setting vertex thickness
    void setSilhouetteThickness(double t){ if( renderer ) renderer->setSilhouetteThickness(t); redraw(); } //setting silhouette thickness
    void setNormalColor(QColor c){ if( renderer ) renderer->setNormalColor(c); redraw(); } //setting the color of the normal from the centeroid
    void setFaceCentroidColor(QColor c){ if( renderer ) renderer->setFaceCentroidColor(c); redraw(); } //setting the color of the face centeroid
    void setFaceCentroidThickness(double t){ if( renderer ) renderer->setFaceCentroidThickness(t); redraw(); } //set faces centroid thickness
    void setNormalThickness(double t){ if( renderer ) renderer->setNormalThickness(t); redraw(); } //set the thickness of the normal
    void setNormalLength(double l){ if( renderer ) renderer->setNormalLength(l); redraw(); } //set the length of the normal

    //setters for properties in glwidget... remember, they always call redraw()
    void setSelectedVertexThickness(double t){ mSelectedVertexThickness = t; redraw(); } //set the selceted vertices' thickness.
    void setSelectedEdgeThickness(double t){ mSelectedEdgeThickness = t; redraw(); } //set the edges thickness
    void setSelectedEdgeColor(QColor c){ mSelectedEdgeColor = c; redraw(); } //set selected edges color
    void setSelectedFaceColor(QColor c){ mSelectedFaceColor = c; redraw(); } //set selected faces color
    void setSelectedVertexColor(QColor c){ mSelectedVertexColor = c; redraw(); } //set selected vertexes color
    void setVertexIDBgColor(QColor c){ mVertexIDBgColor = c; redraw(); } //set vetex ID's BG Color
    void setFaceIDBgColor(QColor c){ mFaceIDBgColor = c; redraw(); } //set Face IDs BG Color
    void setEdgeIDBgColor(QColor c){ mEdgeIDBgColor = c; redraw(); } //set Edge IDs BG Color

    inline void setLightPosition(Vector3d p){ plight.position = p; } //set the position of light dot.

    void setNearPlane(double n) { mCamera->setNearPlane(n); redraw(); } //set near plan frustrum
    void setFarPlane(double f){ mCamera->setFarPlane(f); redraw(); } //set far plane frustrum
    void setFOV(double fov){ mCamera->setFOV(fov); redraw(); } //set field of view

    void zoomIn(){ mCamera->HandleMouseWheel(100, width(),height()); repaint(); } //zooming in function, with mousewheel
    void zoomOut(){ mCamera->HandleMouseWheel(-100, width(),height()); repaint(); } //zooming out function wiht mouse wheel

        //compute lighting and normals functions now moved here from MainWindow
    void recomputeNormals(); //recomputation of normals
    void recomputeLighting(); //recomputation of lighting
    void recomputePatches(); //recomputation of patches

    void toggleHUD() { //toogle HUD menu on and off
        mShowHUD = !mShowHUD;
        this->repaint();
    }

    #ifdef GPU_OK
    void toggleGPU(){
        if (renderer)
            renderer->toggleGPU();
        mUseGPU = !mUseGPU;
        recomputeLighting();
        repaint();
    }
    #endif

    void toggleAntialiasing(){ //Toggle AA
        mAntialiasing = !mAntialiasing;
        // std::cout << mAntialiasing << "\n";
        renderer->toggleAntialiasing();
        repaint();
    }

            // Toggle wireframe
    void toggleWireframe(void) {
        if ( renderer ) renderer->toggleWireframe();
        this->repaint();
    }

            // Toggle silhouette
    void toggleSilhouette(void) {
        if ( renderer ) renderer->toggleSilhouette();
        this->repaint();

    }

            // Toggle vertices
    void toggleVertices(void) {
        if ( renderer ) renderer->toggleVertices();
        this->repaint();
    }

    void toggleFaceIDs( ) { //toggle showing of face IDS
        mShowFaceIDs = !mShowFaceIDs;
        this->repaint();
    }

    void toggleEdgeIDs( ) { //toggle showing of Edge IDS
        mShowEdgeIDs = !mShowEdgeIDs;
        this->repaint();
    }

    void toggleVertexIDs( ) { //toggle Vertex IDs
        mShowVertexIDs = !mShowVertexIDs;
        this->repaint();
    }

    void toggleSelectedIDs( ) { //toggle Selected IDS
        mShowSelectedIDs = !mShowSelectedIDs;
        this->repaint();
    }

    void toggleBrush( ) { //toggle Brush
        mShowBrush = !mShowBrush;
        this->repaint();
    }

    void toggleSelectionWindow( ) { //toggle the selection Window
        mShowSelectionWindow = !mShowSelectionWindow;
        this->repaint();
    }

    void showSelectionWindow( ) { //show selection window
        mShowSelectionWindow = true;
        this->repaint();
    }

    void hideSelectionWindow( ) { //hide the selection window
        mShowSelectionWindow = false;
        this->repaint();
    }

    void setSelectionWindowStartX(double x){ // the starting X Co Ord of SelectionWindow
        mSelectionWindowStartX = x;
    }

    void setSelectionWindowStartY(double y){ // the starting Y COord of SelectionWindow
        mSelectionWindowStartY = y;
    }

    void showBrush( ) { //show the brush
        mShowBrush = true;
        setCursor(Qt::BlankCursor);
        this->repaint();
    }

    void hideBrush( ) { //hide brush
        mShowBrush = false;
        setCursor(Qt::ArrowCursor);
        this->repaint();
    }

    bool isBrushVisible(){ return mShowBrush; } //bool to check whether brush is visible or not

            // Toggle object orientation
    void toggleObjectOrientation(void) {
        if ( renderer ) renderer->toggleObjectOrientation();
        this->repaint();

    }

    void toggleNormals(void) { //toggle normals on and off
        if ( renderer ) renderer->toggleFaceNormals();
        this->repaint();
    }

    void toggleFaceCentroids(void) { //toggle face centroids on and off
        if ( renderer ) renderer->toggleFaceCentroids();
        this->repaint();
    }

    void selectAllFaces(){ //select all the faces
        clearSelectedFaces();
        object->getFaces(object->sel_fptr_array);
        repaint();
    }

    void selectAllEdges(){ //select all the edges
        clearSelectedEdges();
        object->getEdges(object->sel_eptr_array);
        repaint();
    }

    void selectAllVertices(){ //select all the vertices
        clearSelectedVertices();
        object->getVertices(object->sel_vptr_array);
        repaint();
    }

    void selectAllFaceVertices(){ //select all the face vertices
        clearSelectedFaceVertices();
        repaint();
    }

    void selectInverseFaces(){ //select the inverse faces
        OSSIFacePtrArray fparray;
        vector<OSSIFacePtr>::iterator it;
        object->getFaces(fparray);
        for (it = fparray.begin(); it != fparray.end(); it++){
            if (!isSelected(*it)){
                addToSelection(*it);
            }
            else clearSelectedFace(*it);
        }
        repaint();
    }

    void selectInverseEdges(){
        OSSIEdgePtrArray eparray;
        vector<OSSIEdgePtr>::iterator it;
        object->getEdges(eparray);
        for (it = eparray.begin(); it != eparray.end(); it++){
            if (!isSelected(*it)){
                addToSelection(*it);
            }
            else clearSelectedEdge(*it);
        }
        repaint();
    }

    void selectInverseVertices(){
        OSSIVertexPtrArray vparray;
        vector<OSSIVertexPtr>::iterator it;
        object->getVertices(vparray);
        for (it = vparray.begin(); it != vparray.end(); it++){
            if (!isSelected(*it)){
                addToSelection(*it);
            }
            else clearSelectedVertex(*it);
        }
        repaint();
    }

    void selectInverseFaceVertices(){

    }

        // Toggle grid display on/off
    void toggleGrid(void) {

    }

        // Toggle axis display on/off
    void toggleAxes(void) {
        if ( showaxes == true ) showaxes = false;
        else showaxes = true;
        this->repaint();
    }

    protected :

    void initializeGL( ); //initializes GL Widget
    void paintEvent(QPaintEvent *event); //paint event

    void drawText( int width, int height ); //draw text (used to draw text on labels)
    void drawIDs( QPainter *painter, const GLdouble *model, const GLdouble *proj, const GLint	*view); //draws IDs
    void drawSelectedIDs( QPainter *painter, const GLdouble *model, const GLdouble *proj, const GLint	*view); //raws IDs for selected Vs, Es or Fs
    void drawHUD(QPainter *painter); //draw heads up display
    void drawBrush(QPainter *painter); //draw Brush
    void drawSelectionWindow(QPainter *painter); //draw the selection window
    void resizeGL( int width, int height ); //resize teh Gl Widget

    void setupViewport(int width, int height); //setup the viewport

    #ifdef GPU_OK
        void enableGLLights(); //gl lighting for use in cg shaders
      CgData *cg;
      void initCg( );
    #endif // GPU_OK


//Various Booleans
    bool mIsFullScreen;
    bool mShowFaceIDs;
    bool mShowVertexIDs;
    bool mShowEdgeIDs;
    bool mShowSelectedIDs;
    bool mShowFaceVertexIDs;
    bool mShowHUD;
    bool mShowBrush;
    bool mShowSelectionWindow;
    bool mUseGPU;
    bool mAntialiasing;
    int mBrushStartX; //starting point of brush

    //temporarily disable object rendering
    bool renderObject;

        // Selection - these are shared by all viewports
    static OSSILocatorPtrArray sel_lptr_array; // List of selected OSSILocator pointers

    PerspCamera *mCamera; //Camera

        //viewport obejct

    OSSIObjectPtr object; // Pointer to OSSIObject
    ISOPatchObjectPtr patchObject; // The Patch Object associated with object

       //Viewport Renderer

    OSSIRendererPtr renderer;

        //light is now a member of Glwidget instead of MainWindow
    PointLight plight;



    // Default Locator object
    OSSILocatorPtr locatorPtr;

        // Boolean flags - these are viewport related
    bool showgrid;                           // whether to show the grid
    bool showaxes;                           // whether to show the axes

    QImage image;

    public :

    inline ISOPatchObjectPtr patchobject( ) { return patchObject; }

    QCursor *cursor;
    void redraw();

    double getBrushSize(){ return mBrushSize;}
    void setBrushSize(double s){ mBrushSize = max(2.0,min(200.0,s)); repaint();	} // max ot check max between the two

    inline void resetCamera(){ mCamera->Reset(); }


    void setRenderingEnabled(bool b){ renderObject = b; } //enable the rendering
//setting of modes strings to be used later
    void setModeString(QString s){
        mModeString = s;
    }

    void setExtrusionModeString(QString s){
        mExtrusionModeString = s;
    }

    void setRemeshingSchemeString(QString s){
        mRemeshingSchemeString = s;
    }

    void setSelectionMaskString(QString s){
        mSelectionMaskString = s;
    }
    QString getSelectionMaskString() { return mSelectionMaskString; }

    void renderLocatorsForSelect()
    {
        locatorPtr->setRenderSelection(true);
        locatorPtr->render();
        locatorPtr->setRenderSelection(false);
    }

    void resetLocator()
    {
        locatorPtr->setActiveVertex(NULL);
    }

        //Initialize the selection lists
    void initializeSelectionLists(int num) {
                // Reserves memory for the arrays to avoid reallocation
                // It is not mandatory to call this function, but recommended
        object->sel_vptr_array.reserve(num);
        object->sel_eptr_array.reserve(num);
        object->sel_fptr_array.reserve(num);
        object->sel_fvptr_array.reserve(num);
        sel_lptr_array.reserve(num); // brianb
    }

        //Add items to the selection lists - check for NULL pointers
    void addToSelection(OSSILocatorPtr lp)
    {
        if ( lp ) sel_lptr_array.push_back(lp);
    }

    void addToSelection(OSSIVertexPtr vp) {
        if ( vp ) object->sel_vptr_array.push_back(vp);
    }

    void addToSelection(OSSIEdgePtr ep) {
        if ( ep ) object->sel_eptr_array.push_back(ep);
    }

    void addToSelection(OSSIFacePtr fp) {
        if ( fp ) object->sel_fptr_array.push_back(fp);
    }

    void addToSelection(OSSIFaceVertexPtr fvp) {
        if ( fvp ) object->sel_fvptr_array.push_back(fvp);
    }

        //Check if given item is there in the selection list

    static bool isSelected(OSSILocatorPtr vp)
    {
        bool found = false;
        if ( vp )
        {
            for (int i=0; i < sel_lptr_array.size(); ++i)
                if ( sel_lptr_array[i] == vp )
            {
                found = true; break;
            }
        }
        return found;
    }

    bool isSelected(OSSIVertexPtr vp) {
        bool found = false;
        if ( vp )
        {
            for (uint i=0; i < object->sel_vptr_array.size(); ++i)
                if ( object->sel_vptr_array[i] == vp )
            {
                found = true; break;
            }
        }
        return found;
    }

    bool isSelected(OSSIEdgePtr ep) {
        bool found = false;
        if ( ep )
        {
            for (uint i=0; i < object->sel_eptr_array.size(); ++i)
                if ( object->sel_eptr_array[i] == ep )
            {
                found = true; break;
            }
        }
        return found;
    }

    bool isSelected(OSSIFacePtr fp) {
        bool found = false;
        if ( fp )
        {
            for (uint i=0; i < object->sel_fptr_array.size(); ++i)
                if ( object->sel_fptr_array[i] == fp )
            {
                found = true; break;
            }
        }
        return found;
    }

    bool isSelected(OSSIFaceVertexPtr fvp) {
        bool found = false;
        if ( fvp )
        {
            for (uint i=0; i < object->sel_fvptr_array.size(); ++i)
                if ( object->sel_fvptr_array[i] == fvp )
            {
                found = true; break;
            }
        }
        return found;
    }

        //Set the selected item at given index
        //If size of array is smaller than index item will be added to end of array
    void setSelectedLocator(int index, OSSILocatorPtr vp)
    {
        if ( vp && index >= 0 )
        {
            if ( index < sel_lptr_array.size() ) sel_lptr_array[index] = vp;
            else sel_lptr_array.push_back(vp);
        }
    }

    void setSelectedVertex(int index, OSSIVertexPtr vp) {
        if ( vp && index >= 0 )
        {
            if ( (uint) index < object->sel_vptr_array.size() ) object->sel_vptr_array[index] = vp;
            else object->sel_vptr_array.push_back(vp);
        }
    }

    void setSelectedEdge(int index, OSSIEdgePtr ep) {
        if ( ep && index >= 0 )
        {
            if ( (uint) index < object->sel_eptr_array.size() ) object->sel_eptr_array[index] = ep;
            else object->sel_eptr_array.push_back(ep);
        }
    }

    void setSelectedFace(int index, OSSIFacePtr fp) {
        if ( fp && index >= 0 )
        {
            if ( (uint) index < object->sel_fptr_array.size() ) object->sel_fptr_array[index] = fp;
            else object->sel_fptr_array.push_back(fp);
        }
    }

    void setSelectedFaceVertex(int index, OSSIFaceVertexPtr fvp) {
        if ( fvp && index >= 0 )
        {
            if ( (uint) index < object->sel_fvptr_array.size() ) object->sel_fvptr_array[index] = fvp;
            else object->sel_fvptr_array.push_back(fvp);
        }
    }

    void setSelectedVertex(OSSIVertexPtr vp) {
        if ( vp )
        {
            if (!isSelected(vp)){

                object->sel_vptr_array.push_back(vp);
            }
        }
    }

    void setSelectedEdge(OSSIEdgePtr ep) {
        if ( ep )
        {
            if (!isSelected(ep)){

                 object->sel_eptr_array.push_back(ep);
            }
        }
    }

    void setSelectedFace(OSSIFacePtr fp) {
        if ( fp )
        {
            if (!isSelected(fp)){

                object->sel_fptr_array.push_back(fp);
            }
        }
    }

    void setSelectedFaceVertex(OSSIFaceVertexPtr fvp) {
        if ( fvp )
        {
            if (!isSelected(fvp)){

                object->sel_fvptr_array.push_back(fvp);
            }
        }
    }

        //Return the selected items at given index
    OSSILocatorPtr getLocatorPtr() const
    {
        return locatorPtr;
    }

    static OSSILocatorPtr getSelectedLocator(int index)
    {
        if ( index < sel_lptr_array.size() ) return sel_lptr_array[index];
        return NULL;
    }

    OSSIVertexPtr getSelectedVertex(int index) {
        if ( (uint) index < object->sel_vptr_array.size() ) return object->sel_vptr_array[index];
        return NULL;
    }

    OSSIEdgePtr getSelectedEdge(int index) {
        if ( (uint) index < object->sel_eptr_array.size() ) return object->sel_eptr_array[index];
        return NULL;
    }

    OSSIFacePtr getSelectedFace(int index) {
        if ( (uint) index < object->sel_fptr_array.size() ) return object->sel_fptr_array[index];
        return NULL;
    }

    OSSIFacePtrArray getSelectedFaces() {
        return object->sel_fptr_array;
    }

    OSSIVertexPtrArray getSelectedVertices() {
        return object->sel_vptr_array;
    }

    OSSIEdgePtrArray getSelectedEdges() {
        return object->sel_eptr_array;
    }

    OSSIFaceVertexPtrArray getSelectedCorners() {
        return object->sel_fvptr_array;
    }

    OSSIFaceVertexPtr getSelectedFaceVertex(int index) {
        if ( (uint) index < object->sel_fvptr_array.size() ) return object->sel_fvptr_array[index];
        return NULL;
    }

        // Find the number of items in the selection lists
    static int numSelectedLocators(void) {
        return sel_lptr_array.size();
}

int numSelectedVertices(void) {
    return object->sel_vptr_array.size();
}

int numSelectedEdges(void) {
    return object->sel_eptr_array.size();
}

int numSelectedFaces(void) {
    return object->sel_fptr_array.size();
}

int numSelectedCorners(void) {
    return object->sel_fvptr_array.size();
}

        //Clear individual selection lists
static void clearSelectedLocators(void) {
    sel_lptr_array.clear();
}

void clearSelectedVertices(void) {
    object->sel_vptr_array.clear();
}

void clearSelectedEdges(void) {
    object->sel_eptr_array.clear();
}

void clearSelectedFaces(void) {
    object->sel_fptr_array.clear();
}

void clearSelectedCorners(void) {
    object->sel_fvptr_array.clear();
}

void clearSelectedFace(OSSIFacePtr fp){
    vector<OSSIFacePtr>::iterator fiterator;
    for(fiterator = object->sel_fptr_array.begin(); fiterator != object->sel_fptr_array.end(); fiterator++) {
        if (fp == *fiterator){

            object->sel_fptr_array.erase(fiterator);
            return;
        }
    }
}

void clearSelectedEdge(OSSIEdgePtr ep){
    vector<OSSIEdgePtr>::iterator it;
    for(it = object->sel_eptr_array.begin(); it != object->sel_eptr_array.end(); it++) {
        if (ep == *it){
            object->sel_eptr_array.erase(it);
            return;
        }
    }
}

void clearSelectedVertex(OSSIVertexPtr vp){
    vector<OSSIVertexPtr>::iterator it;
    for(it = object->sel_vptr_array.begin(); it != object->sel_vptr_array.end(); it++) {
        if (vp == *it){
            object->sel_vptr_array.erase(it);
            return;
        }
    }
}

void clearSelectedFaceVertex(OSSIFaceVertexPtr fvp){
    vector<OSSIFaceVertexPtr>::iterator it;
    for(it = object->sel_fvptr_array.begin(); it != object->sel_fvptr_array.end(); it++) {
        if (fvp == *it){
            object->sel_fvptr_array.erase(it);
            return;
        }
    }
}

void clearSelectedFaceVertices(void) {
    object->sel_fvptr_array.clear();
}

        // Reset all selection lists
void clearSelected(void) {
    sel_lptr_array.clear(); // brianb
    object->sel_vptr_array.clear();
    object->sel_eptr_array.clear();
    object->sel_fptr_array.clear();
    object->sel_fvptr_array.clear();
}

        // Draw the selected items
void drawSelected(void);



        // Set the object which should be shown in the viewport
    void createPatchObject( ) {
        if( patchObject != NULL ) {
            delete patchObject; patchObject = 0;
        }
        patchObject = new ISOPatchObject( object->getID() );
        if( patchObject )
            patchObject->updatePatches( object );
    }

        // Set the renderer for this viewport
void setRenderer(OSSIRendererPtr rp) {
    renderer = rp;
    renderer->setState();
    this->repaint();
}

OSSIRendererPtr getRenderer(){
    return renderer;
}

        // Set the render flags
void setRenderFlags(int rflags) {
    if ( renderer ) renderer->setRenderFlags(rflags);
    this->repaint();
}

        // Get the render flags
int getRenderFlags(void) const {
    if ( renderer )
        return renderer->getRenderFlags();
    return -1;
}

    // Subroutines (aka functions) for selecting Vertices, Edges, Faces and FaceVertices (Corners)
OSSILocatorPtr selectLocator(int mx, int my, int w=10, int h=10);
OSSIVertexPtr selectVertex(int mx, int my, int w=30, int h=30);
OSSIVertexPtrArray selectVertices(int mx, int my, int w=30, int h=30);
OSSIEdgePtr selectEdge(int mx, int my, int w=10, int h=10);
OSSIEdgePtrArray selectEdges(int mx, int my, int w=10, int h=10);
OSSIFacePtr selectFace(int mx, int my, int w=2.5, int h=2.5);
OSSIFacePtrArray selectFaces(int mx, int my, int w=2.5, int h=2.5);
OSSIFacePtr deselectFaces(int mx, int my, int w=2.5, int h=2.5);
OSSIFaceVertexPtr selectFaceVertex(OSSIFacePtr fp, int mx, int my, int w=40, int h=40);
OSSIFaceVertexPtrArray selectFaceVertices(OSSIFacePtr fp, int mx, int my, int w=40, int h=40);

    // Event handler for the viewport. handles only mouse events when glwidget has focus
    // which relate to navigational controls (ALT + push/drag/release)
void mouseMoveEvent(QMouseEvent *event);
void mousePressEvent(QMouseEvent *event);
void mouseReleaseEvent(QMouseEvent *event);
void wheelEvent(QWheelEvent *event);



private :
friend class QGLFormat;
QColor mGlobalAmbient;
QColor mRenderColor;
QColor mViewportColor;

QColor mWireframeColor;
QColor mSilhouetteColor;
double mWireframeThickness;
double mSilhouetteThickness;
double mVertexThickness;

QColor mVertexIDColor;
QColor mEdgeIDColor;
QColor mFaceIDColor;
QColor mVertexIDBgColor;
QColor mEdgeIDBgColor;
QColor mFaceIDBgColor;

QColor mSelectedVertexColor;
double mSelectedVertexThickness;
QColor mSelectedFaceColor;
QColor mSelectedEdgeColor;
double mSelectedEdgeThickness;

double mBrushSize;
double mStartDragX;
double mStartDragY;
double mSelectionWindowStartX;
double mSelectionWindowStartY;

QColor mXAxisColor;
QColor mYAxisColor;
QColor mZAxisColor;

QWidget *mParent;
QString mModeString, mRemeshingSchemeString, mSelectionMaskString, mExtrusionModeString;

};

#endif
