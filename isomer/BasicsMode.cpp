/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*
*
* This file is the CPP file which enables the BasicsMode in Isomer 3D Modelling Application.
*
* The File Conatining Function Prototypes: BasicsMode.h
*
*/

/****************************************************************************
 ** The Basics Mode of Isomer 3D Modelling application.
 **
 ****************************************************************************/
#include <QtGui>

#include "BasicsMode.h"



/*
*  Constructor
*
* @param parent the MainWindow widget
* @param sm the shortcut manager class for adding a custom shortcut to each menu action or icon
* @param actionList the master list of actions for use with the CommandCompleter class
*
*/
BasicsMode::BasicsMode(QWidget *parent, QShortcutManager *sm, QWidget *actionList) //constructor
        : QWidget(parent) { //initialization list

    setParent(0); //set QWidget parent to 0. QWidget *parent = 0 is the parent of the new widget. If it is 0 (the default), the new widget will be a window. If not, it will be a child of parent, and be constrained by parent's geometry
    mParent = parent; //store the parent in mParent Variable.

    //mode widgets
    mInsertEdgeWidget = new QWidget;
  mDeleteEdgeWidget = new QWidget;
  mCollapseEdgeWidget = new QWidget;
  mSubdivideEdgeWidget = new QWidget;
  mConnectEdgesWidget = new QWidget;
  mSpliceCornersWidget = new QWidget;
  mTransformsWidget = new QWidget;


    //each mode widget will each be added to the
    //ToolOptionsDockWidget of the MainWindow class
    setupInsertEdge();
    setupDeleteEdge();
    setupCollapseEdge();
    setupSubdivideEdge();
    setupConnectEdges();
    setupSpliceCorners();
    setupTransforms();


    mInsertEdgeAction = new QAction(tr("Insert Edge"),this); //tr is an i18n (internationalization) wrapper, thus making translations easier. this keyword is used to specefy the QObject parent
    mInsertEdgeAction->setIcon(QIcon(":/images/insert_edge.png")); //set icon
    mInsertEdgeAction->setCheckable(true); //set whether the action is checkable, that is toggleable
    mInsertEdgeAction->setStatusTip(tr("Enter Insert Edge Mode")); //status bar at the bottom message
    mInsertEdgeAction->setToolTip(tr("Insert Edge Mode")); //tooltip on mouse hover option
    connect(mInsertEdgeAction, SIGNAL(triggered()), this, SLOT(triggerInsertEdge())); //Creates a connection of the given type from the signal in the sender object to the method in the receiver object. Returns true if the connection succeeds; otherwise returns false.
    actionList->addAction(mInsertEdgeAction);

    mDeleteEdgeAction = new QAction(tr("Delete Edge"),this);
    mDeleteEdgeAction->setIcon(QIcon(":/images/delete_edge.png"));
    mDeleteEdgeAction->setCheckable(true);
    mDeleteEdgeAction->setStatusTip(tr("Enter Delete Edge Mode"));
    mDeleteEdgeAction->setToolTip(tr("Delete Edge Mode"));
    connect(mDeleteEdgeAction, SIGNAL(triggered()), this, SLOT(triggerDeleteEdge()));
    actionList->addAction(mDeleteEdgeAction);

    mCollapseEdgeAction = new QAction(tr("Collapse Edge"),this);
    mCollapseEdgeAction->setIcon(QIcon(":/images/collapse_edge.png"));
    mCollapseEdgeAction->setCheckable(true);
    mCollapseEdgeAction->setStatusTip(tr("Enter Collapse Edge Mode"));
    mCollapseEdgeAction->setToolTip(tr("Collapse Edge Mode"));
    connect(mCollapseEdgeAction, SIGNAL(triggered()), this, SLOT(triggerCollapseEdge()));
    actionList->addAction(mCollapseEdgeAction);

    mSubdivideEdgeAction = new QAction(tr("Subdivide Edge"),this);
    mSubdivideEdgeAction->setIcon(QIcon(":/images/subdivide_edge.png"));
    mSubdivideEdgeAction->setCheckable(true);
    mSubdivideEdgeAction->setStatusTip(tr("Enter Subdivide Edge Mode"));
    mSubdivideEdgeAction->setToolTip(tr("Subdivide Edge Mode"));
    connect(mSubdivideEdgeAction, SIGNAL(triggered()), this, SLOT(triggerSubdivideEdge()));
    actionList->addAction(mSubdivideEdgeAction);

    mConnectEdgesAction = new QAction(tr("Connect Edges"),this);
    mConnectEdgesAction->setIcon(QIcon(":/images/connect_edges.png"));
    mConnectEdgesAction->setCheckable(true);
    mConnectEdgesAction->setStatusTip(tr("Enter Connect Edges Mode"));
    mConnectEdgesAction->setToolTip(tr("Connect Edges Mode"));
    connect(mConnectEdgesAction, SIGNAL(triggered()), this, SLOT(triggerConnectEdges()));
    actionList->addAction(mConnectEdgesAction);

    mSpliceCornersAction = new QAction(tr("Splice Corners"),this);
    mSpliceCornersAction->setIcon(QIcon(":/images/splice_corners.png"));
    mSpliceCornersAction->setCheckable(true);
    mSpliceCornersAction->setStatusTip(tr("Enter Splice Corners Mode"));
    mSpliceCornersAction->setToolTip(tr("Splice Corners Mode"));
    connect(mSpliceCornersAction, SIGNAL(triggered()), this, SLOT(triggerSpliceCorners()));
    actionList->addAction(mSpliceCornersAction);

    mTransformsAction = new QAction(tr("Transforms"),this);
    mTransformsAction->setIcon(QIcon(":/images/transforms.png"));
    mTransformsAction->setCheckable(true);
    mTransformsAction->setStatusTip(tr("Enter Transforms Mode"));
    mTransformsAction->setToolTip(tr("Transforms Mode"));
    connect(mTransformsAction, SIGNAL(triggered()), this, SLOT(triggerTransforms()));
    actionList->addAction(mTransformsAction);



}

QMenu* BasicsMode::getMenu(){
    mBasicsMenu = new QMenu(tr("Basics"));

    mBasicsMenu->addAction(mInsertEdgeAction);
    mBasicsMenu->addAction(mDeleteEdgeAction);
    mBasicsMenu->addAction(mCollapseEdgeAction);
    mBasicsMenu->addAction(mSubdivideEdgeAction);
    mBasicsMenu->addAction(mConnectEdgesAction);
    mBasicsMenu->addAction(mSpliceCornersAction);
    mBasicsMenu->addAction(mTransformsAction);


    return mBasicsMenu;
}

void BasicsMode::triggerInsertEdge(){

    ((MainWindow*)mParent)->setToolOptions(mInsertEdgeWidget); //the sidedocked window's options are set accordingly to this mode.
    ((MainWindow*)mParent)->setMode(MainWindow::InsertEdge); //set mode to InsertEdge from mainwindow
}

void BasicsMode::triggerDeleteEdge(){

    ((MainWindow*)mParent)->setToolOptions(mDeleteEdgeWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::DeleteEdge);
}

void BasicsMode::triggerCollapseEdge(){

    ((MainWindow*)mParent)->setToolOptions(mCollapseEdgeWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::CollapseEdge);
}

void BasicsMode::triggerSubdivideEdge(){

    ((MainWindow*)mParent)->setToolOptions(mSubdivideEdgeWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::SubdivideEdge);
}

void BasicsMode::triggerConnectEdges(){

    ((MainWindow*)mParent)->setToolOptions(mConnectEdgesWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::ConnectEdges);
}

void BasicsMode::triggerSpliceCorners(){

    ((MainWindow*)mParent)->setToolOptions(mSpliceCornersWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::SpliceCorners);
}

void BasicsMode::triggerTransforms(){

    ((MainWindow*)mParent)->setToolOptions(mTransformsWidget);
    ((MainWindow*)mParent)->setMode(MainWindow::NormalMode);
}


//function to add all the actions to a group, then to toolbar and then to the stackedWidget
void BasicsMode::addActions(QActionGroup *actionGroup, QToolBar *toolBar, QStackedWidget *stackedWidget){

    actionGroup->addAction(mInsertEdgeAction);
    actionGroup->addAction(mDeleteEdgeAction);
    actionGroup->addAction(mCollapseEdgeAction);
    actionGroup->addAction(mSubdivideEdgeAction);
    actionGroup->addAction(mConnectEdgesAction);
    actionGroup->addAction(mSpliceCornersAction);
    actionGroup->addAction(mTransformsAction);


    toolBar->addAction(mInsertEdgeAction);
    toolBar->addAction(mDeleteEdgeAction);
    toolBar->addAction(mCollapseEdgeAction);
    toolBar->addAction(mSubdivideEdgeAction);
    toolBar->addAction(mConnectEdgesAction);
    toolBar->addAction(mSpliceCornersAction);
    toolBar->addAction(mTransformsAction);


    stackedWidget->addWidget(mInsertEdgeWidget);
    stackedWidget->addWidget(mDeleteEdgeWidget);
    stackedWidget->addWidget(mCollapseEdgeWidget);
    stackedWidget->addWidget(mSubdivideEdgeWidget);
    stackedWidget->addWidget(mConnectEdgesWidget);
    stackedWidget->addWidget(mSpliceCornersWidget);
    stackedWidget->addWidget(mTransformsWidget);


}
// create double spinbox
QDoubleSpinBox *BasicsMode::createDoubleSpinBox(QGridLayout *layout, QLabel *label, QString s, double low, double high, double step, double value, double decimals, int row, int col){

    label->setText(s);
    QDoubleSpinBox *spinbox = new QDoubleSpinBox;
    spinbox->setAccelerated(true);
    spinbox->setRange(low, high);
    spinbox->setSingleStep(step);
    spinbox->setValue(value);
    spinbox->setDecimals(decimals);
    spinbox->setMaximumSize(75,25);
    layout->addWidget(label,row,col);
    layout->addWidget(spinbox,row,col+1);

    return spinbox;
}
//setup different UIs with GridLayout in the Tools Option Diagloug Box.
void BasicsMode::setupInsertEdge() {

    mInsertEdgeLayout = new QGridLayout;
    // mInsertEdgeLayout->setMargin(0);
    noOptionsInsertEdgeLabel = new QLabel(this);(tr("No Options for this tool."));
    mInsertEdgeLayout->addWidget(noOptionsInsertEdgeLabel,0,0);

    mInsertEdgeLayout->setRowStretch(4,1);
    mInsertEdgeLayout->setColumnStretch(2,1);
    mInsertEdgeWidget->setWindowTitle(tr("Insert Edge"));
    mInsertEdgeWidget->setLayout(mInsertEdgeLayout);
}

void BasicsMode::setupDeleteEdge() {

    mDeleteEdgeLayout = new QGridLayout;
    // mDeleteEdgeLayout->setMargin(0);

    //cleanup checkbox
    cleanupDeleteEdgeCheckBox = new QCheckBox(tr("Cleanup"),this);
    connect(cleanupDeleteEdgeCheckBox, SIGNAL(stateChanged(int)),
          ((MainWindow*)mParent), SLOT(toggleDeleteEdgeCleanupFlag(int)));
    mDeleteEdgeLayout->addWidget(cleanupDeleteEdgeCheckBox,0,0);

    mDeleteEdgeLayout->setRowStretch(1,1);
    mDeleteEdgeLayout->setColumnStretch(2,1);
    mDeleteEdgeWidget->setWindowTitle(tr("Delete Edge Mode"));
    mDeleteEdgeWidget->setLayout(mDeleteEdgeLayout);
}

void BasicsMode::setupCollapseEdge() {

    mCollapseEdgeLayout = new QGridLayout;
    noOptionsCollapseEdgeLabel = new QLabel(tr("No Options for this tool."), this);
    mCollapseEdgeLayout->addWidget(noOptionsCollapseEdgeLabel,0,0);

    mCollapseEdgeLayout->setRowStretch(1,1); //1st:the row to stretch 2: by how much
    mCollapseEdgeLayout->setColumnStretch(2,1); //1st:the column to stretch 2nd: by how much
    mCollapseEdgeWidget->setWindowTitle(tr("Collapse Edge"));
    mCollapseEdgeWidget->setLayout(mCollapseEdgeLayout); //sets layout manager widget to the specefied layout
}

void BasicsMode::setupSubdivideEdge() {

    mSubdivideEdgeLayout = new QGridLayout;


    //number of subdivisions spinbox
    numSubdivsLabel = new QLabel(this);
    numSubdivsSpinBox = createDoubleSpinBox(mSubdivideEdgeLayout, numSubdivsLabel, tr("# Subdivisions"), 1, 10, 1, 1, 0, 0,0);
    connect(numSubdivsSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(changeNumSubDivs(double)));

    // mSubdivideEdgeLayout->addStretch(1);
    mSubdivideEdgeLayout->setRowStretch(1,1);
    mSubdivideEdgeLayout->setColumnStretch(2,1);
    mSubdivideEdgeWidget->setWindowTitle(tr("Subdivide Edge Mode"));
    mSubdivideEdgeWidget->setLayout(mSubdivideEdgeLayout);

}

void BasicsMode::setupConnectEdges(){

    mConnectEdgesLayout = new QGridLayout;
    noOptionsConnectEdgesLabel = new QLabel(tr("No Options for this tool."), this);
    mConnectEdgesLayout->addWidget(noOptionsConnectEdgesLabel,0,0);

    mConnectEdgesLayout->setRowStretch(1,1);
    mConnectEdgesLayout->setColumnStretch(2,1);
    mConnectEdgesWidget->setWindowTitle(tr("Connect Edges"));
    mConnectEdgesWidget->setLayout(mConnectEdgesLayout);

}

void BasicsMode::setupSpliceCorners(){

    mSpliceCornersLayout = new QGridLayout;
    mSpliceCornersLayout->setRowStretch(4,1);
    mSpliceCornersLayout->setColumnStretch(2,1);

    noOptionsSpliceCornersLabel = new QLabel(tr("No Options for this tool."),this);
    mSpliceCornersLayout->addWidget(noOptionsSpliceCornersLabel,0,0);

    mSpliceCornersWidget->setWindowTitle(tr("Splice Corners"));
    mSpliceCornersWidget->setLayout(mSpliceCornersLayout);

}

void BasicsMode::setupTransforms(){

    mTransformsLayout = new QGridLayout;
    mTransformsLayout->setVerticalSpacing(1);
    mTransformsLayout->setHorizontalSpacing(1);

    xPosLabel = new QLabel(this);
    xPosSpinBox = createDoubleSpinBox(mTransformsLayout, xPosLabel, tr("X-translate"), -100.0, 100.0, 0.5, 0.0, 3, 1,0);
    connect(xPosSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(translatex(double)));

    yPosLabel = new QLabel(this);
    yPosSpinBox = createDoubleSpinBox(mTransformsLayout, yPosLabel, tr("Y-translate"), -100.0, 100.0, 0.5, 0.0, 3, 2,0);
    connect(yPosSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(translatey(double)));

    zPosLabel = new QLabel(this);
    zPosSpinBox = createDoubleSpinBox(mTransformsLayout, zPosLabel, tr("Z-translate"), -100.0, 100.0, 0.5, 0.0, 3, 3,0);
    connect(zPosSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(translatez(double)));


    //x scale
    xScaleLabel = new QLabel(this);
    xScaleSpinBox = createDoubleSpinBox(mTransformsLayout, xScaleLabel, tr("X-scale"), 0.1, 10.0, 0.1, 1.0, 3, 5,0);
    connect(xScaleSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(scalex(double)));

    yScaleLabel = new QLabel(this);
    yScaleSpinBox = createDoubleSpinBox(mTransformsLayout, yScaleLabel, tr("Y-scale"), 0.1, 10.0, 0.1, 1.0, 3, 6,0);
    connect(yScaleSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(scaley(double)));

    zScaleLabel = new QLabel(this);
    zScaleSpinBox = createDoubleSpinBox(mTransformsLayout, zScaleLabel, tr("Z-scale"), 0.1, 10.0, 0.1, 1.0, 3, 7,0);
    connect(zScaleSpinBox, SIGNAL(valueChanged(double)), ((MainWindow*)mParent), SLOT(scalez(double)));

    freezeTransformsButton = new QPushButton(tr("&Freeze Transforms"));
    connect(freezeTransformsButton, SIGNAL(clicked()), this, SLOT(freezeTransforms()));
    mTransformsLayout->addWidget(freezeTransformsButton,8,0,1,2);

    mTransformsLayout->setRowStretch(9,1);
    mTransformsLayout->setColumnStretch(2,1);
    mTransformsWidget->setWindowTitle(tr("Transforms Mode"));
    mTransformsWidget->setLayout(mTransformsLayout);
}

void BasicsMode::freezeTransforms()
{
    ((MainWindow*)mParent)->freezeTransforms();
    xPosSpinBox->setValue(0.0);
    yPosSpinBox->setValue(0.0);
    zPosSpinBox->setValue(0.0);
    xScaleSpinBox->setValue(1.0);
    yScaleSpinBox->setValue(1.0);
    zScaleSpinBox->setValue(1.0);
}


void BasicsMode::retranslateUi(){
    mInsertEdgeAction->setText(tr("Insert Edge"));
    mInsertEdgeAction->setStatusTip(tr("Enter Insert Edge Mode"));
    mInsertEdgeAction->setToolTip(tr("Insert Edge Mode"));
    mDeleteEdgeAction->setText(tr("Delete Edge"));
    mDeleteEdgeAction->setStatusTip(tr("Enter Delete Edge Mode"));
    mDeleteEdgeAction->setToolTip(tr("Delete Edge Mode"));
    mCollapseEdgeAction->setText(tr("Collapse Edge"));
    mCollapseEdgeAction->setStatusTip(tr("Enter Collapse Edge Mode"));
    mCollapseEdgeAction->setToolTip(tr("Collapse Edge Mode"));
    mSubdivideEdgeAction->setText(tr("Subdivide Edge"));
    mSubdivideEdgeAction->setStatusTip(tr("Enter Subdivide Edge Mode"));
    mSubdivideEdgeAction->setToolTip(tr("Subdivide Edge Mode"));
    mConnectEdgesAction->setText(tr("Connect Edges"));
    mConnectEdgesAction->setStatusTip(tr("Enter Connect Edges Mode"));
    mConnectEdgesAction->setToolTip(tr("Connect Edges Mode"));
    mSpliceCornersAction->setText(tr("Splice Corners"));
    mSpliceCornersAction->setStatusTip(tr("Enter Splice Corners Mode"));
    mSpliceCornersAction->setToolTip(tr("Splice Corners Mode"));
    mTransformsAction->setText(tr("Transforms"));
    mTransformsAction->setStatusTip(tr("Enter Transforms Mode"));
    mTransformsAction->setToolTip(tr("Transforms Mode"));
    mBasicsMenu->setTitle(tr("Basics"));

    //mode spinbox labels etc...
    noOptionsInsertEdgeLabel->setText(tr("No Options for this tool."));
    mInsertEdgeWidget->setWindowTitle(tr("Insert Edge"));
    cleanupDeleteEdgeCheckBox->setText(tr("Cleanup"));
    mDeleteEdgeWidget->setWindowTitle(tr("Delete Edge Mode"));
    noOptionsCollapseEdgeLabel->setText(tr("No Options for this tool."));
    mCollapseEdgeWidget->setWindowTitle(tr("Collapse Edge"));
    numSubdivsLabel->setText(tr("# Subdivisions"));
    mSubdivideEdgeWidget->setWindowTitle(tr("Subdivide Edge Mode"));
    noOptionsConnectEdgesLabel->setText(tr("No Options for this tool."));
    mConnectEdgesWidget->setWindowTitle(tr("Connect Edges"));
    noOptionsSpliceCornersLabel->setText(tr("No Options for this tool."));
    mSpliceCornersWidget->setWindowTitle(tr("Splice Corners"));
    xPosLabel->setText(tr("X-translate"));
    yPosLabel->setText(tr("Y-translate"));
    zPosLabel->setText(tr("Z-translate"));
    xScaleLabel->setText(tr("X-scale"));
    yScaleLabel->setText(tr("Y-scale"));
    zScaleLabel->setText(tr("Z-scale"));
    freezeTransformsButton->setText(tr("&Freeze Transforms"));
    mTransformsWidget->setWindowTitle(tr("Transforms Mode"));
}

