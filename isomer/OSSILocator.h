/*
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* The Original Code is Copyright (C) 2013-2014 by Osama Iqbal
* All rights reserved.
*
* The Original Code is: all of this file.
*
* ***** END GPL LICENSE BLOCK *****
*/

#ifndef _OSSI_LOCATOR_H_

#define _OSSI_LOCATOR_H_

// Class to act as a handle/transformation handle for OSSI verticies

#include <OSSICommon.h>
class OSSILocator;
typedef OSSILocator * OSSILocatorPtr;
typedef vector<OSSILocator> OSSILocatorArray;
typedef vector<OSSILocatorPtr> OSSILocatorPtrArray;
void erase_dlp(OSSILocatorPtr lp);



#include <OSSIVertex.h>


using namespace OSSI;

class OSSILocator
{
  protected :

     static uint suLastID;                             // Distinct ID for each instance
                                                       // The last assigned ID is stored in this
                                                       // class variable

        // Generate a new unique ID
     static uint newID(void)
       {
         uint temp = suLastID;
         suLastID++;
         return temp;
       }

  public :

     Vector3d              coords;                     // Coordinates of locator
     unsigned long         flags;                      // Variable for general use to store flags, etc.
     OSSIVertexPtr         vtx;                        // Associated vertex
     int                   selectedAxis;               // Index of selected axis -1=none,0=x,1=y,2=z,3=free
     bool                  renderSelection;

  protected :

     uint                  uID;                        // ID for this OSSILocator

        // Assign a unique ID for this instance
     void assignID(void)
       {
         uID = OSSILocator :: newID();
       }

  public :

        // Default constructor
     OSSILocator()
       : coords(), flags(0)
       {
         vtx = NULL;
         assignID();
         selectedAxis = -1;
         renderSelection = false;
       }

        // 1 argument constructor
     OSSILocator(const Vector3d& vec)
       : coords(vec), flags(0)
       {
         vtx = NULL;
         assignID();
         selectedAxis = -1;
         renderSelection = false;
       }

        // 3 argument constructor
     OSSILocator(double x, double y, double z)
       : coords(x,y,z), flags(0)
       {
         vtx = NULL;
         assignID();
         selectedAxis = -1;
         renderSelection = false;
       }

        // Copy constructor
     OSSILocator(const OSSILocator& dl)
       : vtx(dl.vtx), coords(dl.coords), flags(dl.flags), uID(dl.uID), selectedAxis(dl.selectedAxis),
         renderSelection(dl.renderSelection)
       { }

        // Destructor
     ~OSSILocator()
       {}

        // Assignment operator
     OSSILocator& operator = (const OSSILocator& dl)
       {
         vtx = dl.vtx;
         coords = dl.coords; flags = dl.flags;
         uID = dl.uID;
         return (*this);
       }

     OSSILocatorPtr copy(void) const
       {
         OSSILocatorPtr newdl = new OSSILocator(*this);
         return newdl;
       }

        // Dump contents of this object
     void dump(ostream& o) const;

     void reset(void)
       {
         coords.reset(); flags = 0;
       }

     void makeUnique(void)
       {
         assignID();
       }

            // friend void makeLocatorUnique(OSSILocatorPtr dlp);

        //--- Query functions ---//

     Vector3d getCoords(void) const
       {
         return coords;
       }

     uint getID(void) const
       {
         return uID;
       }
     int getSelectedAxis() const
       {
         return selectedAxis;
       }

        //--- Mutative functions ---//
     void setRenderSelection(bool s)
       {
         renderSelection = s;
       }

     void setSelectedAxis(int axis)
       {
         if (axis >= 0 && axis <= 3)
           selectedAxis = axis;
         else
           selectedAxis = -1;
       }

     void setCoords(const Vector3d& p)
       {
         coords = p;
       }

     void setActiveVertex(OSSIVertexPtr vptr)
       {
         vtx = vptr;
       }

     OSSIVertexPtr getActiveVertex()
       {
         return vtx;
       }

        // Apply a transformation specified by the matrix to the coordinates
     void transform(const Matrix4x4& tmat)
       {
         Vector4d tp(coords); tp[3] = 1.0;
         tp = tmat * tp;
         tp /= tp[3];
         coords = tp;
       }

        // Print out this locator
     void print(void) const
       {
         cout << "OSSILocator " << uID << "," << vtx << " : " << coords << endl;
       }

     void render(void);
};



#endif /* #ifndef _OSSI_LOCATOR_HH_ */


